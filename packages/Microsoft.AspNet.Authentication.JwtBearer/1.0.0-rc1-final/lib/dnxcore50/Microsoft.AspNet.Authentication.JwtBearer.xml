<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Microsoft.AspNet.Authentication.JwtBearer</name>
    </assembly>
    <members>
        <member name="T:Microsoft.AspNet.Builder.JwtBearerAppBuilderExtensions">
            <summary>
            Extension methods to add OpenIdConnect Bearer authentication capabilities to an HTTP application pipeline.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Builder.JwtBearerAppBuilderExtensions.UseJwtBearerAuthentication(Microsoft.AspNet.Builder.IApplicationBuilder,Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions)">
            <summary>
            Adds the <see cref="T:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerMiddleware"/> middleware to the specified <see cref="T:Microsoft.AspNet.Builder.IApplicationBuilder"/>, which enables Bearer token processing capabilities.
            This middleware understands appropriately
            formatted and secured tokens which appear in the request header. If the Options.AuthenticationMode is Active, the
            claims within the bearer token are added to the current request's IPrincipal User. If the Options.AuthenticationMode 
            is Passive, then the current request is not modified, but IAuthenticationManager AuthenticateAsync may be used at
            any time to obtain the claims from the request's bearer token.
            See also http://tools.ietf.org/html/rfc6749
            </summary>
            <param name="app">The <see cref="T:Microsoft.AspNet.Builder.IApplicationBuilder"/> to add the middleware to.</param>
            <param name="options">A <see cref="T:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions"/> that specifies options for the middleware.</param>
            <returns>A reference to this instance after the operation has completed.</returns>
        </member>
        <member name="M:Microsoft.AspNet.Builder.JwtBearerAppBuilderExtensions.UseJwtBearerAuthentication(Microsoft.AspNet.Builder.IApplicationBuilder,System.Action{Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions})">
            <summary>
            Adds the <see cref="T:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerMiddleware"/> middleware to the specified <see cref="T:Microsoft.AspNet.Builder.IApplicationBuilder"/>, which enables Bearer token processing capabilities.
            This middleware understands appropriately
            formatted and secured tokens which appear in the request header. If the Options.AuthenticationMode is Active, the
            claims within the bearer token are added to the current request's IPrincipal User. If the Options.AuthenticationMode 
            is Passive, then the current request is not modified, but IAuthenticationManager AuthenticateAsync may be used at
            any time to obtain the claims from the request's bearer token.
            See also http://tools.ietf.org/html/rfc6749
            </summary>
            <param name="app">The <see cref="T:Microsoft.AspNet.Builder.IApplicationBuilder"/> to add the middleware to.</param>
            <param name="configureOptions">An action delegate to configure the provided <see cref="T:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions"/>.</param>
            <returns>A reference to this instance after the operation has completed.</returns>
        </member>
        <member name="T:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerDefaults">
            <summary>
            Default values used by bearer authentication.
            </summary>
        </member>
        <member name="F:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme">
            <summary>
            Default value for AuthenticationScheme property in the JwtBearerAuthenticationOptions
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerHandler.HandleAuthenticateAsync">
            <summary>
            Searches the 'Authorization' header for a 'Bearer' token. If the 'Bearer' token is found, it is validated using <see cref="T:System.IdentityModel.Tokens.TokenValidationParameters"/> set in the options.
            </summary>
            <returns></returns>
        </member>
        <member name="T:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerMiddleware">
            <summary>
            Bearer authentication middleware component which is added to an HTTP pipeline. This class is not
            created by application code directly, instead it is added by calling the the IAppBuilder UseJwtBearerAuthentication
            extension method.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerMiddleware.#ctor(Microsoft.AspNet.Builder.RequestDelegate,Microsoft.Extensions.Logging.ILoggerFactory,Microsoft.Extensions.WebEncoders.IUrlEncoder,Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions)">
            <summary>
            Bearer authentication component which is added to an HTTP pipeline. This constructor is not
            called by application code directly, instead it is added by calling the the IAppBuilder UseJwtBearerAuthentication 
            extension method.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerMiddleware.CreateHandler">
            <summary>
            Called by the AuthenticationMiddleware base class to create a per-request handler. 
            </summary>
            <returns>A new instance of the request handler</returns>
        </member>
        <member name="T:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions">
            <summary>
            Options class provides information needed to control Bearer Authentication middleware behavior
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.#ctor">
            <summary>
            Creates an instance of bearer authentication options with default values.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.RequireHttpsMetadata">
            <summary>
            Gets or sets if HTTPS is required for the metadata address or authority.
            The default is true. This should be disabled only in development environments.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.MetadataAddress">
            <summary>
            Gets or sets the discovery endpoint for obtaining metadata
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.Authority">
            <summary>
            Gets or sets the Authority to use when making OpenIdConnect calls.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.Audience">
            <summary>
            Gets or sets the audience for any received OpenIdConnect token.
            </summary>
            <value>
            The expected audience for any received OpenIdConnect token.
            </value>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.Challenge">
            <summary>
            Gets or sets the challenge to put in the "WWW-Authenticate" header.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.Events">
            <summary>
            The object provided by the application to process events raised by the bearer authentication middleware.
            The application may implement the interface fully, or it may create an instance of JwtBearerAuthenticationEvents
            and assign delegates only to the events it wants to process.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.BackchannelHttpHandler">
            <summary>
            The HttpMessageHandler used to retrieve metadata.
            This cannot be set at the same time as BackchannelCertificateValidator unless the value
            is a WebRequestHandler.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.BackchannelTimeout">
            <summary>
            Gets or sets the timeout when using the backchannel to make an http call.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.Configuration">
            <summary>
            Configuration provided directly by the developer. If provided, then MetadataAddress and the Backchannel properties
            will not be used. This information should not be updated during request processing.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.ConfigurationManager">
            <summary>
            Responsible for retrieving, caching, and refreshing the configuration from metadata.
            If not provided, then one will be created using the MetadataAddress and Backchannel properties.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.RefreshOnIssuerKeyNotFound">
            <summary>
            Gets or sets if a metadata refresh should be attempted after a SecurityTokenSignatureKeyNotFoundException. This allows for automatic
            recovery in the event of a signature key rollover. This is enabled by default.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.SystemClock">
            <summary>
            For testing purposes only.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.SecurityTokenValidators">
            <summary>
            Gets the ordered list of <see cref="T:System.IdentityModel.Tokens.ISecurityTokenValidator"/> used to validate access tokens.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerOptions.TokenValidationParameters">
            <summary>
            Gets or sets the parameters used to validate identity tokens.
            </summary>
            <remarks>Contains the types and definitions required for validating a token.</remarks>
            <exception cref="T:System.ArgumentNullException">if 'value' is null.</exception>
        </member>
        <member name="T:Microsoft.AspNet.Authentication.JwtBearer.IJwtBearerEvents">
            <summary>
            OpenIdConnect bearer token middleware events.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.IJwtBearerEvents.AuthenticationFailed(Microsoft.AspNet.Authentication.JwtBearer.AuthenticationFailedContext)">
            <summary>
            Invoked if exceptions are thrown during request processing. The exceptions will be re-thrown after this event unless suppressed.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.IJwtBearerEvents.ReceivingToken(Microsoft.AspNet.Authentication.JwtBearer.ReceivingTokenContext)">
            <summary>
            Invoked when a protocol message is first received.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.IJwtBearerEvents.ReceivedToken(Microsoft.AspNet.Authentication.JwtBearer.ReceivedTokenContext)">
            <summary>
            Invoked with the security token that has been extracted from the protocol message.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.IJwtBearerEvents.ValidatedToken(Microsoft.AspNet.Authentication.JwtBearer.ValidatedTokenContext)">
            <summary>
            Invoked after the security token has passed validation and a ClaimsIdentity has been generated.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.IJwtBearerEvents.Challenge(Microsoft.AspNet.Authentication.JwtBearer.JwtBearerChallengeContext)">
            <summary>
            Invoked to apply a challenge sent back to the caller.
            </summary>
        </member>
        <member name="T:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerEvents">
            <summary>
            OpenIdConnect bearer token middleware events.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerEvents.OnAuthenticationFailed">
            <summary>
            Invoked if exceptions are thrown during request processing. The exceptions will be re-thrown after this event unless suppressed.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerEvents.OnReceivingToken">
            <summary>
            Invoked when a protocol message is first received.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerEvents.OnReceivedToken">
            <summary>
            Invoked with the security token that has been extracted from the protocol message.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerEvents.OnValidatedToken">
            <summary>
            Invoked after the security token has passed validation and a ClaimsIdentity has been generated.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.JwtBearerEvents.OnChallenge">
            <summary>
            Invoked to apply a challenge sent back to the caller.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.ReceivingTokenContext.Token">
            <summary>
            Bearer Token. This will give application an opportunity to retrieve token from an alternation location.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.Resources.Exception_OptionMustBeProvided">
            <summary>
            The '{0}' option must be provided.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.Resources.FormatException_OptionMustBeProvided(System.Object)">
            <summary>
            The '{0}' option must be provided.
            </summary>
        </member>
        <member name="P:Microsoft.AspNet.Authentication.JwtBearer.Resources.Exception_ValidatorHandlerMismatch">
            <summary>
            An ICertificateValidator cannot be specified at the same time as an HttpMessageHandler unless it is a WebRequestHandler.
            </summary>
        </member>
        <member name="M:Microsoft.AspNet.Authentication.JwtBearer.Resources.FormatException_ValidatorHandlerMismatch">
            <summary>
            An ICertificateValidator cannot be specified at the same time as an HttpMessageHandler unless it is a WebRequestHandler.
            </summary>
        </member>
    </members>
</doc>
