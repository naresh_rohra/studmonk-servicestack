﻿using System.Threading.Tasks;

namespace StudMonk.ServiceStack.RestApi.Identity
{
    public interface IAccountRepository
    {
        Task<bool> DeleteUserProfilePictureAsync(string UserId);
        Task<UserProfilePicture> GetUserProfilePictureByIdAsync(string UserId);
        Task<bool> SaveProfilePic(string UserId, string DisplayName, byte[] imageBytes);
    }
}