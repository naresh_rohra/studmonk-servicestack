﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;

namespace StudMonk.ServiceStack.RestApi.Identity
{
    public class RolesRequirement : IAuthorizationRequirement
    {
        public Model.Role[] Roles { get; set; }
    }

    public class RolesAuthorizationHandler : AuthorizationHandler<RolesRequirement>
    {
        public override Task HandleAsync(AuthorizationContext context)
        {
            var rolesRequirements = context.PendingRequirements.Where(r => typeof(RolesRequirement) == r.GetType()).Cast<RolesRequirement>().ToArray();
            foreach(var requirement in rolesRequirements)
            { 
                Handle(context, requirement);
            }
            return Task.FromResult(0);
        }

        protected override void Handle(AuthorizationContext context, RolesRequirement requirement)
        {
            bool isInRole = false;
            foreach(var role in requirement.Roles)
            {
                if(context.User.IsInRole(role.Name))
                {
                    isInRole = true;
                    break;
                }
            }
            if(isInRole)
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }
        }

        protected override Task HandleAsync(AuthorizationContext context, RolesRequirement requirement)
        {
            Handle(context, requirement);
            return Task.FromResult(0);
        }
    }
}
