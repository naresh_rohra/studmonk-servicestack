﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudMonk.ServiceStack.RestApi.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }

        public DateTime RegisteredOn { get; set; }

        public string DeviceId { get; set; }

        public virtual List<UserProfilePicture> UserProfilePictures { get; set; }

        public string TenantId { get; set; }

        public DateTime? ActiveFrom { get; set; }

        public DateTime? ActiveTill { get; set; }

        public Boolean? IsLocked { get; set; }
    }

    public class UserProfilePicture
    {
        public virtual string UserId { get; set; }

        public byte ImageType { get; set; }

        public Byte[] Image { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? LastModifiedOn { get; set; }
                
        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }
    }

    public class ApplicationUserInfo
    {
        public string Fullname { get; set; }

        public string Email { get; set; }

        public string UserId { get; set; }

        public string PhoneNo { get; set; }

        public DateTime RegisteredOn { get; set; }

        public string Role { get; set; }

        public string TenantId { get; set; }

        public AuthorizedDeviceInfo[] AuthorizedDevices { get; set; }

        public DateTime? ActiveFrom { get; set; }

        public DateTime? ActiveTill { get; set; }

        public bool IsLocked { get; set; }
    }

    public class AuthorizedDeviceInfo
    {
        public string DeviceName;
        public string DeviceId;
    }

    public class LocalIdentityDbContext : IdentityDbContext<Identity.ApplicationUser>
    {
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<UserProfilePicture> UserProfilePictures { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserProfilePicture>()
            .HasOne(p => p.ApplicationUser)
            .WithMany(b => b.UserProfilePictures);

            modelBuilder.Entity<UserProfilePicture>()
            .HasKey(o => new { o.UserId, o.ImageType }) ;
        }
    }
}