﻿using Microsoft.AspNet.Identity;

namespace StudMonk.ServiceStack.RestApi.Identity
{
    public class RegistrationResult 
    {
        public string UserId;
        public IdentityResult Result;
        public string Token;
    }
}
