﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using System;


namespace StudMonk.ServiceStack.RestApi.Identity
{
    public class AccountRepository : IAccountRepository
    {
        private LocalIdentityDbContext _context;
        public AccountRepository(LocalIdentityDbContext context)
        {
            _context = context;
        }

        public async Task<bool> DeleteUserProfilePictureAsync(string UserId)
        {
            var UserProfilePicture = await GetUserProfilePictureByIdAsync(UserId);

            if (UserProfilePicture == null)
            {
                return false;
            }
            _context.UserProfilePictures.Remove(UserProfilePicture);
            return (await _context.SaveChangesAsync() > 0);
        }

        public async Task<UserProfilePicture> GetUserProfilePictureByIdAsync(string UserId)
        {
            return await _context.UserProfilePictures
                                        .Where(v => v.UserId == UserId)
                                        .FirstOrDefaultAsync();
        }

        public async Task<bool> SaveProfilePic(string UserId, string DisplayName, byte[] imageBytes)
        {
            try
            {
                //user for display name
                var user = (from q in _context.Users
                            where q.Id == UserId
                            select q).FirstOrDefault();

                if (user != null)
                {
                    //var userProfile = await _repository.GetUserProfilePictureByIdAsync(UserId);
                    var userProfile = (from p in _context.UserProfilePictures
                                       where p.UserId == UserId
                                       select p).FirstOrDefault();

                    if (userProfile != null)//has previous data update
                    {
                        userProfile.Image = imageBytes;
                        user.DisplayName = DisplayName;
                    }
                    else
                    {
                        user.DisplayName = DisplayName;

                        UserProfilePicture UserProfilePicture = new UserProfilePicture { UserId = UserId, Image = imageBytes, ImageType = 0, CreatedOn = DateTime.Now };
                        _context.UserProfilePictures.Add(UserProfilePicture);
                    }

                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

    }
}