﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using System.IdentityModel.Tokens;
using System.Security.Cryptography;
using Microsoft.AspNet.Authentication.JwtBearer;
using Microsoft.AspNet.Diagnostics;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using System.Web.Http;
using StudMonk.ServiceStack.RestApi.Quartz.JobScheduler;
using Quartz;
using StudMonk.ServiceStack.RestApi.Identity;
using System;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.RestApi
{
    public class Startup
    {
        public static IConfigurationRoot ConfigurationRoot { get; set; }

        public static AppConfig.RootConfiguration AppRootConfiguration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            //Setup configuration
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appconfig.json");
            ConfigurationRoot = builder.Build();
        }

        // This method gets called by a runtime.
        // Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();

            AppRootConfiguration = new AppConfig.RootConfiguration(ConfigurationRoot);

            services.AddScoped(typeof(ModelRepository.InstanceActivator), o => new EFModelRepository.InstanceActivator(AppRootConfiguration.Data.DefaultConnection.ConnectionString));
            services.AddScoped(typeof(DomainOperations.InstanceActivator), typeof(DomainProcess.InstanceActivator));
            services.AddScoped(typeof(UserManager<Identity.ApplicationUser>), typeof(Identity.ApplicationUserManager<Identity.ApplicationUser>));

            services.AddSingleton(typeof(ILoggerFactory), o => new LoggerFactory().AddSerilog(GetLogger()));
            services.AddSingleton(typeof(DomainOperations.EmailNotification), o => new DomainProcess.EmailNotificationProcess(AppRootConfiguration.Email.Server, AppRootConfiguration.Email.Port, AppRootConfiguration.Email.FromEmailAddr, AppRootConfiguration.Email.Username, AppRootConfiguration.Email.Password));//
            services.AddSingleton(typeof(AwsSnsSettings), o => new AwsSnsSettings(AppRootConfiguration.AwsSns.AWSAccessKey, AppRootConfiguration.AwsSns.AWSSecretKey, AppRootConfiguration.AwsSns.Region, AppRootConfiguration.AwsSns.ServiceURL, AppRootConfiguration.AwsSns.TopicArn, AppRootConfiguration.AwsSns.QuestionOfDayTriggerIntervalInHours));

            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddCors();
            services.AddMvc();

            ConfigureAuthServices(services);

            services.AddSingleton(typeof(AppConfig.RootConfiguration), o => AppRootConfiguration);
        }

        private void ConfigureAuthServices(IServiceCollection services)
        {
            services.AddEntityFramework()
                .AddSqlServer()
                .AddDbContext<Identity.LocalIdentityDbContext>(options =>options.UseSqlServer(ConfigurationRoot.Get<string>("Data:DefaultConnection:ConnectionString")));
            
            services.AddAuthorization(auth =>
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build())
            );

            services.AddIdentity<Identity.ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<Identity.LocalIdentityDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<AuthorizationOptions>(options => {
                options.AddPolicy("UserManagement", policy => policy.AddRequirements(new Identity.RolesRequirement() { Roles = new Model.Role[] { Model.Roles.Administrator, Model.Roles.TenantAdministrator } }).Build());
            });
            services.Configure<AuthorizationOptions>(options => {
                options.AddPolicy("FullTestManagement", policy => policy.AddRequirements(new Identity.RolesRequirement() { Roles = new Model.Role[] { Model.Roles.Administrator, Model.Roles.ContentAdministrator } }).Build());
            });
            services.AddSingleton<IAuthorizationHandler, Identity.RolesAuthorizationHandler>();
        }

        private Serilog.ILogger GetLogger()
        {
            return new Serilog.LoggerConfiguration()
                .WriteTo.Sink(new Serilog.Sinks.RollingFile.RollingFileSink(AppRootConfiguration.Logs.PathPattern,
                    new Serilog.Formatting.Json.JsonFormatter(), null, null))
                .CreateLogger();
        }

        private static void HandleOptionsRequest(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                await Task.Run(() => {
                    AddCorsHeadersToResponse(context.Response);
                    context.Response.StatusCode = 200;
                });
            });
        }

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.MapWhen(context => context.Request.Method == "OPTIONS", HandleOptionsRequest);

            // Configure the HTTP request pipeline.
            app.UseStaticFiles();

            ConfigureAuthApp(app);

            app.UseIdentity();

            app.UseMvc();
            app.UseRouter(new RouteCollection());
            app.UseCors(builder => builder.WithHeaders(AppRootConfiguration.Security.Cors.AllowedHeader)
                .WithMethods(AppRootConfiguration.Security.Cors.AllowedMethods).WithOrigins(AppRootConfiguration.Security.Cors.AllowedOrigins).AllowCredentials());
        }

        private void AddCorsHeaders(HttpResponse response)
        {
            response.Headers.Add("Access-Control-Allow-Origin", AppRootConfiguration.Security.Cors.AllowedOrigins);
            response.Headers.Add("Access-Control-Allow-Headers", AppRootConfiguration.Security.Cors.AllowedHeader);
            response.Headers.Add("Access-Control-Expose-Headers", AppRootConfiguration.Security.Cors.AllowedHeader);
            response.Headers.Add("Access-Control-Allow-Methods", AppRootConfiguration.Security.Cors.AllowedMethods);
        }

        private static void AddCorsHeadersToResponse(HttpResponse response)
        {
            response.Headers.Add("Access-Control-Allow-Origin", AppRootConfiguration.Security.Cors.AllowedOrigins);
            response.Headers.Add("Access-Control-Allow-Headers", AppRootConfiguration.Security.Cors.AllowedHeader);
            response.Headers.Add("Access-Control-Expose-Headers", AppRootConfiguration.Security.Cors.AllowedHeader);
            response.Headers.Add("Access-Control-Allow-Methods", AppRootConfiguration.Security.Cors.AllowedMethods);
        }

        private void ConfigureAuthApp(IApplicationBuilder app)
        {
            // Register a simple error handler to catch token expiries and change them to a 401, 
            // and return all other errors as a 500. This should almost certainly be improved for
            // a real application.
            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Use(async (context, next) =>
                {
                    var error = context.Features[typeof(IExceptionHandlerFeature)] as IExceptionHandlerFeature;
                    // This should be much more intelligent - at the moment only expired 
                    // security tokens are caught - might be worth checking other possible 
                    // exceptions such as an invalid signature.

                    AddCorsHeaders(context.Response);
                    if (context.Request.Method == "OPTIONS")
                    {
                        context.Response.StatusCode = 200;
                    }
                    else if (error != null && error.Error is SecurityTokenExpiredException ||
                        (error.Error is HttpResponseException && ((HttpResponseException)error.Error).Response.StatusCode == System.Net.HttpStatusCode.Unauthorized))
                    {
                        context.Response.StatusCode = 401;
                        // What you choose to return here is up to you, in this case a simple 
                        // bit of JSON to say you're no longer authenticated.
                        context.Response.ContentType = "application/json";
                        await context.Response.WriteAsync(
                            JsonConvert.SerializeObject(
                                new { authenticated = false, tokenExpired = true }));
                    }
                    else if (error != null && error.Error != null)
                    {
                        context.Response.StatusCode = 500;
                        context.Response.ContentType = "application/json";

                        if (error.Error is HttpResponseException)
                        {
                            await context.Response.WriteAsync(
                                JsonConvert.SerializeObject
                                (new { success = false, error = ((HttpResponseException)error.Error).Response.ReasonPhrase }));
                        }
                        else
                        {
                            // TODO: Shouldn't pass the exception message straight out, change this.
                            await context.Response.WriteAsync(
                                JsonConvert.SerializeObject
                                (new { success = false, error = error.Error.Message }));
                        }
                    }
                    // We're not trying to handle anything else so just let the default 
                    // handler handle.
                    else await next();
                });
            });
        }

        private SecurityKey GetSecurityKey()
        {
            var provider = new RSACryptoServiceProvider(2048);
            var parameters = provider.ExportParameters(true);
            provider.Dispose();
            return new RsaSecurityKey(parameters);
        }
    }
}
