﻿using Microsoft.Extensions.Logging;
using System;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using StudMonk.ServiceStack.RestApi.Quartz.JobScheduler;
using StudMonk.ServiceStack.RestApi.AppConfig;

namespace StudMonk.ServiceStack.RestApi
{
    public class NotificationWorker
    {
        AppConfig.RootConfiguration _appConfiguration;

        public NotificationWorker(AppConfig.RootConfiguration appConfiguration)
        {
            _appConfiguration = appConfiguration;
        }

        public void SendNotificationForMessageKey(ILogger logger, string notificationMsgKey, params string[] values)
        {
            try
            {
                NotificationMessage message;
                _appConfiguration.NotificationMessages.Messages.TryGetValue(notificationMsgKey, out message);
                
                if(message != null)
                {
                    this.SendNotification(logger, message.Subject, String.Format(message.Body, values));
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.InnerException != null ? ex.InnerException.StackTrace : ex.StackTrace);
            }
        }

        public void SendNotification(ILogger logger, string subject, string body)
        {
            try
            {
                if (!String.IsNullOrEmpty(subject) && !String.IsNullOrEmpty(body))
                {
                    var settings = _appConfiguration.AwsSns;

                    AmazonSimpleNotificationServiceConfig config = new AmazonSimpleNotificationServiceConfig { ServiceURL = settings.ServiceURL, AuthenticationRegion = settings.Region };
                    using (var snsClient = new AmazonSimpleNotificationServiceClient(settings.AWSAccessKey, settings.AWSSecretKey, config))
                    {
                        PublishRequest publishRequest = new PublishRequest(settings.TopicArn, body, subject);
                        PublishResponse publishResult = snsClient.Publish(publishRequest);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.InnerException != null ? ex.InnerException.StackTrace : ex.StackTrace);
            }
        }
    }
}
