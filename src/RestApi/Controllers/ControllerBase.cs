﻿using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Web.Http;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNet.Authorization;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    public abstract class ControllerBase<T> : Controller where T : DomainOperations.OperationSet
    {
        protected AppConfig.RootConfiguration _appConfiguration;
        protected T _operationSet;
        protected ILogger _logger;
        protected IHttpContextAccessor _httpContextAccessor;
        protected DomainOperations.AuditLog _auditLogProcess;
        protected IAuthorizationService _authorizationService;

        protected string _validIssuer = "StudMonk", _validAudience = "all";
        protected Model.Identity CurrentUserIdentity { get; set; }

        protected ClaimsPrincipal _claimsPrinciple;

        protected ControllerBase()
        {
        }

        protected ControllerBase(
            ModelRepository.InstanceActivator repositoryInstanceActivator,
            DomainOperations.InstanceActivator domainInstanceActivator, 
            IHttpContextAccessor httpContextAccessor, 
            AppConfig.RootConfiguration appConfiguration, 
            ILoggerFactory loggerFactory,
            IAuthorizationService authorizationService)
        {
            try
            {
                _authorizationService = authorizationService;
                _appConfiguration = appConfiguration;
                _logger = loggerFactory.CreateLogger("Controller");
                _httpContextAccessor = httpContextAccessor;
                AddCorsHeaders(httpContextAccessor.HttpContext.Response.Headers);
                ValidateBearerToken();
                _operationSet = domainInstanceActivator.GetInstance<T>(this.CurrentUserIdentity);
                _auditLogProcess = domainInstanceActivator.GetInstance<DomainOperations.AuditLog, ModelRepository.AuditLogs>(repositoryInstanceActivator.GetInstance<ModelRepository.AuditLogs>(), this.CurrentUserIdentity);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        protected DomainOperations.AuditLog AuditLog { get { return _auditLogProcess; } }

        protected void ValidateBearerToken()
        {
            CurrentUserIdentity = new Model.Identity { UserId = null, Role = null };

            StringValues authorizationHeader;
            if (_httpContextAccessor.HttpContext.Request.Headers.TryGetValue("Authorization", out authorizationHeader))
            {
                if (authorizationHeader.Count > 0)
                {
                    string bearerToken = authorizationHeader.ToArray()[0].Replace("Bearer ", "");
                    if (bearerToken != "")
                    {
                        try
                        { 
                            var tokenValidationParameters = new TokenValidationParameters { IssuerSigningKey = GetSecurityKey(), ValidAudience = _validAudience, ValidIssuer = _validIssuer, ValidateSignature = false };
                            var tokenHandler = new JwtSecurityTokenHandler();
                            SecurityToken securityToken;
                            this._claimsPrinciple = tokenHandler.ValidateToken(bearerToken, tokenValidationParameters, out securityToken);
                            if (this._claimsPrinciple.Identity != null)
                            {
                                this.CurrentUserIdentity = new Model.Identity
                                {
                                    UserId = this._claimsPrinciple.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier) != null ? this._claimsPrinciple.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value : null,
                                    Role = this._claimsPrinciple.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role) != null ? this._claimsPrinciple.Claims.First(c => c.Type == ClaimTypes.Role).Value : null,
                                    TenantId = this._claimsPrinciple.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GroupSid) != null ? this._claimsPrinciple.Claims.First(c => c.Type == ClaimTypes.GroupSid).Value : null
                                };
                            }
                        }
                        catch(SecurityTokenNoExpirationException ex)
                        {
                            LogError(ex);
                            throw;
                        }
                    }
                }
            }
        }

        protected async Task<bool> AuthorizeAgainstPolicy(string policy)
        {
            if (!(await this._authorizationService.AuthorizeAsync(this._claimsPrinciple, policy)))
            {
                throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
            }
            return false;
        }

        protected SecurityKey GetSecurityKey()
        {
            var provider = new RSACryptoServiceProvider(2048);
            var parameters = provider.ExportParameters(true);
            provider.Dispose();
            return new RsaSecurityKey(parameters);
        }

        protected void AddCorsHeaders(IHeaderDictionary httpResponseHeaders)
        {
            httpResponseHeaders.Add("Access-Control-Allow-Origin", _appConfiguration.Security.Cors.AllowedOrigins);
            httpResponseHeaders.Add("Access-Control-Allow-Headers", _appConfiguration.Security.Cors.AllowedHeader);
            httpResponseHeaders.Add("Access-Control-Expose-Headers", _appConfiguration.Security.Cors.AllowedHeader);
            httpResponseHeaders.Add("Access-Control-Allow-Methods", _appConfiguration.Security.Cors.AllowedMethods);
        }

        protected void AddCorsHeaders(HttpResponseHeaders httpResponseHeaders)
        {
            httpResponseHeaders.Add("Access-Control-Allow-Origin", _appConfiguration.Security.Cors.AllowedOrigins);
            httpResponseHeaders.Add("Access-Control-Allow-Headers", _appConfiguration.Security.Cors.AllowedHeader);
            httpResponseHeaders.Add("Access-Control-Expose-Headers", _appConfiguration.Security.Cors.AllowedHeader);
            httpResponseHeaders.Add("Access-Control-Allow-Methods", _appConfiguration.Security.Cors.AllowedMethods);
        }

        protected HttpResponseMessage GetExceptionResponse(Exception ex)
        {
            string reasonPhrase = "";
            if (ex is AggregateException)
            {
                reasonPhrase = NormalizeReasonPhrase(((AggregateException)ex).Flatten().InnerException.Message);
            }
            else if (ex.InnerException != null)
            {
                reasonPhrase = NormalizeReasonPhrase(ex.InnerException.Message);
            }
            else
            {
                reasonPhrase = NormalizeReasonPhrase(ex.Message);
            }
            var httpResponse = new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError) { ReasonPhrase = reasonPhrase };
            AddCorsHeaders(httpResponse.Headers);
            return httpResponse;
        }
        private string NormalizeReasonPhrase(string exceptionMessage)
        {
            return exceptionMessage.Replace(System.Environment.NewLine, ";").Replace("[\r", "").Replace("\n", "");
        }


        protected void LogError(Exception ex)
        {
            _logger.LogError(ex.InnerException != null ? ex.InnerException.StackTrace : ex.StackTrace);
        }
    }

    public abstract class ControllerBaseWithModelRepository<T, R> : ControllerBase<T>
        where T : DomainOperations.OperationSet
        where R : ModelRepository.Repository
    {
        protected ControllerBaseWithModelRepository(
            ModelRepository.InstanceActivator repositoryInstanceActivator,
            DomainOperations.InstanceActivator domainInstanceActivator,
            IHttpContextAccessor httpContextAccessor,
            AppConfig.RootConfiguration appConfiguration,
            ILoggerFactory loggerFactory,
            IAuthorizationService authorizationService)
        {
            try
            {
                _authorizationService = authorizationService;
                _appConfiguration = appConfiguration;
                _logger = loggerFactory.CreateLogger("DefaultLogger");
                _httpContextAccessor = httpContextAccessor;
                AddCorsHeaders(httpContextAccessor.HttpContext.Response.Headers);
                var repository = repositoryInstanceActivator.GetInstance<R>();
                ValidateBearerToken();
                _operationSet = domainInstanceActivator.GetInstance<T, R>(repository, this.CurrentUserIdentity);
                _auditLogProcess = domainInstanceActivator.GetInstance<DomainOperations.AuditLog, ModelRepository.AuditLogs>(repositoryInstanceActivator.GetInstance<ModelRepository.AuditLogs>(), this.CurrentUserIdentity);
            }
            catch (SecurityTokenExpiredException ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized) { ReasonPhrase = "Token expired" });
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
