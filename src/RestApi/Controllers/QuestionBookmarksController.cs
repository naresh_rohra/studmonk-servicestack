﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using Microsoft.AspNet.Authorization;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class QuestionBookmarksController : ControllerBaseWithModelRepository<DomainOperations.QuestionBookmark, ModelRepository.QuestionBookmarks>
    {
        public QuestionBookmarksController(
                ModelRepository.InstanceActivator repositoryInstanceActivator,
                DomainOperations.InstanceActivator domainInstanceActivator,
                IHttpContextAccessor httpContextAccessor,
                AppConfig.RootConfiguration appConfiguration,
                ILoggerFactory loggerFactory,
                IAuthorizationService authorizationService
                )
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        { }

        //GET api/questionbookmarks/subject/5
        [HttpGet("subject/{id}")]
        public Model.Question[] GetForSubject(string id)
        {
            try
            {
                return _operationSet.GetForSubject(Guid.Parse(id));
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //GET api/questionbookmark/chapter/5
        [HttpGet("chapter/{id}")]
        public Model.Question[] GetForChapter(string id)
        {
            try
            {
                return _operationSet.GetForChapter(Guid.Parse(id));
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //GET api/questionbookmarks/user/5
        [HttpGet("user/{id}/{pageNo}")]
        public Model.Question[] GetForUser(string id, int pageNo)
        {
            try
            {
                return _operationSet.GetForUser(id, pageNo, _appConfiguration.GeneralSettings.BookmarksPerPage);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //GET api/questionbookmarks/user/5
        [HttpGet("user/{id}")]
        public Model.Question[] GetForUser(string id)
        {
            try
            {
                return _operationSet.GetForUser(id, 1, _appConfiguration.GeneralSettings.BookmarksPerPage);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/questionbookmarks/add
        [HttpPost("add")]
        public void Add([FromBody]string userId, [FromBody]Guid[] questionIds)
        {
            try
            {
                _operationSet.Add(userId, questionIds);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/questionbookmarks/remove
        [HttpPost("remove")]
        public void Remove([FromBody]DTO.RemoveQuestionBookmarkRequest request)
        {
            try
            {
                _operationSet.Remove(request.userId, request.questionIds);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
