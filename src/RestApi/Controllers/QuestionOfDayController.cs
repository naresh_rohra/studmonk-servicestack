﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using Microsoft.AspNet.Authorization;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class QuestionOfDayController : ControllerBaseWithModelRepository<DomainOperations.QuestionOfDay, ModelRepository.QuestionOfDays>
    {
        public QuestionOfDayController(ModelRepository.InstanceActivator repositoryInstanceActivator,
               DomainOperations.InstanceActivator domainInstanceActivator,
               IHttpContextAccessor httpContextAccessor,
               AppConfig.RootConfiguration appConfiguration,
               ILoggerFactory loggerFactory,
               IAuthorizationService authorizationService
               )
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        {
            var converter = new Newtonsoft.Json.Converters.StringEnumConverter { CamelCaseText = true };
            var options = new MvcJsonOptions();
            options.SerializerSettings.Converters.Add(converter);
        }

        // GET: api/questionofday
        [HttpGet]
        public Model.QuestionOfDay[] Get()
        {
            try
            {
                return _operationSet.GetAll();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPut]
        public Model.QuestionOfDay Put([FromForm]Model.QuestionOfDay value)
        {
            try
            {
                return _operationSet.Save(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("attempt/post")]
        public Model.QuestionOfDayAttempt PostUserAttempt([FromBody]DomainOperations.Types.QuestionOfDayAttemptRequest attemptRequest)
        {
            try
            {
                _operationSet.SaveUserAttempt(attemptRequest);
                Model.QuestionOfDayAttempt _QuestionOfDayAttempt = _operationSet.GetUserAttempt(attemptRequest.UserId, attemptRequest.QuestionOfDayId.ToString());
                this.AuditLog.Save(new Model.AuditLog()
                {
                    UserId = attemptRequest.UserId,
                    RecordedOn = DateTime.UtcNow,
                    Category = "Question",
                    Action = "QoD.PostUserAttempt",
                    ContextData = JsonConvert.SerializeObject(new
                    {
                        questionOfDayId = attemptRequest.QuestionOfDayId,
                        questionId = attemptRequest.QuestionId
                    })
                });
                return _QuestionOfDayAttempt;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("getqofday")]
        public StudMonk.ServiceStack.DomainOperations.Types.QuestionOfDayRequest GetQuestionOfTheDay(string userId, string streamId)
        {
            try
            {
                Guid streamGuid;
                Guid.TryParse(streamId, out streamGuid);
                return _operationSet.GetQuestionOfTheDay(userId, streamGuid);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet]
        public Model.QuestionOfDay[] Get(string userId, string questionId)
        {
            try
            {
                return _operationSet.GetAll();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("getquestionOfdayanalytics")]
        public StudMonk.ServiceStack.DomainOperations.Types.QuestionOfDayAnalyticsData GetQuestionOfDayAnalytics(string userId, string questionOfDayId)
        {
            try
            {
                return _operationSet.GetQuestionOfDayAnalytics(userId, questionOfDayId);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
