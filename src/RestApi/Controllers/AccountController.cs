﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web.Http;
using System.IO;
using Microsoft.Net.Http.Headers;
using StudMonk.ServiceStack.RestApi.Identity;
using Microsoft.AspNet.Authorization;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : ControllerBase<DomainOperations.Account>
    {
        private readonly UserManager<Identity.ApplicationUser> _userManager;
        private readonly SignInManager<Identity.ApplicationUser> _signInManager;
        private readonly DomainOperations.Cryptograhpy _cryptography;
        private readonly DomainOperations.EmailNotification _emailNotification;
        private readonly IAccountRepository _repository;
        private readonly ModelRepository.Users _usersRepository;

        public AccountController(
                DomainOperations.InstanceActivator domainInstanceActivator,
                ModelRepository.InstanceActivator repositoryInstanceActivator,
                IHttpContextAccessor httpContextAccessor,
                AppConfig.RootConfiguration appConfiguration,
                ILoggerFactory loggerFactory,
                UserManager<Identity.ApplicationUser> userManager,
                SignInManager<Identity.ApplicationUser> signInManager,
                DomainOperations.EmailNotification emailNotification,
                IAccountRepository repository,
                IAuthorizationService authorizationService)
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _cryptography = domainInstanceActivator.GetInstance<DomainOperations.Cryptograhpy>(this.CurrentUserIdentity);
            _usersRepository = repositoryInstanceActivator.GetInstance<ModelRepository.Users>();
            _emailNotification = emailNotification;
            _repository = repository;
        }

        [HttpPost("registerauthor")]
        //[Authorize(Policy = "UserManagement")]
        public async Task<Identity.RegistrationResult> RegisterAuthor([FromForm]string fullname, [FromForm]string email, [FromForm]string password)
        {
            await this.AuthorizeAgainstPolicy("UserManagement");
            try
            {
                var duplicate = await _userManager.FindByEmailAsync(email);
                if (duplicate == null)
                {
                    return await RegisterUser(fullname, email, "", "", password, Model.Roles.Author.Id, "Internal", email, "", DateTime.MinValue, DateTime.MaxValue);
                }
                else
                {
                    IdentityError[] errors = new IdentityError[] { new IdentityError { Description = "User with this email already registered", Code = "" } };
                    return new Identity.RegistrationResult { Result = IdentityResult.Failed(errors) };
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("registerreviewer")]
        //[Authorize(Policy = "UserManagement")]
        public async Task<Identity.RegistrationResult> RegisterReviewer([FromForm]string fullname, [FromForm]string email, [FromForm]string password)
        {
            await this.AuthorizeAgainstPolicy("UserManagement");
            try
            {
                var duplicate = await _userManager.FindByEmailAsync(email);
                if (duplicate == null)
                {
                    return await RegisterUser(fullname, email, "", "", password, Model.Roles.Reviewer.Id, "Internal", email, "", null, null);
                }
                else
                {
                    IdentityError[] errors = new IdentityError[] { new IdentityError { Description = "User with this email already registered", Code = "" } };
                    return new Identity.RegistrationResult { Result = IdentityResult.Failed(errors) };
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("import/students-registration")]
        //[Authorize(Policy = "UserManagement")]

        public async Task<DTO.StudentRegistrationImportEntry[]> ImportStudentsRegistration([FromBody] DTO.StudentRegistrationImportRequest request)
        {
            await this.AuthorizeAgainstPolicy("UserManagement");
            try
            {
                foreach (var entry in request.entries)
                {
                    var response = await this.RegisterStudent(entry.Fullname, entry.Email, entry.Password, entry.PhoneNo, string.Empty, request.tenantId, entry.ActiveFrom, entry.ActiveTill);
                    if (response.Result.Succeeded)
                    {
                        entry.StatusCode = 1;
                    }
                    else
                    {
                        entry.StatusCode = -1;
                        entry.Status = response.Result.Errors.First().Description;
                    }
                }
                return request.entries;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("register")]
        public async Task<Identity.RegistrationResult> RegisterStudent([FromForm]string fullname, [FromForm]string email, [FromForm]string password, [FromForm]string phoneNo, [FromForm]string deviceid, [FromForm]string tenantId, [FromForm]DateTime? activeFrom, [FromForm]DateTime? activeTill)
        {
            try
            {
                var duplicate = await _userManager.FindByEmailAsync(email);
                if (duplicate == null)
                {
                    var response = await RegisterUser(fullname, email, phoneNo, deviceid, password, Model.Roles.Student.Id, "Internal", email, tenantId, activeFrom, activeTill);
                    this.AuditLog.Save(new Model.AuditLog()
                    {
                        UserId = response.UserId,
                        RecordedOn = DateTime.UtcNow,
                        Category = "Account",
                        Action = "Student.Register",
                        ContextData = JsonConvert.SerializeObject(new
                        {
                            ip = Request.HttpContext.Connection.RemoteIpAddress
                        })
                    });
                    return response;
                }
                else
                {
                    IdentityError[] errors = new IdentityError[] { new IdentityError { Description = "User with this email already registered", Code = "" } };
                    return new Identity.RegistrationResult { Result = IdentityResult.Failed(errors) };
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("registerexternal")]
        public async Task<Identity.RegistrationResult> RegisterExternal([FromForm]string fullname, [FromForm]string email, [FromForm]string phoneNo, [FromForm]string loginProvider, [FromForm]string token, [FromForm]string deviceid)
        {
            try
            {
                var duplicate = await _userManager.FindByEmailAsync(email);

                if (duplicate == null)
                {
                    var password = email;
                    var result = await RegisterUser(fullname, email, phoneNo, deviceid, password, Model.Roles.Student.Id, loginProvider, token, "", null, null);
                    this.AuditLog.Save(new Model.AuditLog()
                    {
                        UserId = result.UserId,
                        RecordedOn = DateTime.UtcNow,
                        Category = "Account",
                        Action = "Student.Register",
                        ContextData = JsonConvert.SerializeObject(new
                        {
                            ip = Request.HttpContext.Connection.RemoteIpAddress
                        })
                    });
                    return result;
                }
                else
                {
                    return new Identity.RegistrationResult { Result = IdentityResult.Success, UserId = duplicate.Id, Token = GenerateBearerToken(duplicate) };
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("confirmforgotpassword")]
        public async Task<Identity.ForgotPasswordResultResponse> ConfirmForgotPassword([FromForm]string username, [FromForm]string token, [FromForm]string newPassword)
        {
            try
            {

                var forgotPwdResponseResult = new Identity.ForgotPasswordResultResponse
                {
                    Email = username,
                    RequestTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
                };

                var user = await _userManager.FindByEmailAsync(username);
                if (user != null)
                {
                    forgotPwdResponseResult.IdentityResult = await _userManager.ResetPasswordAsync(user, token, _cryptography.Hash(newPassword));
                    this.AuditLog.Save(new Model.AuditLog()
                    {
                        UserId = user.Id,
                        RecordedOn = DateTime.UtcNow,
                        Category = "Account",
                        Action = "ConfirmForgotPassword",
                        ContextData = JsonConvert.SerializeObject(new
                        {
                            succeeded = forgotPwdResponseResult.IdentityResult.Succeeded
                        })
                    });
                    return forgotPwdResponseResult;
                }
                else
                {
                    throw new Exception("User doesn't exist");
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        private bool CheckAndSaveUserAuthorizedDevice(string userId, byte noOfDevicesAllowed, string deviceId, string deviceName)
        {
            bool result = false;
            if(String.IsNullOrEmpty(deviceId) && noOfDevicesAllowed != 0)
            {
                throw new Exception("deviceId is not specified. Unauthorized device");
            }
            var userAuthorizedDevices = _usersRepository.GetUserAuthorizedDevices(userId);
            //device is already authorized
            if (userAuthorizedDevices.Any(uad => uad.Id == deviceId))
            {
                result = true;
            }
            //either current registered is less than allowed or unlimited allowed (zero)
            else if(userAuthorizedDevices.Count() < noOfDevicesAllowed || noOfDevicesAllowed == 0)
            {
                if(!String.IsNullOrEmpty(deviceId))
                { 
                    _usersRepository.SaveUserAuthorizedDevice(new Model.UserAuthorizedDevice()
                    {
                        Id = deviceId,
                        UserId = userId,
                        DeviceName = deviceName
                    });
                }
                result = true;
            }                
            return result;
        }

        [HttpGet("app-ping")]
        public bool Access()
        {
            if(this.CurrentUserIdentity != null)
            { 
                this.AuditLog.Save(new Model.AuditLog()
                {
                    UserId = this.CurrentUserIdentity.UserId,
                    RecordedOn = DateTime.UtcNow,
                    Category = "Account",
                    Action = "Access"
                });
            }
            return true;
        }

        [HttpPost("login")]
        public async Task<Identity.AuthenticationResult> Login([FromForm]string username, [FromForm]string password, bool rememberMe, [FromForm]string tenantId, [FromForm]string deviceId, [FromForm]string deviceName)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(username, _cryptography.Hash(password), rememberMe, false);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByEmailAsync(username);
                    if (user.IsLocked.HasValue && user.IsLocked.Value == true)
                    {
                        throw new Exception("Account is inactive. Please contact administrator");
                    }
                    if ((user.ActiveFrom.HasValue && DateTime.UtcNow < user.ActiveFrom.Value) || (user.ActiveTill.HasValue && DateTime.UtcNow > user.ActiveTill.Value))
                    {
                        throw new Exception("Account has expired. Please contact administrator");
                    }
                    var options = new Options()
                    {
                        Partner = "",
                        PartnerId = "",
                        NegativeMarking = false
                    };

                    if (tenantId != null)
                    {
                        if (tenantId != user.TenantId.Trim() && tenantId.Trim() != string.Empty)
                        {
                            throw new Exception("User not authorized for this partner");
                        }
                    }

                    if(user.TenantId != null)
                    {
                        var tenant = _usersRepository.GetTenant(user.TenantId);
                        if (tenant == null || !tenant.Active)
                        {
                            throw new Exception(String.Format("Partner account has been deactivated. Please contact administrator"));
                        }
                        var userRoles = await _userManager.GetRolesAsync(user);
                        if (userRoles.FirstOrDefault(rid => rid == Model.Roles.Student.Name) != null)
                        {
                            if (!CheckAndSaveUserAuthorizedDevice(user.Id, tenant.NoOfDevicesAllowed, deviceId, deviceName))
                            {
                                throw new Exception("You are not authorized to login from this device. Please contact administrator");
                            }
                        }
                        options = new Options()
                        {
                            Partner = tenant.Name,
                            PartnerId = tenant.Id,
                            NegativeMarking = tenant.NegativeMarking
                        };
                    }
                    var roles = await _userManager.GetRolesAsync(user);
                    user.Roles.Add(new IdentityUserRole<string> { RoleId = roles.Count > 0 ? roles[0] : Model.Roles.Student.Id, UserId = user.Id });
                    var securityToken = GenerateBearerToken(user);
                    var authResult = new Identity.AuthenticationResult
                    {
                        Result = result,
                        UserId = user.Id,
                        Email = user.Email,
                        FullName = user.DisplayName,
                        token = securityToken,
                        PhoneNumber = user.PhoneNumber,
                        DisplayName = user.DisplayName,
                        RegisteredOn = user.RegisteredOn.ToString("yyyy-MM-dd"),
                        Role = user.Roles.First().RoleId,
                        options = options
                    };
                    this.AuditLog.Save(new Model.AuditLog()
                    {
                        UserId = user.Id,
                        RecordedOn = DateTime.UtcNow,
                        Category = "Account",
                        Action = "Login",
                        ContextData = JsonConvert.SerializeObject(new
                        {
                            ip = Request.HttpContext.Connection.RemoteIpAddress
                        })
                    });
                    return authResult;
                }
                else
                {
                    return new Identity.AuthenticationResult
                    {
                        Result = result
                    };
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("forgot")]
        public async Task<Identity.ForgotPasswordRequestResult> ForgotPassword([FromForm]string email)
        {
            try
            {
                var forgotPwdRequestResult = new Identity.ForgotPasswordRequestResult
                {
                    Email = email,
                    RequestTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
                };
                var user = await _userManager.FindByEmailAsync(email);
                if (user == null)
                {
                    forgotPwdRequestResult.EmailAccountFound = false;
                    forgotPwdRequestResult.RecoveryToken = 0;
                    forgotPwdRequestResult.MailSend = false;
                }
                else
                {
                    var login = await _userManager.GetLoginsAsync(user);
                    if ((login.Count > 0) && (login[0].LoginProvider.ToLower() != "guestuser"))
                    {
                        if (user != null)
                        {
                            bool t = _userManager.SupportsUserSecurityStamp;
                            var code = await _userManager.GeneratePasswordResetTokenAsync(user);

                            Random rnd = new Random();
                            int myRandomNo = rnd.Next(1000, 9999);

                            AppConfig.ForgotPasswordEmailTemplate _ForgotPasswordEmailTemplate = new AppConfig.EmailTemplateConfiguration().GetForgotPasswordEmailTemplate();
                            _emailNotification.SendPasswordResetToken(user.DisplayName, myRandomNo.ToString(), user.Email, _ForgotPasswordEmailTemplate.TemplatedBody, _ForgotPasswordEmailTemplate.TemplatedSubject);

                            forgotPwdRequestResult.EmailAccountFound = true;
                            forgotPwdRequestResult.RecoveryToken = myRandomNo;
                            forgotPwdRequestResult.MailSend = true;
                            forgotPwdRequestResult.code = code;

                            this.AuditLog.Save(new Model.AuditLog()
                            {
                                UserId = user.Id,
                                RecordedOn = DateTime.UtcNow,
                                Category = "Account",
                                Action = "ForgotPassword"
                            });
                        }
                        else
                        {
                            forgotPwdRequestResult.EmailAccountFound = false;
                            forgotPwdRequestResult.RecoveryToken = 0;
                            forgotPwdRequestResult.MailSend = false;
                        }
                    }
                    else
                    {
                        forgotPwdRequestResult.EmailAccountFound = false;
                        forgotPwdRequestResult.RecoveryToken = 0;
                        forgotPwdRequestResult.MailSend = false;
                    }
                }
                return forgotPwdRequestResult;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("changepassword")]
        public async Task<Identity.ChangePasswordResult> ChangePassword([FromForm] string email, [FromForm] string oldpassword, [FromForm] string newpassword)
        {
            var authResult = new Identity.AuthenticationResult();
            var changePwdResult = new Identity.ChangePasswordResult
            {
                Email = email,
                RequestTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
            };

            if (email.ToLower() != "guest@studmonk.com")
            {
                var user = await _userManager.FindByEmailAsync(email);
                if (user != null)
                {
                    //valid old password
                    authResult.Result = await _signInManager.PasswordSignInAsync(email, _cryptography.Hash(oldpassword), false, false);
                    if (authResult.Result.Succeeded)
                    {
                        changePwdResult.IsValidUserCredentials = true;

                        var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                        changePwdResult.IdentityResult = await _userManager.ResetPasswordAsync(user, token, _cryptography.Hash(newpassword));
                        this.AuditLog.Save(new Model.AuditLog()
                        {
                            UserId = user.Id,
                            RecordedOn = DateTime.UtcNow,
                            Category = "Account",
                            Action = "ChangePassword",
                            ContextData = JsonConvert.SerializeObject(new
                            {
                                succeeded = changePwdResult.IdentityResult.Succeeded
                            })
                        });
                    }
                    else
                    {
                        changePwdResult.IsValidUserCredentials = false;
                    }
                }
                else
                {
                    changePwdResult.IsValidUserCredentials = false;
                }
            }
            else
            {
                changePwdResult.IsValidUserCredentials = false;
            }
            return changePwdResult;
        }

        byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        [HttpPost("guestlogin")]
        public async Task<Identity.RegistrationResult> GuestLogin([FromForm]string deviceId)
        {
            try
            {
                Identity.ApplicationUser user = (from q in _userManager.Users
                                                 where q.DeviceId == deviceId
                                                 select q).FirstOrDefault();

                if (user == null)
                {
                    string email = "guest_" + deviceId + "@studmonk.com";
                    var password = email;
                    var result = await RegisterUser("Guest", email, "", deviceId, password, Model.Roles.Student.Id, "GuestUser", email, "", null, null);
                    this.AuditLog.Save(new Model.AuditLog()
                    {
                        UserId = result.UserId,
                        RecordedOn = DateTime.UtcNow,
                        Category = "Account",
                        Action = "Guest.Login",
                        ContextData = JsonConvert.SerializeObject(new
                        {
                            ip = Request.HttpContext.Connection.RemoteIpAddress
                        })
                    });
                    return result;
                }
                else
                {
                    return new Identity.RegistrationResult { Result = IdentityResult.Success, UserId = user.Id, Token = GenerateBearerToken(user) };
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        private string GenerateBearerToken(Identity.ApplicationUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var signingCredentials = new SigningCredentials(GetSecurityKey(), SecurityAlgorithms.RsaSha256Signature);

            string token = "";
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, user.DisplayName));
            claims.Add(new Claim(ClaimTypes.Role, user.Roles.Count > 0 ? user.Roles.First().RoleId : Model.Roles.Student.Name));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id));
            if(!String.IsNullOrEmpty(user.TenantId))
                claims.Add(new Claim(ClaimTypes.GroupSid, user.TenantId));
            var descriptor = new SecurityTokenDescriptor
            {
                Audience = _validAudience,
                Issuer = _validIssuer,
                IssuedAt = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddDays(365 * 5),//token expiry
                SigningCredentials = signingCredentials,
                Claims = claims,
                NotBefore = DateTime.Now.AddDays(-1)
            };

            var securityToken = tokenHandler.CreateToken(descriptor.Issuer, descriptor.Audience, new ClaimsIdentity(descriptor.Claims),
                descriptor.NotBefore, descriptor.Expires,
                descriptor.IssuedAt, descriptor.SigningCredentials);
            token = tokenHandler.WriteToken(securityToken);
            return token;
        }

        private async Task<Identity.RegistrationResult> RegisterUser(string fullname, string email, string phoneNo, string deviceId, string password, string role, string loginProvider, string providerKey, string tenantId, DateTime? activeFrom, DateTime? activeTill)
        {
            fullname = (fullname == null) ? email : fullname;
            fullname = (fullname == "null") ? email : fullname;

            var userId = Guid.NewGuid().ToString();
            var user = new Identity.ApplicationUser
            {
                DisplayName = fullname,
                UserName = email,
                Email = email,
                Id = userId,
                PhoneNumber = phoneNo,
                RegisteredOn = DateTime.UtcNow,
                DeviceId = deviceId,
                TenantId = tenantId == null || tenantId.Trim() == string.Empty ? null : tenantId,
                ActiveFrom = activeFrom,
                ActiveTill = activeTill.HasValue ? activeTill.Value.AddDays(1).AddSeconds(-1) : activeTill
            };

            user.Roles.Add(new IdentityUserRole<string> { UserId = userId, RoleId = role });
            user.Logins.Add(new IdentityUserLogin<string> { LoginProvider = loginProvider, ProviderDisplayName = loginProvider, ProviderKey = providerKey, UserId = userId });

            var result = await _userManager.CreateAsync(user, _cryptography.Hash(password));

            return new Identity.RegistrationResult { Result = result, UserId = userId, Token = GenerateBearerToken(user) };
        }

        [HttpPost("updateprofile")]
        public async void PostFormData(bool savetoDisk = false)
        {
            if (!Request.HasFormContentType)
            {
                throw new Exception("input is not in valid format !");
            }
            try
            {
                IFormFile profilePic = Request.Form.Files[0];
                if (profilePic.ContentType != "image/jpeg")
                {
                    throw new Exception("profile picture is not in supported format !");
                }
                else
                {
                    string UserId = Request.Form["UserId"];
                    string DisplayName = Request.Form["DisplayName"];

                    //convert image to byte array
                    byte[] imageBytes;
                    Stream stream = profilePic.OpenReadStream();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        stream.CopyTo(ms);
                        imageBytes = ms.ToArray();
                    }

                    bool result = await _repository.SaveProfilePic(UserId, DisplayName, imageBytes);

                    //save on physical path 
                    if (savetoDisk && result)
                    {
                        var parsedContentDisposition = ContentDispositionHeaderValue.Parse(profilePic.ContentDisposition);
                        await profilePic.SaveAsAsync("./profile//" + UserId + Path.GetExtension(parsedContentDisposition.FileName.Trim('"')) + "");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("profile")]
        public async Task<Model.DTO.UserDetail> GetUserProfile([FromForm]string UserId)
        {
            try
            {
                var user = (from q in _userManager.Users
                            where q.Id == UserId
                            select q).FirstOrDefault();
                if (user == null)
                {
                    throw new Exception("Unable to process the request!");
                }
                var userProfile = await _repository.GetUserProfilePictureByIdAsync(UserId);
                string imageAsBase64 = Convert.ToBase64String(userProfile.Image);

                Model.DTO.UserDetail userDetail = new Model.DTO.UserDetail { DisplayName = user.DisplayName, ProfilePic = imageAsBase64, UserId = user.Id };
                return userDetail;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("student/enroll-to-stream")]
        public Model.StudentEnrollmentDetail EnrollStudentToStream([FromBody]DomainOperations.Types.EnrollmentRequest request)
        {
            try
            { 
                var response = _operationSet.EnrollStudent(request.UserId, request.StreamId);
                this.AuditLog.Save(new Model.AuditLog()
                {
                    UserId = request.UserId,
                    RecordedOn = DateTime.UtcNow,
                    Category = "Account",
                    Action = "EnrollToStream",
                    ContextData = JsonConvert.SerializeObject(new
                    {
                        streamId = request.StreamId
                    })
                });
                return response;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
