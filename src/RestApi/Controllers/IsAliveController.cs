﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class IsAliveController : Controller
    {
        // GET: api/values
        [HttpGet]
        public string Get()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }
    }
}
