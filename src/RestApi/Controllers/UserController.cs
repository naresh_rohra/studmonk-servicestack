﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using System.Linq;
using Microsoft.AspNet.Authorization;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase<DomainOperations.Account>
    {
        private readonly UserManager<Identity.ApplicationUser> _userManager;
        private readonly SignInManager<Identity.ApplicationUser> _signInManager;
        private readonly DomainOperations.Cryptograhpy _cryptography;
        private readonly ModelRepository.Users _usersRepository;

        public UserController(
                DomainOperations.InstanceActivator domainInstanceActivator,
                ModelRepository.InstanceActivator repositoryInstanceActivator,
                IHttpContextAccessor httpContextAccessor,
                AppConfig.RootConfiguration appConfiguration,
                ILoggerFactory loggerFactory,
                UserManager<Identity.ApplicationUser> userManager,
                SignInManager<Identity.ApplicationUser> signInManager,
                IAuthorizationService authorizationService)
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _usersRepository = repositoryInstanceActivator.GetInstance<ModelRepository.Users>();
            _cryptography = domainInstanceActivator.GetInstance<DomainOperations.Cryptograhpy>(this.CurrentUserIdentity);
        }

        private async Task<Identity.ApplicationUser> GetUserById(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            return user;
        }

        private async Task<Identity.ApplicationUser> GetUserByUsername(string username)
        {
            var user = await _userManager.FindByEmailAsync(username);
            return user;
        }

        private bool IsInvalidUser(Identity.ApplicationUser user)
        {
            return user == null || (user.LockoutEnd.HasValue && user.LockoutEnd.Value >= DateTime.UtcNow);
        }

        [HttpGet("{id}")]
        public async Task<Identity.ApplicationUserInfo> Get(string id)
        {
            return await GetInfo(id);
        }

        [HttpPost("info")]
        public async Task<Identity.ApplicationUserInfo> GetInfo([FromBody]string userId)
        {
            try
            {
                var user = GetUserById(userId).Result;
                if (IsInvalidUser(user))
                {
                    throw new Exception("User doesn't exist");
                }

                var userInfo = new Identity.ApplicationUserInfo
                {
                    Email = user.Email,
                    Fullname = user.DisplayName,
                    UserId = user.Id,
                    PhoneNo = user.PhoneNumber,
                    RegisteredOn = user.RegisteredOn,
                    TenantId = user.TenantId,
                    ActiveFrom = user.ActiveFrom,
                    ActiveTill = user.ActiveTill,
                    IsLocked = user.IsLocked.HasValue ? user.IsLocked.Value : false
                };
                userInfo.Role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
                userInfo.AuthorizedDevices = this._usersRepository.GetUserAuthorizedDevices(userId).Select(ua => new Identity.AuthorizedDeviceInfo()
                {
                    DeviceName = ua.DeviceName,
                    DeviceId = ua.Id
                }).ToArray();
                return userInfo;

            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("changeinfo-foradmin")]
        //[Authorize(Policy = "UserManagement")]
        public async Task<IdentityResult> ChangeInfoForAdmin([FromBody] DTO.ChangeInfoRequest request)
        {
            await this.AuthorizeAgainstPolicy("UserManagement");
            try
            {
                if (request.userId == null)
                    return null;

                var user = GetUserById(request.userId).Result;
                if (IsInvalidUser(user))
                {
                    throw new Exception("User doesn't exist");
                }

                //Change phone no
                var phoneChangeResult = await _userManager.SetPhoneNumberAsync(user, request.phoneNo);
                if (!phoneChangeResult.Succeeded)
                {
                    return phoneChangeResult;
                }

                if (request.userAuthorizedDevices != null)
                {
                    this._usersRepository.UpdateUserAuthorizedDevices(request.userId, request.userAuthorizedDevices.Select(ua => ua.DeviceId).ToArray(), this.CurrentUserIdentity.UserId);
                }

                user.ActiveFrom = request.activeFrom;
                user.ActiveTill = request.activeTill;
                user.IsLocked = request.locked;

                await _userManager.UpdateAsync(user);

                return phoneChangeResult;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("changeinfo")]
        public async Task<IdentityResult> ChangeInfo([FromBody]string userId, [FromBody]string phoneNo, [FromBody]Identity.AuthorizedDeviceInfo[] userAuthorizedDevices)
        {
            try
            {
                if (userId == null)
                    return null;

                var user = GetUserById(userId).Result;
                if (IsInvalidUser(user))
                {
                    throw new Exception("User doesn't exist");
                }

                //Change phone no
                var phoneChangeResult = await _userManager.SetPhoneNumberAsync(user, phoneNo);
                if (!phoneChangeResult.Succeeded)
                {
                    return phoneChangeResult;
                }

                if(userAuthorizedDevices!=null)
                {
                    this._usersRepository.UpdateUserAuthorizedDevices(userId, userAuthorizedDevices.Select(ua=>ua.DeviceId).ToArray(), this.CurrentUserIdentity.UserId);
                }

                return phoneChangeResult;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("changepassword")]
        public async Task<IdentityResult> ChangePassword([FromBody]string userId, [FromBody]string currentPassword, [FromBody]string newPassword)
        {
            try
            {
                var user = GetUserById(userId).Result;
                if (IsInvalidUser(user))
                {
                    throw new Exception("User doesn't exist");
                }

                var result = await _userManager.ChangePasswordAsync(user, _cryptography.Hash(currentPassword), _cryptography.Hash(newPassword));
                return result;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("delete")]
        public async Task<IdentityResult> Delete([FromForm]string userId)
        {
            try
            {
                var user = GetUserById(userId).Result;
                if (IsInvalidUser(user))
                {
                    throw new Exception("User doesn't exist");
                }
                var result = await _userManager.SetLockoutEndDateAsync(user, DateTime.UtcNow.AddYears(10));
                return result;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("systemusers")]
        //[Authorize(Policy = "UserManagement")]
        public async Task<Identity.ApplicationUserInfo[]> GetSystemUsers()
        {
            await AuthorizeAgainstPolicy("UserManagement");

            IQueryable<Identity.ApplicationUserInfo> users;
            try
            {
                if (this.CurrentUserIdentity.IsTenantAdministrator())
                {
                    users = _userManager.Users.Where(u => u.Roles.Count(r => r.RoleId == Model.Roles.Student.Id) > 0
                        && u.Id != this.CurrentUserIdentity.UserId
                        && u.TenantId == this.CurrentUserIdentity.TenantId)
                    .Select(u => new Identity.ApplicationUserInfo
                    {
                        UserId = u.Id,
                        Fullname = u.DisplayName,
                        Email = u.Email,
                        RegisteredOn = u.RegisteredOn,
                        PhoneNo = u.PhoneNumber,
                        Role = u.Roles.FirstOrDefault() != null ? u.Roles.FirstOrDefault().RoleId : "",
                        TenantId = u.TenantId
                    });
                }
                else
                {
                    users = _userManager.Users.Where(u => u.Roles.Count(r => r.RoleId != Model.Roles.Student.Id) > 0
                        && u.Id != this.CurrentUserIdentity.UserId)

                    .Select(u => new Identity.ApplicationUserInfo
                    {
                        UserId = u.Id,
                        Fullname = u.DisplayName,
                        Email = u.Email,
                        RegisteredOn = u.RegisteredOn,
                        PhoneNo = u.PhoneNumber,
                        Role = u.Roles.FirstOrDefault() != null ? u.Roles.FirstOrDefault().RoleId : "",
                        TenantId = u.TenantId
                    });
                }
                return users.OrderBy(u => u.Fullname).ToArray();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("changepasswordforuser")]
        public async Task<IdentityResult> ChangePasswordForUser([FromBody]string userId, [FromBody]string newPassword)
        {
            if (!this.CurrentUserIdentity.IsAdmin())
                throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);

            try
            {
                var user = GetUserById(userId).Result;
                if (IsInvalidUser(user))
                {
                    throw new Exception("User doesn't exist");
                }

                var result = await _userManager.ChangePasswordAsync(user, user.PasswordHash, _cryptography.Hash(newPassword));
                return result;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("tenants")]
        public Model.Tenant[] GetAllTenants()
        {
            try
            {
                return _usersRepository.GetAllTenants();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("get-user-ranking")]
        public Model.DTO.UserRank GetUserRanking([FromBody]Model.DTO.UserRankingRequest request)
        {
            if (!(this.CurrentUserIdentity != null))
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                if (request == null)
                {
                    request = new Model.DTO.UserRankingRequest();
                }
                return _usersRepository.GetUserRanking(request);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
