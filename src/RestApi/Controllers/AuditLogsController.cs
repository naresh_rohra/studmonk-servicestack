﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using Microsoft.AspNet.Authorization;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class AuditLogsController : ControllerBaseWithModelRepository<DomainOperations.AuditLog, ModelRepository.AuditLogs>
    {
        public AuditLogsController(
            ModelRepository.InstanceActivator repositoryInstanceActivator,
            DomainOperations.InstanceActivator domainInstanceActivator,
            IHttpContextAccessor httpContextAccessor,
            AppConfig.RootConfiguration appConfiguration,
            ILoggerFactory loggerFactory,
            IAuthorizationService authorizationService
            )
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        { }

        [HttpPost("get-user-wise-action")]
        public Model.AuditLogReport[] GetRecentUserAction([FromBody]Model.DTO.SearchAuditLogsRequest request)
        {
            if(!(this.CurrentUserIdentity != null && (this.CurrentUserIdentity.IsAdmin() || this.CurrentUserIdentity.IsAnalyst())))
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                if(request == null)
                {
                    request = new Model.DTO.SearchAuditLogsRequest();
                }
                return _operationSet.GetRecentUserAction(request);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("get-dump")]
        public Model.AuditLog[] GetDump([FromBody]Model.DTO.SearchAuditLogsRequest request)
        {
            if (!(this.CurrentUserIdentity != null && (this.CurrentUserIdentity.IsAdmin() || this.CurrentUserIdentity.IsAnalyst())))
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                if (request == null)
                {
                    request = new Model.DTO.SearchAuditLogsRequest();
                }
                return _operationSet.GetDump(request);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("get-stats")]
        public Model.AuditLogStat[] GetStats([FromBody]Model.DTO.SearchAuditLogsRequest request)
        {
            if (!(this.CurrentUserIdentity != null && (this.CurrentUserIdentity.IsAdmin() || this.CurrentUserIdentity.IsAnalyst())))
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                if (request == null)
                {
                    request = new Model.DTO.SearchAuditLogsRequest();
                }
                return _operationSet.GetAuditLogStats(request);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("get-app-logs")]
        public Model.DTO.AppLogEntry[] GetAppLogs([FromBody]Model.DTO.SearchAppLogsRequest request)
        {
            var logEntries = new List<Model.DTO.AppLogEntry>(); //[] { };
            if (!(this.CurrentUserIdentity != null && (this.CurrentUserIdentity.IsAdmin() || this.CurrentUserIdentity.IsAnalyst())))
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                if (request != null)
                {
                    var path = _appConfiguration.Logs.PathPattern.Replace("{Date}", request.Date.ToString("yyyyMMdd"));
                    if(System.IO.File.Exists(path))
                    {
                        var logLines = System.IO.File.ReadAllLines(path);
                        foreach(var line in logLines)
                        {
                            var jsonObj = JObject.Parse(line);
                            logEntries.Add(new Model.DTO.AppLogEntry() {
                                Timestamp = (DateTime) jsonObj.GetValue("Timestamp"),
                                Level = (string)jsonObj.GetValue("Level"),
                                MessageTemplate = (string)jsonObj.GetValue("MessageTemplate")
                            });
                        }
                    }
                }
                if(request.TimeFrom != null && request.TimeTo != null)
                {
                    logEntries = logEntries.Where(l => l.Timestamp.TimeOfDay >= request.TimeFrom.TimeOfDay && l.Timestamp.TimeOfDay <= request.TimeTo.TimeOfDay).ToList();
                }
                return logEntries.ToArray();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("get-full-test-top-scorers")]
        public Model.DTO.UserRank[] GetFullTestTopScorers([FromBody]Model.DTO.TopScorersRequest request)
        {
            if (!(this.CurrentUserIdentity != null && (this.CurrentUserIdentity.IsAdmin() || this.CurrentUserIdentity.IsAnalyst())))
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                if (request == null)
                {
                    request = new Model.DTO.TopScorersRequest();
                }
                return _operationSet.GetFullTestTopScorers(request);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("get-full-test-scorers-for-user")]
        public Model.DTO.UserTestScore[] GetFullTestScoresForUser([FromBody]Model.DTO.UserScoresRequest request)
        {
            if (!(this.CurrentUserIdentity != null && (this.CurrentUserIdentity.IsAdmin() || this.CurrentUserIdentity.IsAnalyst())))
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                if (request == null)
                {
                    request = new Model.DTO.UserScoresRequest();
                }
                return _operationSet.GetFullTestScoresForUser(request);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
