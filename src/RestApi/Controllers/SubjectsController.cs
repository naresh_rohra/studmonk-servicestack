﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using Microsoft.AspNet.Authorization;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class SubjectsController : ControllerBaseWithModelRepository<DomainOperations.Subject, ModelRepository.Subjects>
    {
        public SubjectsController(ModelRepository.InstanceActivator repositoryInstanceActivator,
                DomainOperations.InstanceActivator domainInstanceActivator,
                IHttpContextAccessor httpContextAccessor,
                AppConfig.RootConfiguration appConfiguration,
                ILoggerFactory loggerFactory,
                IAuthorizationService authorizationService)
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        {
            try
            {
                var converter = new Newtonsoft.Json.Converters.StringEnumConverter { CamelCaseText = true };
                var options = new MvcJsonOptions();
                options.SerializerSettings.Converters.Add(converter);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        // GET: api/subjects
        [HttpGet]
        public Model.Subject[] Get()
        {
            try
            {
                return _operationSet.GetAll();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("stream/{id}")]
        public Model.Subject[] GetForStream(Guid id)
        {
            try
            {
                return _operationSet.GetForStream(id);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // GET: api/subjects/{id}/concepts
        [HttpGet("{id}/concepts")]
        public Model.DTO.Concept[] Get(Guid id)
        {
            try
            {
                return _operationSet.GetAllConcepts(id);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // GET api/subjects/5
        [HttpGet("{id}")]
        public Model.Subject Get(string id)
        {
            try
            {
                return _operationSet.Get(Guid.Parse(id));
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/subjects
        [HttpPost]
        public Model.Subject Post([FromForm]Model.Subject value)
        {
            try
            {
                return _operationSet.Save(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // PUT api/subjects
        [HttpPut]
        public Model.Subject Put([FromForm]Model.Subject value)
        {
            try
            {
                return _operationSet.Save(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // DELETE api/subjects/5
        [HttpDelete("{id}")]
        public bool Delete(string id)
        {
            try
            {
                _operationSet.Delete(Guid.Parse(id));
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
