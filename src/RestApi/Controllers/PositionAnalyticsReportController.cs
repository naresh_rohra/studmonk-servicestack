﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using Microsoft.AspNet.Authorization;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class PositionAnalyticsReportController : ControllerBaseWithModelRepository<DomainOperations.PositionAnalyticReport, ModelRepository.PositionAnalyticReports>
    {
        public PositionAnalyticsReportController(
             ModelRepository.InstanceActivator repositoryInstanceActivator,
             DomainOperations.InstanceActivator domainInstanceActivator,
             IHttpContextAccessor httpContextAccessor,
             AppConfig.RootConfiguration appConfiguration,
             ILoggerFactory loggerFactory,
             IAuthorizationService authorizationService
             )
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        {
            var converter = new Newtonsoft.Json.Converters.StringEnumConverter { CamelCaseText = true };
            var options = new MvcJsonOptions();
            options.SerializerSettings.Converters.Add(converter);
        }

        [HttpGet("getpositionanalytics")]
        public Model.PositionAnalyticReport GetPositionAnalyticReport(string userId, string subjectId)
        {
            try
            {
                return _operationSet.GetPositionAnalyticReport(userId, subjectId);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
