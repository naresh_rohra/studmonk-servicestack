﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class TestsController : ControllerBaseWithModelRepository<DomainOperations.Test, ModelRepository.Tests>
    {
        public TestsController(ModelRepository.InstanceActivator repositoryInstanceActivator,
                DomainOperations.InstanceActivator domainInstanceActivator,
                IHttpContextAccessor httpContextAccessor,
                AppConfig.RootConfiguration appConfiguration,
                ILoggerFactory loggerFactory,
                IAuthorizationService authorizationService)
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        {

        }

        //GET api/tests/test/5
        [HttpGet("test/{testId}")]
        public Model.Test Get(string testId)
        {
            try
            {
                return _operationSet.Get(Guid.Parse(testId));
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/tests/getnew
        [HttpPost("getnew")]
        public Model.Test GetNew([FromForm]DomainOperations.Types.NewTestRequest request)
        {
            try
            {
                Guid[] normalizedChapterIds = new Guid[0];
                try
                {
                    if (request.ChapterIds[0].IndexOf(",") >= 0)
                    {
                        request.ChapterIds = request.ChapterIds[0].Split(',');
                    }
                    request.ChapterIds = request.ChapterIds.Where(id => id != null && id.Trim() != String.Empty).ToArray();
                    request.ChapterIdList = (from id in request.ChapterIds select Guid.Parse(id.Trim())).ToArray();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.StackTrace);
                }
                var test = _operationSet.GetNew(request);
                if (this.CurrentUserIdentity != null)
                {
                    this.AuditLog.Save(new Model.AuditLog()
                    {
                        UserId = this.CurrentUserIdentity.UserId,
                        RecordedOn = DateTime.UtcNow,
                        Category = "Test",
                        Action = "Personalised.New",
                        ContextData = JsonConvert.SerializeObject(new
                        {
                            testId = test.Id
                        })
                    });
                }
                return test;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }


        [HttpPost("attempt/post")]
        public void PostUserAttempt([FromBody]DomainOperations.Types.TestAttemptRequest attemptRequest)
        {
            try
            {
                _operationSet.SaveUserAttempt(attemptRequest);
                if (this.CurrentUserIdentity != null)
                {
                    this.AuditLog.Save(new Model.AuditLog()
                    {
                        UserId = this.CurrentUserIdentity.UserId,
                        RecordedOn = DateTime.UtcNow,
                        Category = "Test",
                        Action = "Personalised.PostAttempt",
                        ContextData = JsonConvert.SerializeObject(new
                        {
                            testId = attemptRequest.TestId
                        })
                    });
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/tests/attempt/postfulltest
        [HttpPost("attempt/postfulltest")]
        public string PostAttemptForFullTest([FromBody]DomainOperations.Types.FullTestAttemptRequest fulltesAttemptRequest)
        {
            try
            {
                var result = _operationSet.SaveUserAttemptForFullTest(fulltesAttemptRequest.fullTestAttemptRequest, new Guid(fulltesAttemptRequest.attemptId));
                if (this.CurrentUserIdentity != null)
                {
                    this.AuditLog.Save(new Model.AuditLog()
                    {
                        UserId = this.CurrentUserIdentity.UserId,
                        RecordedOn = DateTime.UtcNow,
                        Category = "Test",
                        Action = "FullTest.PostAttempt",
                        ContextData = JsonConvert.SerializeObject(new
                        {
                            testId = fulltesAttemptRequest.fullTestAttemptRequest.TestId
                        })
                    });
                }
                return result;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // PUT api/tests
        [HttpPut]
        public Model.Test Put([FromForm]Model.Test value)
        {
            try
            {
                return _operationSet.Save(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // DELETE api/tests/5
        [HttpDelete("{id}")]
        public bool Delete(string id)
        {
            try
            {
                _operationSet.Delete(Guid.Parse(id));
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //api/tests/fulltests
        [HttpGet("fulltests")]
        public Model.DTO.FullTest[] GetFullTestList(string lastSyncDate, string streamId, string relevance)
        {
            try
            {
                return _operationSet.GetAllFullTest(_appConfiguration.Test.NeetPerQuestionDuration, _appConfiguration.Test.PerQuestionDuration, _appConfiguration.Test.BreakDurationInMinutes, lastSyncDate, streamId, this.CurrentUserIdentity.TenantId, relevance);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }


        //GET api/tests/fulltest/5
        [HttpGet("fulltest/{testId}")]
        public Model.DTO.FullTest GetFullTest(string testId)
        {
            try
            {
                return _operationSet.GetFullTest(Guid.Parse(testId), this.CurrentUserIdentity.UserId, _appConfiguration.Test.PerQuestionDuration, _appConfiguration.Test.BreakDurationInMinutes, _appConfiguration.Test.NeetPerQuestionDuration);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //GET api/tests/fulltest/5
        [HttpPost("fulltest/save-details")]
        public bool SaveFullTestDetails([FromBody]Model.DTO.FullTest fullTest)
        {
            try
            {
                _operationSet.SaveFullTestDetails(fullTest);
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //api/tests/fulltest
        [HttpPost("fulltest")]
        public Model.DTO.FullTest GetFullTest([FromForm]string testId, [FromForm]string userId)
        {
            try
            {
                Model.DTO.FullTest fullTest = new Model.DTO.FullTest();
                fullTest = _operationSet.GetFullTest(new Guid(testId), userId, _appConfiguration.Test.PerQuestionDuration, _appConfiguration.Test.BreakDurationInMinutes, _appConfiguration.Test.NeetPerQuestionDuration);
                return fullTest;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //api/tests/rate
        [HttpPost("rate")]
        public Model.UserTestRating SaveUserTestRating([FromBody]Model.UserTestRating rating)
        {
            try
            {
                return _operationSet.SaveUserTestRating(rating);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("history/{userId}")]
        public Model.Test[] GetTestOfUser(string userId)
        {
            try
            {
                return _operationSet.GetTestOfUser(userId, 0, _appConfiguration.GeneralSettings.TestsTakenPerPage);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("user-attempt/{userId}/{attemptid}")]
        public Model.Test GetTestWithUserAttempt(string userId, Guid attemptId)
        {
            try
            {
                return _operationSet.GetTestWithUserAttempt(userId, attemptId);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //GET api/tests/history/5
        [HttpGet("history/{userId}/{pageNo}")]
        public Model.Test[] GetTestOfUser(string userId, int pageNo)
        {
            try
            {
                return _operationSet.GetTestOfUser(userId, pageNo, _appConfiguration.GeneralSettings.TestsTakenPerPage);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("formats/for-stream/{streamId}")]
        public Model.TestFormat[] GetTestFormatsForStream(Guid streamId)
        {
            try
            {
                return _operationSet.GetTestFormatsForStream(streamId);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("formats")]
        public Model.TestFormat[] GetTestFormats()
        {
            try
            {
                return _operationSet.GetTestFormatsForStream(Guid.Empty);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("get-full-test-preview")]
        //[Authorize(Policy = "FullTestManagement")]
        public async Task<ModelRepository.Types.FullTestPreviewResponse> GetFullTestPreview([FromBody]ModelRepository.Types.GenerateFullTestRequest request)
        {
            await this.AuthorizeAgainstPolicy("FullTestManagement");
            try
            {
                return _operationSet.GetFullTestPreview(request);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("generate-full-test")]
        //[Authorize(Policy = "FullTestManagement")]
        public async Task<bool> GenerateFullTest([FromBody]ModelRepository.Types.GenerateFullTestRequest request)
        {
            await this.AuthorizeAgainstPolicy("FullTestManagement");
            try
            {
                _operationSet.GenerateFullTest(request);
                if (request.SendNotification && String.IsNullOrEmpty(request.TenantId))
                {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                    Task.Factory.StartNew(() =>
                    {
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        if (this.CurrentUserIdentity != null)
                        {
                            this.AuditLog.Save(new Model.AuditLog()
                            {
                                UserId = this.CurrentUserIdentity.UserId,
                                RecordedOn = DateTime.UtcNow,
                                Category = "Notification",
                                Action = "NewFullTest",
                                ContextData = JsonConvert.SerializeObject(new
                                {
                                    testName = request.TestName,
                                    streamId = request.StreamId
                                })
                            });
                        }
                        new NotificationWorker(_appConfiguration).SendNotificationForMessageKey(_logger, "NewTest", request.TestName, request.StreamId.ToString());
                    });
                }
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpGet("sponsors")]
        public Model.Sponsor[] GetAllSponsors()
        {
            try
            {
                return _operationSet.GetAllSponsors();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
