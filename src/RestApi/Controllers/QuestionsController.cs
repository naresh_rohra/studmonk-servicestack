﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using System.Linq;
using StudMonk.ServiceStack.DomainOperations.Types;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using System.Text.RegularExpressions;
using Microsoft.AspNet.Authorization;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class QuestionsController : ControllerBaseWithModelRepository<DomainOperations.Question, ModelRepository.Questions>
    {
        public QuestionsController(ModelRepository.InstanceActivator repositoryInstanceActivator,
                DomainOperations.InstanceActivator domainInstanceActivator,
                IHttpContextAccessor httpContextAccessor,
                AppConfig.RootConfiguration appConfiguration,
                ILoggerFactory loggerFactory,
                IAuthorizationService authorizationService)
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        { }

        //GET api/questions/chapter/5
        [HttpPost("search/chapter/{chapterId}")]
        public Model.Question[] SearchForChapter(string chapterId, [FromForm]string searchText, [FromForm]string questionId)
        {
            try
            {
                if (searchText != null)
                {
                    var normalizedSearchText = Regex.Replace(searchText, "<.*?>", string.Empty);
                    var currentQuestion = Guid.Empty;
                    Guid.TryParse(questionId, out currentQuestion);
                    return _operationSet.SearchForChapter(Guid.Parse(chapterId), normalizedSearchText.Length <= 25 ? normalizedSearchText : normalizedSearchText.Substring(0, 25), currentQuestion).Questions;
                }
                return new Model.Question[] { };
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //GET api/questions/chapter/5/1
        [HttpGet("chapter/{chapterId}/{qualityStatus}/{searchText}/{conceptText}/{relevanceText}/{sortBy}/{sortOrder}/{pageNumber}")]
        public Model.DTO.QuestionsList GetForSubjectChaptersForQualityStatus(string chapterId, Model.QualityStatus qualityStatus, int pageNumber, string sortBy, string sortOrder, string searchText, string conceptText, string relevanceText)
        {
            try
            {
                var request = new ModelRepository.Types.QuestionListRequest()
                {
                    ChapterIds = new Guid[] { Guid.Parse(chapterId) },
                    QualityStatus = qualityStatus,
                    PageNumber = pageNumber,
                    SortBy = sortBy,
                    SortOrder = sortOrder == "asc" ? ModelRepository.Types.SortOrder.Ascending : ModelRepository.Types.SortOrder.Descending,
                    SearchText = searchText == "[null]" ? "" : searchText,
                    ConceptText = conceptText == "[null]" ? "" : conceptText,
                    RelevanceText = relevanceText == "[null]" ? "" : relevanceText
                };
                return GetForSubjectChaptersForQualityStatusWithPagination(request);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        private Model.DTO.QuestionsList GetForSubjectChaptersForQualityStatusWithPagination(ModelRepository.Types.QuestionListRequest request)
        {
            return _operationSet.GetForChapter(request);
        }

        //GET api/questions/subject/5/1
        [HttpGet("subject/{subjectId}/{qualityStatus}/{searchText}/{conceptText}/{relevanceText}/{sortBy}/{sortOrder}/{pageNumber}")]
        public Model.DTO.QuestionsList GetForSubjectChaptersWithDifficultyLevel(string subjectId, Model.QualityStatus qualityStatus, int pageNumber, string sortBy, string sortOrder, string searchText, string conceptText, string relevanceText)
        {
            try
            {
                return _operationSet.GetForSubjectChaptersWithDifficultyLevel(new ModelRepository.Types.QuestionListRequest()
                {
                    SubjectId = Guid.Parse(subjectId),
                    QualityStatus = qualityStatus,
                    PageNumber = pageNumber,
                    SortBy = sortBy,
                    SortOrder = sortOrder == "asc" ? ModelRepository.Types.SortOrder.Ascending : ModelRepository.Types.SortOrder.Descending,
                    SearchText = searchText == "[null]" ? "" : searchText,
                    ConceptText = conceptText == "[null]" ? "" : conceptText,
                    RelevanceText = relevanceText == "[null]" ? "" : relevanceText
                });
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // GET api/questions/5
        [HttpGet("{id}")]
        public Model.Question Get(string id)
        {
            try
            {
                return _operationSet.Get(Guid.Parse(id));
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/questions
        [HttpPost]
        public Model.Question Post([FromForm]Model.Question value)
        {
            try
            {
                return _operationSet.Save(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("saveandapprove")]
        public Model.Question SaveAndApprove([FromForm]Model.Question value)
        {
            try
            {
                return _operationSet.SaveAndApprove(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/questions/import
        [HttpPost("import")]
        public bool ImportQuestions([FromForm] QuestionImportRequest questionImportReq)
        {
            try
            {
                return _operationSet.ImportQuestions(questionImportReq);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // PUT api/questions
        [HttpPut]
        public Model.Question Put([FromForm]Model.Question value)
        {
            try
            {
                return _operationSet.Save(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // DELETE api/questions/5
        [HttpDelete("{id}")]
        public bool Delete(string id)
        {
            try
            {
                _operationSet.Delete(Guid.Parse(id));
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/questions/delete
        [HttpPost("delete")]
        public bool DeleteWithPost([FromForm]string id)
        {
            try
            {
                _operationSet.Delete(Guid.Parse(id));
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [HttpPost("deletemany")]
        public bool DeleteWithPost([FromForm]string[] ids)
        {
            try
            {
                _operationSet.Delete((from id in ids select Guid.Parse(id)).ToArray());
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/questions/markasneedscorrection
        [HttpPost("markasneedscorrection")]
        public bool MarkAsNeedsCorrection([FromForm]string[] ids)
        {
            try
            {
                _operationSet.MarkQuestionsAsNeedsCorrection((from id in ids select Guid.Parse(id)).ToArray());
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/questions/markasreviewed
        [HttpPost("markasreviewed")]
        public bool MarkAsReviewed([FromForm]string[] ids)
        {
            try
            {
                _operationSet.MarkQuestionsAsReviewed((from id in ids select Guid.Parse(id)).ToArray());
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/questions/markasreviewed
        [HttpPost("markasnotreviewed")]
        public bool MarkAsNotReviewed([FromForm]string[] ids)
        {
            try
            {
                _operationSet.MarkQuestionsAsNotReviewed((from id in ids select Guid.Parse(id)).ToArray());
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //api/questions/tags/get-all-concept-tags
        [HttpGet("tags/get-all-concept-tags")]
        public string[] GetAllConceptTags()
        {
            try
            {
                return _operationSet.GetAllConceptTags();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //api/questions/tags/get-all-relevance-tags
        [HttpGet("tags/get-all-relevance-tags")]
        public string[] GetAllRelevanceTags()
        {
            try
            {
                return _operationSet.GetAllRelevanceTags();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        //api/questions/flag
        [HttpPost("flag")]
        public Model.UserQuestionFlag SaveQuestionFlag([FromBody]Model.UserQuestionFlag questionFlag)
        {
            try
            {
                return _operationSet.SaveUserQuestionFlag(questionFlag);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

    }
}
