﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNet.Authorization;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class ClientController : ControllerBase<DomainOperations.Client>
    {
        public ClientController(
                ModelRepository.InstanceActivator repositoryInstanceActivator,
                DomainOperations.InstanceActivator domainInstanceActivator,
                IHttpContextAccessor httpContextAccessor,
                AppConfig.RootConfiguration appConfiguration,
                ILoggerFactory loggerFactory,
                IAuthorizationService authorizationService)
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        { }

        // GET: api/client
        [Route("settings")]
        [HttpGet]
        public Model.Settings.Client Get()
        {
            try
            {
                return _operationSet.GetSettings();
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        [Route("auth")]
        [HttpPost]
        public bool Authenticate([FromForm]string username, [FromForm]string password)
        {
            try
            {
                var builder = new ConfigurationBuilder()
                .AddJsonFile("identity-store.json");
                var configurationRoot = builder.Build();
                var identities = configurationRoot.GetSection("identities").GetChildren();
                foreach (var identity in identities)
                {
                    if (identity.Get<string>("username") == username && identity.Get<string>("password") == password)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
