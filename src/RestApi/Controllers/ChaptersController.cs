﻿using System;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using Microsoft.AspNet.Authorization;

namespace StudMonk.ServiceStack.RestApi.Controllers
{
    [Route("api/[controller]")]
    public class ChaptersController : ControllerBaseWithModelRepository<DomainOperations.Chapter, ModelRepository.Chapters>
    {
        public ChaptersController(
            ModelRepository.InstanceActivator repositoryInstanceActivator,
            DomainOperations.InstanceActivator domainInstanceActivator,
            IHttpContextAccessor httpContextAccessor,
            AppConfig.RootConfiguration appConfiguration,
            ILoggerFactory loggerFactory,
            IAuthorizationService authorizationService
            )
            : base(repositoryInstanceActivator, domainInstanceActivator, httpContextAccessor, appConfiguration, loggerFactory, authorizationService)
        { }

        //GET api/chapters/subject/5
        [HttpGet("subject/{id}")]
        public Model.Chapter[] GetForSubject(string id)
        {
            try
            {
                return _operationSet.GetAllForSubject(Guid.Parse(id));
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // GET api/chapters/5
        [HttpGet("{id}")]
        public Model.Chapter Get(string id)
        {
            try
            {
                return _operationSet.Get(Guid.Parse(id));
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/chapters
        [HttpPost]
        public Model.Chapter Post([FromForm]Model.Chapter value)
        {
            try
            {
                return _operationSet.Save(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // PUT api/chapters
        [HttpPut]
        public Model.Chapter Put([FromForm]Model.Chapter value)
        {
            try
            {
                return _operationSet.Save(value);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // DELETE api/chapters/5
        [HttpDelete("{id}")]
        public bool Delete(string id)
        {
            try
            {
                _operationSet.Delete(Guid.Parse(id));
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }

        // POST api/chapters/delete
        [HttpPost("delete")]
        public bool DeleteWithPost([FromForm]string id)
        {
            try
            {
                _operationSet.Delete(Guid.Parse(id));
                return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw new HttpResponseException(GetExceptionResponse(ex));
            }
        }
    }
}
