﻿using Microsoft.Extensions.Configuration;
using System;
using StudMonk.ServiceStack.RestApi.Quartz.JobScheduler;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.RestApi.AppConfig
{
    public class RootConfiguration
    {
        public RootConfiguration(IConfigurationRoot configurationRoot)
        {
            GeneralSettings = new GeneralSettings(configurationRoot.GetSection("GeneralSettings"));
            Security = new SecuritySettings(configurationRoot.GetSection("Security"));
            Logs = new LogsSettings(configurationRoot.GetSection("Logs"));
            Data = new DataSettings(configurationRoot.GetSection("Data"));
            Email = new EmailSettings(configurationRoot.GetSection("Email"));
            AwsSns = new AwsSnsSettings(configurationRoot.GetSection("AwsSns"));
            Test = new TestSettings(configurationRoot.GetSection("Test"));
            NotificationMessages = new NotificationMessagesSettings(configurationRoot.GetSection("NotificationMessages"));
        }

        public GeneralSettings GeneralSettings { get; set; }

        public NotificationMessagesSettings NotificationMessages { get; set; }

        public SecuritySettings Security { get; set; }

        public LogsSettings Logs { get; set; }

        public DataSettings Data { get; set; }

        public EmailSettings Email { get; set; }

        public AwsSnsSettings AwsSns { get; set; }

        public TestSettings Test { get; set; }
    }

    public class NotificationMessagesSettings
    {
        public Dictionary<string, NotificationMessage> Messages = new Dictionary<string, NotificationMessage>();

        public NotificationMessagesSettings(IConfigurationSection configSection)
        {
            foreach(var item in configSection.GetChildren())
            {
                Messages.Add(item.Key, item.Get<NotificationMessage>());
            }
        }
    }

    public class NotificationMessage
    {
        public string Subject { get; set; }

        public string Body { get; set; }
    }


    public class DataSettings
    {
        public DataSettings(IConfigurationSection configSection)
        {
            DefaultConnection = new ConnectionSettings { ConnectionString = configSection.Get<String>("DefaultConnection:ConnectionString") };            
        }

        public ConnectionSettings DefaultConnection { get; set; }

        public class ConnectionSettings
        {
            public string ConnectionString { get; set; }
        }
    }

    public class SecuritySettings
    {
        public SecuritySettings(IConfigurationSection configSection)
        {
            Cors = new CorsSettings
            {
                AllowedHeader = configSection.Get<string>("Cors:AllowedHeader"),
                AllowedMethods = configSection.Get<string>("Cors:AllowedMethods"),
                AllowedOrigins = configSection.Get<string>("Cors:AllowedOrigins"),
                AllowCredentials = configSection.Get<bool>("Cors:AllowCredentials")
            };

        }

        public CorsSettings Cors;

        public class CorsSettings
        {
            public string AllowedHeader;
            public string AllowedMethods;
            public string AllowedOrigins;
            public bool AllowCredentials;
        }
    }

    public class LogsSettings
    {
        public LogsSettings(IConfigurationSection configSection)
        {
            PathPattern = configSection.Get<string>("PathPattern");
        }

        public string PathPattern { get; set; }
    }

    public class TestSettings
    {
        public TestSettings(IConfigurationSection configSection)
        {
            PerQuestionDuration = configSection.Get<int>("PerQuestionDuration");
            BreakDurationInMinutes = configSection.Get<int>("BreakDurationInMinutes");
            NeetPerQuestionDuration = configSection.Get<int>("NeetPerQuestionDuration");
        }

        public int PerQuestionDuration { get; set; }

        public int BreakDurationInMinutes { get; set; }

        public int NeetPerQuestionDuration { get; set; }
    }

    public class EmailSettings
    {
        public string Server { get; set; }

        public int Port { get; set; }

        public string FromEmailAddr { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public EmailSettings(IConfigurationSection configSection)
        {
            Server = configSection.Get<string>("Server");
            Port = configSection.Get<int>("Port");
            FromEmailAddr = configSection.Get<string>("FromEmailAddr");
            Username = configSection.Get<string>("Username");
            Password = configSection.Get<string>("Password");
        }
    }

    public class ForgotPasswordEmailTemplate
    {
        public string TemplatedBody { get; set; }

        public string TemplatedSubject { get; set; }
    }

    public class EmailTemplateConfiguration
    {
        IConfigurationRoot _ConfigurationRoot { get; set; }

        public ForgotPasswordEmailTemplate GetForgotPasswordEmailTemplate()
        {
            _ConfigurationRoot = new ConfigurationBuilder().AddJsonFile("email-templates.json").Build();
            ForgotPasswordEmailTemplate _ForgotPasswordEmailTemplate = new ForgotPasswordEmailTemplate
            {
                TemplatedBody = _ConfigurationRoot.Get<string>("ForgotPassword:Body"),
                TemplatedSubject = _ConfigurationRoot.Get<string>("ForgotPassword:Subject")
            };
            return _ForgotPasswordEmailTemplate;
        }
    }

    public class GeneralSettings
    {
        public GeneralSettings(IConfigurationSection configSection)
        {
            BookmarksPerPage = configSection.Get<Int32>("BookmarksPerPage");
            TestsTakenPerPage = configSection.Get<Int32>("TestsTakenPerPage");
        }

        public Int32 BookmarksPerPage { get; set; }

        public Int32 TestsTakenPerPage { get; set; }
    }
}
