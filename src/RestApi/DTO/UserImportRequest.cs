﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.RestApi.DTO
{
    public class StudentRegistrationImportRequest
    {
        public string tenantId;
        public StudentRegistrationImportEntry[] entries;
    }

    public class StudentRegistrationImportEntry
    {
        public string Fullname;
        public string Email;
        public string Password;
        public string PhoneNo;
        public DateTime? ActiveFrom;
        public DateTime? ActiveTill;
        public string Stream;
        public Int16 StatusCode;
        public string Status;
    }
}
