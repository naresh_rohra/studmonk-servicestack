﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.RestApi.DTO
{
    public class RemoveQuestionBookmarkRequest
    {
        public string userId;
        public Guid[] questionIds;
    }
}
