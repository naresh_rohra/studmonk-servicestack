﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.RestApi.DTO
{
    public class ChangeInfoRequest
    {
        public string userId;
        public string phoneNo;
        public DateTime? activeFrom;
        public DateTime? activeTill;
        public bool locked;
        public Identity.AuthorizedDeviceInfo[] userAuthorizedDevices;
    }
}
