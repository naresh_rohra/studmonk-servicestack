﻿using Microsoft.Extensions.Configuration;

namespace StudMonk.ServiceStack.RestApi.Quartz.JobScheduler
{
    public class AwsSnsSettings
    {
        public string AWSAccessKey { get; set; }

        public string AWSSecretKey { get; set; }

        public string TopicArn { get; set; }

        public string Region { get; set; }

        public string ServiceURL { get; set; }

        public int QuestionOfDayTriggerIntervalInHours { get; set;}

        public AwsSnsSettings(IConfigurationSection configSection)
        {
            AWSAccessKey = configSection.Get<string>("AWSAccessKey");
            AWSSecretKey = configSection.Get<string>("AWSSecretKey");
            TopicArn = configSection.Get<string>("TopicArn");
            Region = configSection.Get<string>("Region");
            ServiceURL = configSection.Get<string>("ServiceURL");
            QuestionOfDayTriggerIntervalInHours =int.Parse(configSection.Get<string>("QuestionOfDayTriggerIntervalInHours"));
            SNSJobScheduler.Start(this);
        }

        public AwsSnsSettings(string awsAccessKey, string awsSecretKey, string topicArn, string region, string serviceURL,int questionOfDayTriggerIntervalInHours)
        {
            AWSAccessKey = awsAccessKey;
            AWSSecretKey = awsSecretKey;
            TopicArn = topicArn;
            Region = region;
            ServiceURL = serviceURL;
            QuestionOfDayTriggerIntervalInHours = questionOfDayTriggerIntervalInHours;
            SNSJobScheduler.Start(this);
        }
    }
}
