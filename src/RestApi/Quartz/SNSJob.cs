﻿using System;
using Quartz;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Microsoft.Extensions.Logging;

namespace StudMonk.ServiceStack.RestApi.Quartz.JobScheduler
{
    class SNSJob : IJob
    {
        public AwsSnsSettings AwsSnsSettings { get; set; }

        public SNSJob()
        {
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                const string Msg = @"{'type':'QOD','title':'question of the day','message':'Test your skill with new question everyday','qid':123}";
                AwsSnsSettings = (AwsSnsSettings)context.JobDetail.JobDataMap["Settings"];

                AmazonSimpleNotificationServiceConfig config = new AmazonSimpleNotificationServiceConfig { ServiceURL = AwsSnsSettings.ServiceURL, AuthenticationRegion = AwsSnsSettings.Region };
                AmazonSimpleNotificationServiceClient snsClient = new AmazonSimpleNotificationServiceClient(AwsSnsSettings.AWSAccessKey, AwsSnsSettings.AWSSecretKey, config);

                PublishRequest publishRequest = new PublishRequest(AwsSnsSettings.TopicArn, Msg, "Question of the day");
                PublishResponse publishResult = snsClient.Publish(publishRequest);

                snsClient.Dispose();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
