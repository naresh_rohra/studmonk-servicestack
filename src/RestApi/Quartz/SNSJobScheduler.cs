﻿using Quartz;
using Quartz.Impl;
using System;

namespace StudMonk.ServiceStack.RestApi.Quartz.JobScheduler
{
    class SNSJobScheduler
    {
        public static void Start(AwsSnsSettings settings)
        {
            StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = schedulerFactory.GetScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<SNSJob>().Build();

            var trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule(s => s
                    .OnEveryDay()
                    .WithIntervalInMinutes(1)
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(08, 00))
                    .EndingDailyAfterCount(1)                   
                )
                .Build();

            job.JobDataMap["Settings"] = settings;
            scheduler.ScheduleJob(job, trigger);
        }
    }
}
