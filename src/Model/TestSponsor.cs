﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestSponsor")]
    public class TestSponsor : ModelWithTracking<Guid>
    {
        public Guid TestId { get; set; }
        public Guid SponsorId { get; set; }

        [NotMapped]
        [JsonIgnore]
        public override Guid Id { get; set; }
    }
}
