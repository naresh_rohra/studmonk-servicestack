﻿namespace StudMonk.ServiceStack.Model.DTO
{
    public class QuestionsList
    {
        public Model.Question[] Questions { get; set; }

        public int TotalQuestions { get; set; }
    }
}
