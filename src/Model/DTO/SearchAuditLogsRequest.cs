﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.Model.DTO
{
    public class SearchAuditLogsRequest
    {
        public string UserId { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string TenantId { get; set; }

        public string Category { get; set; }

        public string Action { get; set; }

        public DateTime RecordedFrom { get; set; }

        public DateTime RecordedTo { get; set; }

        public DateTime FirstRecordedFrom { get; set; }

        public DateTime FirstRecordedTo { get; set; }

        public DateTime RecentRecordedFrom { get; set; }

        public DateTime RecentRecordedTo { get; set; }

        public string Frequency { get; set; }
    }

    public class SearchAppLogsRequest
    {
        public DateTime Date;

        public DateTime TimeFrom;

        public DateTime TimeTo;

        public string Type;
    }

    public class AppLogEntry
    {
        public DateTime Timestamp;

        public string Level;

        public string MessageTemplate;

        public string Properties;

        public string SourceContext;
    }
}
