﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.Model.DTO
{
    public class UserRankingRequest
    {
        public string UserId { get; set; }

        public Guid StreamId { get; set; }
    }
}
