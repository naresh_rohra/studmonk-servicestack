﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.Model.DTO
{
    public class UserRank
    {
        public string UserId { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public int Rank { get; set; }

        public int NoOfAttempts { get; set; }

        public decimal AvgScorePercent { get; set; }

        public decimal HighestScorePercent { get; set; }

        public decimal LowestScorePercent { get; set; }
    }
}
