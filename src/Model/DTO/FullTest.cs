﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.Model.DTO
{
    public class SubjectWiseTestSection
    {
        public Guid SubjectId { get; set; }
        public string SubjectName { get; set; }
        public Model.Question[] Questions { get; set; }
        public int DurationInMinutes { get; set; }
        public int NoOfQuestions { get; set; }
        
        public bool ShouldSerializeQuestions()
        {
            if((Questions ==null)&&(DoNotSerializeQuestions == true))
            {
                return false;
            }
            return true;
        }

        [JsonIgnore]
        public bool DoNotSerializeQuestions { get; set; }
    }

    public class TestSection
    {
        public string Name { get; set; }
        public ICollection<SubjectWiseTestSection> SubjectWiseTestSections { get; set; }
        public int DurationInMinutes { get; set; }
        public int NoOfQuestions { get; set; }
        public int BreakDurationInMinutes { get; set; }
    }

    public class FullTest
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int DurationInMinutes { get; set; }

        public int NoOfQuestions { get; set; }

        public short? MaxScore { get; set; }

        public Guid StreamId { get; set; }

        public ICollection<TestSection> TestSections { get; set; }

        //public byte TestType { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TestType TestType
        {
            get;set;
        }

        public string TestDate { get; set; }

        public virtual ICollection<Sponsor> Sponsors { get; set; }

        public ICollection<ScoringSystem> ScoringSystem { get; set; }

        public FullTestInstruction Instructions { get; set; }

        public Model.Tenant Tenant { get; set; }

        public string Relevance { get; set; }
    }

    public class FullTestInstruction
    {
        public Guid TestId { get; set; }

        public string[] TopText { get; set; }

        public string[] BottomText { get; set; }

        public TestSubjectChapters[] ChaptersCovered { get; set; }
    }

    public enum TestType
    {
        PersonalizedTest=0,
        FullTest=1
    }

    public class TestSubjectChapters
    {
        public string Subject { get; set; }
        public string[] Chapters { get; set; }
    }

    public class ScoringSystem
    {
        public Guid ScoringSystemId { get; set; }

        public Guid SubjectId { get; set; }

        public string SubjectName { get; set; }

        public Int16 CorrectAnswer { get; set; }

        public Int16 InCorrectAnswer { get; set; }

        public Int16 Unanswered { get; set; }
    }
}
