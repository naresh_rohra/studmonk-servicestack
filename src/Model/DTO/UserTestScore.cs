﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.Model.DTO
{
    public class UserTestScore
    {
        public string Test { get; set; }

        public int Score { get; set; }

        public short? MaxScore { get; set; }

        public decimal ScorePercent { get; set; }
        
        public DateTime AttemptedOn { get; set; }

        public int NoOfQuestions { get; set; }

        public int CorrectAnswers { get; set; }

        public int InCorrectAnswers { get; set; }

        public int SkippedQuestions { get; set; }

        public string Stream { get; set; }

        public Guid AttemptId { get; set; }

        public Guid TestId { get; set; }
    }
}
