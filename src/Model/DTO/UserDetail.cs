﻿using System;

namespace StudMonk.ServiceStack.Model.DTO
{
    public class UserDetail
    {
        public string UserId { get; set; }

        public string DisplayName { get; set; }

        public string ProfilePic { get; set; }
    }
}
