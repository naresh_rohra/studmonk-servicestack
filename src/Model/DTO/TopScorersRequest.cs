﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.Model.DTO
{
    public class TopScorersRequest
    {
        public DateTime RecordedFrom { get; set; }

        public DateTime RecordedTo { get; set; }

        public string Stream { get; set; }

        public string TenantId { get; set; }

        public string RankingCriteria { get; set; }

        public byte MinAttempts { get; set; }

        public byte RankTill { get; set; }
    }
}
