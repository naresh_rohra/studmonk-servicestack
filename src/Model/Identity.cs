﻿namespace StudMonk.ServiceStack.Model
{
    public class Identity
    {
        public string UserId { get; set; }

        public string Role { get; set; }

        public string TenantId { get; set; }

        public bool IsInRole(string role)
        {
            return this.Role == role;
        }

        public bool IsAdmin()
        {
            return this.Role == Roles.Administrator.Id || this.Role == Roles.Administrator.Name;
        }

        public bool IsContentAdmin()
        {
            return this.Role == Roles.ContentAdministrator.Id || this.Role == Roles.ContentAdministrator.Name;
        }

        public bool IsTenantAdministrator()
        {
            return this.Role == Roles.TenantAdministrator.Id || this.Role == Roles.TenantAdministrator.Name;
        }

        public bool IsAnalyst()
        {
            return this.Role == Roles.Analyst.Id || this.Role == Roles.Analyst.Name;
        }

        public bool IsAuthor()
        {
            return this.Role == Roles.Author.Id || this.Role == Roles.Author.Name;
        }

        public bool IsReviewer()
        {
            return this.Role == Roles.Reviewer.Id || this.Role == Roles.Reviewer.Name;
        }

        public bool IsStudent()
        {
            return this.Role == Roles.Student.Id || this.Role == Roles.Student.Name;
        }
    }
}
