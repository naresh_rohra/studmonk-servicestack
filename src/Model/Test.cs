﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.Model
{
    [Table("Test")]
    public class Test : ModelWithTracking<Guid>
    {
        [Column("TestId")]
        public override Guid Id { get; set; }

        public int DurationInMinutes { get; set; }

        public int NoOfQuestions { get; set; }

        public DifficultyLevel DifficultyLevel { get; set; }

        public Guid StreamId { get; set; }
        
        [JsonProperty("Chapters")]
        [NotMapped]
        public virtual ICollection<TestChapter> TestChapters { get; set; }

        [JsonIgnore]
        [NotMapped]
        public virtual ICollection<Chapter> Chapters { get; set; }

        [NotMapped]
        public virtual ICollection<Question> Questions { get; set; }

        public byte TestType { get; set; }

        public string Name { get; set; }

        public string TenantId { get; set; }

        public short? MaxScore { get; set; }

        [JsonProperty("TestAttemptSummary")]
        [NotMapped]
        public virtual ICollection<TestAttemptSummary> TestAttemptSummary { get; set; }

        [JsonIgnore]
        public bool Active { get; set; }

        public Guid? ScoringSystemId { get; set; }

        public string Relevance { get; set; }
    }
}
