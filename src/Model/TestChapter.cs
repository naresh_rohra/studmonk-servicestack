﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestChapter")]
    public class TestChapter : ModelWithTracking<Guid>
    {
        [Column("TestId")]
        public override Guid Id { get; set; }

        [Key]
        public Guid ChapterId { get; set; }

        public int NoOfQuestions { get; set; }
    }
}