﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudMonk.ServiceStack.Model
{
    [Table("Sponsor")]
    public class Sponsor: ModelWithTracking<Guid>
    {
        [Column("SponsorId")]
        public override Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string @State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonTitle { get; set; }
        public string Description { get; set; }
        public Byte[] Image { get; set; }
    }
}
