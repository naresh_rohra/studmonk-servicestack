﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.Model
{
    [Table("QuestionOfDayAttempt")]
    public class QuestionOfDayAttempt : ModelWithTracking<Guid>
    {
        [Column("QuestionOfDayId")]
        [Key]
        public  Guid QuestionOfDayId { get; set; }

        [Key]
        public string UserId { get; set; }

        public string AnswerChoice { get; set; }

        public byte AnswerStatus { get; set; }

        public short Score { get; set; }

        public int TimeSpentInSeconds { get; set; }

        [NotMapped]
        [JsonIgnore]
        public override Guid Id { get; set; }
    }
}
