﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestFormatSubject")]
    public class TestFormatSubject : ModelWithTracking<Guid>
    {
        [Column("FormatId")]
        public override Guid Id { get; set; }

        public Guid SubjectId { get; set; }

        public Int16 NoOfQuestions { get; set; }

        public Int16 TimePerQuestion { get; set; }

        public bool Active { get; set; }

        [NotMapped]
        public Subject Subject { get; set; }
    }
}
