﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestInstruction")]
    public class TestInstruction : ModelWithTracking<Guid>
    {
        [NotMapped]
        [JsonIgnore]
        public override Guid Id { get; set; }
        public Guid TestId { get; set; }
        public string TopText { get; set; }
        public string BottomText { get; set; }    
        public bool Active { get; set;}
    }
}
