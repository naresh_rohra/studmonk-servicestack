﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestQuestion")]
    public class TestQuestion : ModelWithTracking<Guid>
    {
        [Column("TestId")]
        public override Guid Id { get; set; }

        [Key]
        public Guid QuestionId { get; set; }
    }
}