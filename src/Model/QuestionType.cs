﻿using System;

namespace StudMonk.ServiceStack.Model
{
    public enum QuestionType : Byte
    {
        MultiChoiceSingleSelect = 1,
        MultiChoiceMultiSelect = 2
    }
    
    public class AllQuestionTypes
    {
        public QuestionType MultiChoiceSingleSelect = QuestionType.MultiChoiceSingleSelect;
        public QuestionType MultiChoiceMultiSelect = QuestionType.MultiChoiceMultiSelect;
    }
}
