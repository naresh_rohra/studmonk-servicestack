﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace StudMonk.ServiceStack.Model
{
    [Table("StudentEnrollmentDetail")]
    public class StudentEnrollmentDetail : ModelWithTrackingAndAudit<Guid>
    {
        [Column("StreamId")]
        public override Guid Id { get; set; }

        public string UserId { get; set; }

        public bool Active { get; set; }
    }
}
