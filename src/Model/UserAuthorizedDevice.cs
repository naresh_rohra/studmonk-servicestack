﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace StudMonk.ServiceStack.Model
{
    [Table("UserAuthorizedDevice")]
    public class UserAuthorizedDevice : ModelWithTrackingAndAudit<string>
    {
        [Column("DeviceId")]
        public override string Id { get; set; }

        public string UserId { get; set; }

        public string DeviceName { get; set; }

        [JsonIgnore]
        public bool Active { get; set; }
    }
}
