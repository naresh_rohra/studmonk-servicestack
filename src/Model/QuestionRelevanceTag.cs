﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StudMonk.ServiceStack.Model
{
    [Table("QuestionRelevanceTag")]
    public class QuestionRelevanceTag : QuestionTag
    {
        public QuestionRelevanceTag() { }

        public QuestionRelevanceTag(QuestionTag qTag): base(qTag) { }

        [NotMapped]
        public Guid QuestionId { get; set; }
    }
}