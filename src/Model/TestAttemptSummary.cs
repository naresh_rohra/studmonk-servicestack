﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestAttemptSummary")]
    public class TestAttemptSummary : ModelWithTracking<Guid>
    {
        [Column("AttemptId")]
        [JsonProperty("AttemptId")]
        public override Guid Id { get; set; }

        public short CorrectAnswers { get; set; }

        public short PartiallyCorrectAnswers { get; set; }

        public short InCorrectAnswers { get; set; }

        public short SkippedQuestions { get; set; }
        
        public short Score { get; set; }
        
        public Guid TestId { get; set; }

        public int TimeSpentInSeconds { get; set; }

        public byte TestEndReason { get; set; }

        public string UserId { get; set; }

        [JsonIgnore]
        public virtual ICollection<TestAttemptDetail> AttemptDetails { get; set; }

        [JsonProperty("AttemptedOn")]
        [NotMapped]
        public DateTime AttemptedOn { get { return CreatedOn; } }
    }
}
