﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.Model
{
    [Table("UserQuestionFlag")]
    public class UserQuestionFlag : ModelWithTrackingAndAudit<Guid>
    {
        [Column("QuestionId")]
        public override Guid Id { get; set; }

        public string UserId { get; set; }

        public Flag Flag { get; set; }

        [JsonIgnore]
        [NotMapped]
        public new DateTime? LastModifiedOn { get; set; }

        [JsonIgnore]
        [NotMapped]
        public new string LastModifiedBy { get; set; }
    }
}