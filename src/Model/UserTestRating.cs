﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.Model
{
    [Table("UserTestRating")]
    public class UserTestRating : ModelWithTrackingAndAudit<Guid>
    {
        [Column("TestId")]
        public override Guid Id { get; set; }

        public string UserId { get; set; }

        public byte Rating { get; set; }

        [JsonIgnore]
        [NotMapped]
        public new DateTime? LastModifiedOn { get; set; }

        [JsonIgnore]
        [NotMapped]
        public new string LastModifiedBy { get; set; }
    }
}