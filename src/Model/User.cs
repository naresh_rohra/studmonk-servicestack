﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.Model
{
    [Table("User")]
    public class User : ModelWithTracking<Guid>
    {
        [Column("UserId")]
        public override Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        [JsonIgnore]
        public string Password { get; set; }

        public bool Active { get; set; }

        public byte UserType { get; set; }

        public DateTime DOB { get; set; }

        public string Gender { get; set; }

        public string MobileNo { get; set; }

        public string FacebookHandle { get; set; }

        public string TwitterHandle { get; set; }

        public string GoogleHandle { get; set; }

        public string StateCode { get; set; }

        public string DeviceId { get; set; }

        public Guid InstitutionId { get; set; }
    }
}
