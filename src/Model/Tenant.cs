﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace StudMonk.ServiceStack.Model
{
    [Table("Tenant")]
    public class Tenant : ModelWithTrackingAndAudit<string>
    {
        [Column("TenantId")]
        public override string Id { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public bool Active { get; set; }

        public Byte NoOfDevicesAllowed {get;set;}

        public Boolean NegativeMarking { get; set; }
    }
}
