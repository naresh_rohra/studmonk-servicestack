﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestScoringSystem")]
    public class TestScoringSystem : ModelWithTracking<Guid>
    {
        [Column("ScoringSystemId")]
        public override Guid Id { get; set; }
        
        public Guid SubjectId { get; set; }

        public Guid StreamId { get; set; }

        public Int16 CorrectAnswer { get; set; }

        public Int16 InCorrectAnswer { get; set; }

        public Int16 Unanswered { get; set; }

        public bool Active { get; set;}
    }
}
