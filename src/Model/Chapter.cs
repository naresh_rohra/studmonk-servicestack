﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace StudMonk.ServiceStack.Model
{
    [Table("Chapter")]
    public class Chapter : ModelWithTrackingAndAudit<Guid>
    {
        [JsonProperty("chapterId")]
        [Column("ChapterId")]
        public override Guid Id { get; set; }

        [JsonProperty("chapterName")]
        public string Name { get; set; }

        [JsonProperty("chapterDescription")]
        public string Description { get; set; }

        [JsonProperty("chapterNo")]
        public byte ChapterNo { get; set; }

        [JsonProperty("easyQuestions")]
        public int EasyQuestions { get; set; }

        [JsonProperty("mediumQuestions")]
        public int MediumQuestions { get; set; }

        [JsonProperty("hardQuestions")]
        public int HardQuestions { get; set; }

        [JsonProperty("nonReviewedQuestions")]
        public int NonReviewedQuestions { get; set; }
        
        [JsonIgnore]
        public Guid SubjectId { get; set; }

        [JsonIgnore]
        public Guid StreamId { get; set; }

        [JsonIgnore]
        public bool Active { get; set; }
    }
}
