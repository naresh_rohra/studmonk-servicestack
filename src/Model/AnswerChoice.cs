﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudMonk.ServiceStack.Model
{
    [Table("AnswerChoice")]
    public class AnswerChoice : ModelWithTracking<Guid>
    {
        [Column("AnswerChoiceId")]
        public override Guid Id { get; set; }

        public string Text { get; set; }

        public bool IsCorrect { get; set; }

        public Guid QuestionId { get; set; }
    }
}
