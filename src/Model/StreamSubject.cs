﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace StudMonk.ServiceStack.Model
{
    [Table("StreamSubject")]
    public class StreamSubject : ModelWithTracking<Guid>
    {
        [Column("StreamId")]
        public override Guid Id { get; set; }

        public Guid SubjectId { get; set; }
    }
}
