﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.Model
{
    [Table("QuestionOfDay")]
    public class QuestionOfDay : ModelWithTracking<Guid>
    {
        [NotMapped]
        [JsonIgnore]
        public override Guid Id { get; set; }

        [Column("QuestionOfDayId")]
        [Key]
        public  Guid QuestionOfDayId { get; set; }

        public Guid QuestionId { get; set; }

        [NotMapped]
        public Model.Question Question { get; set; }
    }
}
