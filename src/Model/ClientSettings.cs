﻿namespace StudMonk.ServiceStack.Model.Settings
{
    public class Client
    {
        public AllDifficultyLevels DifficultyLevels { get; set; }
        public AllTextTypes TextTypes { get; set; }

        public Client()
        {
            DifficultyLevels = new AllDifficultyLevels();
            TextTypes = new AllTextTypes();
        }
    }
}
