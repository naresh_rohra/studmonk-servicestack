﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace StudMonk.ServiceStack.Model
{
    [Table("Subject")]
    public class Subject : ModelWithTracking<Guid>
    {
        [JsonProperty("subjectId")]
        [Column("SubjectId")]
        public override Guid Id { get; set; }

        [JsonProperty("subjectName")]
        public string Name { get; set; }

        [JsonProperty("subjectDescription")]
        public string Description { get; set; }

        public Guid InstitutionId { get; set; }
    }
}
