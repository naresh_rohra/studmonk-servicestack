﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudMonk.ServiceStack.Model
{
    [Table("AuditLog")]
    public class AuditLog : ModelWithTracking<Int64>
    {
        [Column("AuditLogId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override Int64 Id { get; set; }

        public string UserId { get; set; }

        [NotMapped]
        public string UserName { get; set; }

        [NotMapped]
        public string FullName { get; set; }

        [NotMapped]
        public string Tenant { get; set; }

        public string Category { get; set; }

        public string Action { get; set; }

        public DateTime RecordedOn { get; set; }

        public string ContextData { get; set; }

        [JsonIgnore]
        [NotMapped]
        public override DateTime CreatedOn { get; set; }

        [JsonIgnore]
        [NotMapped]
        public override DateTime? LastModifiedOn { get; set; }
    }

    public class AuditLogReport
    {
        public string UserId { get; set; }

        [NotMapped]
        public string UserName { get; set; }

        [NotMapped]
        public string FullName { get; set; }

        [NotMapped]
        public string Tenant { get; set; }

        public Int64 FirstAuditLogId { get; set; }

        public string FirstCategory { get; set; }

        public string FirstAction { get; set; }

        public DateTime FirstRecordedOn { get; set; }

        public Int64 RecentAuditLogId { get; set; }

        public string RecentCategory { get; set; }

        public string RecentAction { get; set; }

        public DateTime RecentRecordedOn { get; set; }
    }

    public class AuditLogStat
    {
        public string Category { get; set; }

        public string Action { get; set; }

        public Int64 Count { get; set; }

        public string Period { get; set; }
    }
}
