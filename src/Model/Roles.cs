﻿namespace StudMonk.ServiceStack.Model
{
    public static class Roles
    {
        public static Role Student = new Role("student", "Student");
        public static Role Author = new Role("author", "Author");
        public static Role Reviewer = new Role("reviewer", "Reviewer");
        public static Role TenantAdministrator = new Role("tenant-admin", "Tenant Administrator");
        public static Role ContentAdministrator = new Role("content-admin", "Content Administrator");
        public static Role Administrator = new Role("admin", "Administrator");
        public static Role Analyst = new Role("analyst", "Analyst");
    }

    public class Role
    {
        public Role(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Id { get; }

        public string Name { get; }
    }
}

