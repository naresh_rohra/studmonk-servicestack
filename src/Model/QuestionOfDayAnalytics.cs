﻿namespace StudMonk.ServiceStack.Model
{
    public class QuestionOfDayAnalytics
    {
        public int TotalSuccess { get; set; }

        public int TotalFailure { get; set; }

        public int TotalAttempt { get; set; }

        public int CurrentStreak { get; set; }

        public int RecordStreak { get; set; }
    }

}
