﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StudMonk.ServiceStack.Model
{
    [Table("QuestionConceptTag")]
    public class QuestionConceptTag : QuestionTag
    {
        public QuestionConceptTag() { }

        public QuestionConceptTag(QuestionTag qTag): base(qTag) { }

        [NotMapped]
        public Guid QuestionId { get; set; }
    }
}