﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace StudMonk.ServiceStack.Model
{
    [Table("Stream")]
    public class Stream : ModelWithTracking<Guid>
    {
        [Column("StreamId")]
        public override Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string WebsiteUrl { get; set; }

        public Guid BoardId { get; set; }

        public bool Active { get; set; }
    }
}
