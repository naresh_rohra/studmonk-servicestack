﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestFormat")]
    public class TestFormat : ModelWithTracking<Guid>
    {
        [Column("FormatId")]
        public override Guid Id { get; set; }

        public string Name { get; set; }

        public Int32 DurationInMinutes { get; set; }

        public string InstructionsTopText { get; set; }

        public string InstructionsBottomText { get; set; }

        public Guid StreamId { get; set; }

        public bool Active { get; set; }

        public short? MaxScore { get; set; }
        
        [NotMapped]
        public TestFormatSubject[] Subjects { get; set; }

        public Guid ScoringSystemId { get; set; }

        public ICollection<DTO.ScoringSystem> ScoringSystem { get; set; }

        public string Relevance { get; set; }
    }
}
