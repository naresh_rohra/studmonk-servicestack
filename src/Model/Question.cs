﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace StudMonk.ServiceStack.Model
{
    [Table("Question")]
    public class Question : ModelWithTrackingAndAudit<Guid>
    {
        [Column("QuestionId")]
        public override Guid Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long? QuestionNo { get; set; }

        public QuestionType QuestionType { get; set; }

        public string Text
        {
            get;
            set;
        }

        public TextType TextType { get; set; }

        public DifficultyLevel DifficultyLevel { get; set; }

        public QualityStatus QualityStatus { get; set; }

        public string Hint { get; set; }

        public string Explanation { get; set; }

        public string PreviousAppearances { get; set; }

        public long? Size { get; set; }

        public Guid ChapterId { get; set; }

        [NotMapped]
        public Guid? SubjectId { get; set; }

        public ICollection<AnswerChoice> AnswerChoices { get; set; }

        [NotMapped]
        public string[] TaggedConcepts { get; set; }

        [NotMapped]
        public string[] TaggedRelevances { get; set; }

        [JsonIgnore]
        [NotMapped]
        public ICollection<QuestionConceptTag> ConceptTags { get; set; }

        [JsonIgnore]
        [NotMapped]
        public ICollection<QuestionRelevanceTag> RelevanceTags { get; set; }

        [JsonIgnore]
        public bool Active { get; set; }

        [NotMapped]
        public string ShortText
        {
            get
            {
                if (this.Text != null)
                {
                    var pattern = @"<img([\w\W]+?)/>|<img([\w\W]+?)</img>";
                    var replacement = " ";
                    var rgx = new Regex(pattern);
                    return rgx.Replace(this.Text, replacement);
                }
                else
                {
                    return null;
                }
            }
        }

        public string Sources { get; set; }
    }
}
