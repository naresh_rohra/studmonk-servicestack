﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StudMonk.ServiceStack.Model
{
    [Table("QuestionBookmark")]
    public class QuestionBookmark : ModelWithTracking<Guid>
    {
        [Column("QuestionId")]
        public override Guid Id { get; set; }

        [Key]
        public string UserId { get; set; }

        public Guid TestId { get; set; }

        public string BookmarkType { get; set; }
    }
}
