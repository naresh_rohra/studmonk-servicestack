﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.Model
{
    [Table("Tag")]
    public class Tag : ModelWithTrackingAndAudit<Int64>
    {
        [Column("TagId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override Int64 Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
