﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StudMonk.ServiceStack.Model
{
    [Table("TestAttemptDetail")]
    public class TestAttemptDetail : ModelWithTracking<Guid>
    {
        [Column("AttemptId")]
        public override Guid Id { get; set; }

        [Key]
        public Guid QuestionId { get; set; }

        public string AnswerChoice { get; set; }

        public byte AnswerStatus { get; set; }

        public short Score { get; set; }

        public int TimeSpentInSeconds { get; set; }
    }
}
