﻿using System;

namespace StudMonk.ServiceStack.Model
{
    public enum QualityStatus : Byte
    {
        Any = 0,
        NotReviewed = 1,
        Reviewed = 2,
        NeedsCorrection = 3
    }
}
