﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.Model
{
    public enum Flag : byte
    {
        Like = 1,
        Dislike = 2,
        Incorrect = 3,
        RequestForSolution = 4,
        RequestionForHint = 5
    }
}
