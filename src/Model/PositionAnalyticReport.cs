﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.Model
{
    public class PositionAnalyticReport: ModelWithTracking<Guid>
    {
        [NotMapped]
        [JsonIgnore]
        public override Guid Id { get; set; }

        public string  UserId { get; set; }
        public Guid SubjectId { get; set; }

        public double UserHighScorePercentage { get; set; }
        public double LastYearToppperPercentage { get; set; }
        public double AppToppperPercentage { get; set; }
        public double AppAveragePercentage { get; set; }
        public int DifficultyLevel { get; set; }
    }
}
