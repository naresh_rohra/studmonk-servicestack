﻿using System;

namespace StudMonk.ServiceStack.Model
{
    public enum TextType : Byte
    {
        Text = 1,
        ChemistryFormula = 2,
        MathematicsFormula = 3
    }

    public class AllTextTypes
    {
        public TextType Text = TextType.Text;
        public TextType ChemistryFormula = TextType.ChemistryFormula;
        public TextType MathematicsFormula = TextType.MathematicsFormula;
    }
}
