﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace StudMonk.ServiceStack.Model
{
    public abstract class ModelBase<TKey>
    {
        [Key]
        public abstract TKey Id { get; set; }
    }

    public abstract class ModelWithTracking<TKey> : ModelBase<TKey>
    {
        [JsonIgnore]
        public virtual DateTime CreatedOn { get; set; }

        [JsonIgnore]
        public virtual DateTime? LastModifiedOn { get; set; }
    }

    public abstract class ModelWithTrackingAndAudit<TKey> : ModelWithTracking<TKey>
    {
        [JsonIgnore]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public string LastModifiedBy { get; set; }
    }
}
