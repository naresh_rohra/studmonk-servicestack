﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StudMonk.ServiceStack.Model
{
    public class QuestionTag : ModelWithTrackingAndAudit<Guid>
    {
        public QuestionTag() { }

        public QuestionTag(QuestionTag qTag)
        {
            Id = qTag.Id;
            TagId = qTag.TagId;
            IsActive = qTag.IsActive;
            CreatedBy = qTag.CreatedBy;
            CreatedOn = qTag.CreatedOn;
            LastModifiedBy = qTag.LastModifiedBy;
            LastModifiedOn = qTag.LastModifiedOn;
            State = qTag.State;
        }

        [Column("QuestionId")]
        public override Guid Id { get; set; }

        [Key]
        public Int64 TagId { get; set; }

        public bool IsActive { get; set; }

        [NotMapped]
        public ModificationState State { get; set; }
    }

    public enum ModificationState
    {
        Added = 1,
        Deleted = 2,
        Unchanged = 3
    }
}
