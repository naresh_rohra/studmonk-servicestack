﻿using System;

namespace StudMonk.ServiceStack.Model
{
    public enum DifficultyLevel : Byte
    {
        Random = 0,
        Easy = 1,
        Medium = 2,
        Hard = 3
    }
    
    public class AllDifficultyLevels
    {
        public DifficultyLevel Easy = DifficultyLevel.Easy;
        public DifficultyLevel Medium = DifficultyLevel.Medium;
        public DifficultyLevel Hard = DifficultyLevel.Hard;
        public DifficultyLevel Random = DifficultyLevel.Random;
    }
}
