﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.Model.Constants
{
    public class SubjectId
    {
        public static Guid MATHEMATICS = Guid.Parse("46d3900a-d1e4-4537-8aee-137a29120069");
        public static Guid CHEMISTRY = Guid.Parse("a1ccfdd9-3b4e-4453-be1b-6b4fc55d8caf");
        public static Guid BIOLOGY = Guid.Parse("5eb27717-c185-41b1-9679-8ac1e6527328");
        public static Guid PHYSICS = Guid.Parse("f04d9b0e-9ce0-4e90-bddd-e42551ba7ee9");
    }

    public class StreamId
    {
        public static Guid ENGINEERING = Guid.Parse("94085a60-62a5-4096-8110-b0feeb225cf5");
        public static Guid MEDICAL = Guid.Parse("ec3ebe58-1c67-4759-aac0-b9ad43010cbe");
    }
}
