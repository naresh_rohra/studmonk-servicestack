﻿using System;
using System.Linq;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Data.Entity.ChangeTracking;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public abstract class RepositoryBase<TModel, TKey> : ModelRepository.Repository, ModelRepository.RepositoryBaseOperations<TModel, TKey>
        where TModel : Model.ModelWithTracking<TKey>,
        new()
    {
        protected virtual bool IsCompositeRepository
        {
            get
            {
                return false;
            }
        }

        protected string ConnectionString
        {
            get; set;
        }

        protected List<string> _fieldsIgnoredForSaveExisting = new List<string>();
        protected List<string> _fieldsIgnoredForSaveNew = new List<string>();

        protected DbSet<TModel> _entitySet;

        protected DbContextOptions _dbContextOptions;

        protected Lazy<ApplicationDbContext> _modelContext;

        protected ApplicationDbContext Context
        {
            get
            {
                return _modelContext.Value;
            }
        }

        protected RepositoryBase(string connectionString)
        {
            ConnectionString = connectionString;
            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseSqlServer(connectionString);
            _dbContextOptions = optionsBuilder.Options;
            _modelContext = new Lazy<ApplicationDbContext>(() => new ApplicationDbContext(_dbContextOptions));
            _fieldsIgnoredForSaveExisting.AddRange(new string[] { "CreatedOn" });
        }

        protected void SetEntitySet(DbSet<TModel> entitySet)
        {
            _entitySet = entitySet;
        }

        protected void SaveContextChanges()
        {
            Context.SaveChanges();
        }

        public virtual TModel Get(TKey id)
        {
            return _entitySet.FirstOrDefault(e => e.Id.Equals(id));
        }

        public virtual TModel[] Get(TKey[] ids)
        {
            return _entitySet.Where(e => ids.Contains(e.Id)).ToArray();
        }

        public TModel[] GetForIds(TKey[] ids)
        {
            var result = _entitySet.Where(e => ids.Contains(e.Id));
            return result.ToArray();
        }

        public TModel SaveNew(TModel entity)
        {

            SetKeyForEntity(entity);
            entity.CreatedOn = DateTime.UtcNow;
            BeforeSaveNew(new TModel[] { entity });
           _entitySet.Add(entity);
            IgnoreFieldsForSaveNew(entity);
            this.SaveContextChanges();
            AfterSaveNew(new TModel[] { entity });
            return entity;
        }

        public TModel[] SaveNew(TModel[] entities)
        {
            foreach (var entity in entities)
            {
                if (!IsCompositeRepository)
                    SetKeyForEntity(entity);
                entity.CreatedOn = DateTime.UtcNow;
                _entitySet.Add(entity);
                IgnoreFieldsForSaveNew(entity);
            }
            this.BeforeSaveNew(entities);
            this.SaveContextChanges();
            AfterSaveNew(entities);
            return entities;
        }

        private void SetKeyForEntity(TModel entity)
        {
            if(entity.Id.GetType().Name.ToLower() == "guid")
            {
                entity.Id = (TKey)TypeDescriptor.GetConverter(typeof(TKey)).ConvertFromInvariantString(Guid.NewGuid().ToString());
            }
        }

        public TModel SaveExisting(TModel entity)
        {
            entity.LastModifiedOn = DateTime.UtcNow;
            _entitySet.Attach(entity);
            Context.Update(entity);
            IgnoreFieldsForSaveExisting(entity);
            this.BeforeSaveExisting(entity);
            this.SaveContextChanges();
            AfterSaveExisting(entity);
            return entity;
        }

        protected void IgnoreFieldsForSaveExisting(TModel entity)
        {
            foreach (string field in this._fieldsIgnoredForSaveExisting)
            {
                Context.Entry(entity).Property(field).IsModified = false;
            }
        }

        protected void IgnoreFieldsForSaveNew(TModel entity)
        {
            foreach (string field in this._fieldsIgnoredForSaveNew)
            {
                Context.Entry(entity).Property(field).IsModified = false;
            }
        }

        public virtual void Delete(TKey id)
        {
            TModel entity = Get(id);
            HardDelete(entity);
            this.SaveContextChanges();
        }

        public virtual void Delete(TKey[] ids)
        {
            foreach (var entity in GetForIds(ids))
            {
                HardDelete(entity);
            }
            this.SaveContextChanges();
        }

        public virtual void Delete(TModel entity)
        {
            HardDelete(entity);
            this.SaveContextChanges();
        }

        public virtual void Delete(TModel[] entities)
        {
            foreach(var entity in entities)
            { 
                HardDelete(entity);
            }
            this.SaveContextChanges();
        }

        protected virtual void HardDelete(TModel entity)
        {
            _entitySet.Attach(entity);
            _entitySet.Remove(entity);            
        }
        
        protected void SetModifiedFlagForFields(EntityEntry entityTrackingObject, string[] fields, bool isModified)
        {
            foreach (var field in fields)
            {
                var property = entityTrackingObject.Property(field);
                if (property != null)
                {
                    property.IsModified = isModified;
                }
            }
        }

        protected virtual void BeforeSaveExisting(TModel entity)
        { }

        protected virtual void BeforeSaveExisting(TModel[] entity)
        {

        }

        protected virtual void AfterSaveExisting(TModel entity)
        { }

        protected virtual void BeforeSaveNew(TModel[] entity)
        { }

        protected virtual void AfterSaveNew(TModel[] entity)
        { }
    }
}