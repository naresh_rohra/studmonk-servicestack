﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using StudMonk.ServiceStack.Model.DTO;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class Tests : RepositoryBase<Model.Test, Guid>, ModelRepository.Tests
    {
        public Tests(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.Tests);
        }

        protected override void AfterSaveNew(Model.Test[] tests)
        {
            foreach (var test in tests)
            {
                test.TestChapters = new List<Model.TestChapter>();

                foreach (var chapter in test.Chapters)
                {
                    var testChapter = new Model.TestChapter
                    {
                        Id = test.Id,
                        ChapterId = chapter.Id,
                        NoOfQuestions = test.Questions.Count(q => q.ChapterId == chapter.Id),
                        CreatedOn = DateTime.UtcNow
                    };

                    Context.TestChapters.Add(testChapter);
                    test.TestChapters.Add(testChapter);
                }

                foreach (var question in test.Questions)
                {
                    Context.TestQuestions.Add(new Model.TestQuestion
                    {
                        Id = test.Id,
                        QuestionId = question.Id,
                        CreatedOn = DateTime.UtcNow
                    });
                }
            }
            Context.SaveChanges();
        }

        public Model.Test[] GetForChapter(Guid chapterId)
        {
            var result = from tc in Context.TestChapters
                         join t in Context.Tests on tc.Id equals t.Id
                         where tc.ChapterId == chapterId
                         select t;
            return result.ToArray();
        }

        public Model.Test[] GetAllFullTest(string lastSyncDate, string streamId, string tenantId, string relevance)
        {
            DateTime dateTime;
            Model.Test[] result;

            if (new Guid(streamId) == Guid.Empty)
            {
                if (DateTime.TryParseExact(lastSyncDate, "yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                {
                    result = (from t in Context.Tests
                              where (t.TestType == 1) && (t.CreatedOn > dateTime) && (t.Active == true) &&
                                 (String.IsNullOrEmpty(t.TenantId) || (!String.IsNullOrEmpty(tenantId) && t.TenantId == tenantId))
                              select t).OrderBy(t => t.Name).ToArray();
                }
                else
                {
                    result = (from t in Context.Tests
                              where (t.TestType == 1) && (t.Active == true) &&
                                 (String.IsNullOrEmpty(t.TenantId) || (!String.IsNullOrEmpty(tenantId) && t.TenantId == tenantId))
                              select t).OrderBy(t => t.Name).ToArray();
                }
            }
            else if (DateTime.TryParseExact(lastSyncDate, "yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
            {
                result = (from t in Context.Tests
                          where (t.TestType == 1) && (t.CreatedOn > dateTime) && (t.StreamId == new Guid(streamId)) && (t.Active == true) &&
                             (String.IsNullOrEmpty(t.TenantId) || (!String.IsNullOrEmpty(tenantId) && t.TenantId == tenantId))
                          select t).OrderBy(t => t.Name).ToArray();
            }
            else
            {
                result = (from t in Context.Tests
                          where (t.TestType == 1) && (t.StreamId == new Guid(streamId)) && (t.Active == true) &&
                             (String.IsNullOrEmpty(t.TenantId) || (!String.IsNullOrEmpty(tenantId) && t.TenantId == tenantId))
                          select t).OrderBy(t => t.Name).ToArray();
            }

            if (!String.IsNullOrEmpty(relevance))
            {
                return result.Where(t => t.Relevance == relevance).ToArray();
            }
            else
            {
                return result;
            }
        }

        public Model.Test GetTestWithUserAttempt(string userId, Guid attemptId)
        {
            var test = (from t in Context.Tests
                        join ta in Context.TestAttemptsSummary on t.Id equals ta.TestId
                        where ta.UserId == userId && ta.Id == attemptId
                        select t).FirstOrDefault();

            if (test != null)
            {
                var questionAnswerChoices = (from ac in Context.AnswerChoices
                                             join q in Context.Questions on ac.QuestionId equals q.Id
                                             join tad in Context.TestAttemptsDetail on q.Id equals tad.QuestionId
                                             join tas in Context.TestAttemptsSummary on tad.Id equals tas.Id
                                             where tas.UserId == userId && tas.Id == attemptId
                                             select ac).ToArray();

                var testQuestions = (from q in Context.Questions
                                     join tad in Context.TestAttemptsDetail on q.Id equals tad.QuestionId
                                     join tas in Context.TestAttemptsSummary on tad.Id equals tas.Id
                                     where tas.UserId == userId && tas.Id == attemptId
                                     select new { TestId = tas.TestId, Question = q }).ToArray();
                foreach (var testQ in testQuestions)
                {
                    testQ.Question.AnswerChoices = questionAnswerChoices.Where(ac => ac.QuestionId == testQ.Question.Id).ToArray();
                }

                var testAttemptSummaries = (from tas in Context.TestAttemptsSummary
                                            where tas.UserId == userId && tas.Id == attemptId
                                            select tas).ToArray();

                var testChapters = (from tc in Context.TestChapters
                                    join tas in Context.TestAttemptsSummary on tc.Id equals tas.TestId
                                    where tas.UserId == userId && tas.Id == attemptId
                                    select new { TestId = tas.TestId, TestChapter = tc }).ToArray();

                var questionsRepository = new Questions(this.ConnectionString);

                test.Questions = testQuestions.Where(q => q.TestId == test.Id).Select(q => q.Question).ToArray();
                test.TestChapters = testChapters.Where(tc => tc.TestId == test.Id).Select(tc => tc.TestChapter).ToArray();
                test.TestAttemptSummary = testAttemptSummaries.Where(tas => tas.TestId == test.Id).ToArray();
            }
            return test;
        }

        public Model.Test[] GetTestOfUser(string userId, int pageNo, int pageSize)
        {
            pageNo = pageNo >= 1 ? pageNo : 1;

            Model.Test[] tests;
            tests = (from t in Context.Tests
                     join ta in Context.TestAttemptsSummary on t.Id equals ta.TestId
                     where ta.UserId == userId
                     select t).OrderByDescending(ta => ta.CreatedOn).Skip(pageSize * (pageNo - 1)).Take(pageSize).ToArray();

            var testAttemptSummaries = (from tas in Context.TestAttemptsSummary
                                        where tas.UserId == userId
                                        select tas).ToArray();

            foreach (var test in tests)
            {
                test.TestAttemptSummary = testAttemptSummaries.Where(tas => tas.TestId == test.Id).ToArray();
            }

            return tests;
        }

        public Model.TestAttemptSummary[] GetTestAttemptSummary(Guid testId, string userId)
        {
            var testAttemptSummary = (from ta in Context.TestAttemptsSummary
                                      where ((ta.UserId == userId) && (ta.TestId == testId))
                                      select ta).ToArray();

            return testAttemptSummary;
        }

        public Model.Sponsor[] GetSponsorsForTest(Guid testId)
        {
            return (from ts in Context.TestSponsors
                    join s in Context.Sponsors on ts.SponsorId equals s.Id
                    where ts.TestId == testId
                    select
                    new Model.Sponsor
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Address = s.Address ?? String.Empty,
                        Street = s.State ?? String.Empty,
                        City = s.City ?? String.Empty,
                        State = s.State ?? String.Empty,
                        Zip = s.Zip ?? String.Empty,
                        Email = s.Email ?? String.Empty,
                        Phone = s.Phone ?? String.Empty,
                        Mobile = s.Mobile ?? String.Empty,
                        Website = s.Website ?? String.Empty,
                        ContactPerson = s.ContactPerson ?? String.Empty,
                        ContactPersonTitle = s.ContactPersonTitle ?? String.Empty,
                        Description = s.Description ?? String.Empty,
                        Image = s.Image,
                        CreatedOn = s.CreatedOn,
                        LastModifiedOn = s.LastModifiedOn
                    }
                    ).ToArray();
        }

        public Model.UserTestRating SaveUserTestRating(Model.UserTestRating rating)
        {
            var existingRating = (from r in Context.UserTestRatings
                                  where r.Id == rating.Id
                                  && r.UserId == rating.UserId
                                  select r).FirstOrDefault();

            if (existingRating == null)
            {
                rating.CreatedOn = DateTime.UtcNow;
                rating.CreatedBy = rating.UserId;
                Context.Add(rating);
            }
            else
            {
                existingRating.Rating = rating.Rating;
                var trackingObject = Context.Entry(existingRating);
                trackingObject.State = Microsoft.Data.Entity.EntityState.Modified;
            }

            Context.SaveChanges();
            return rating;
        }

        public Model.DTO.FullTestInstruction GetTestInstruction(Guid testId)
        {
            Model.DTO.FullTestInstruction fullTestInstruction = new Model.DTO.FullTestInstruction();
            var testInst = (from ti in Context.TestInstructions
                            where ti.TestId == testId
                            select ti).FirstOrDefault();

            var detail = (from tc in Context.TestChapters
                          join c in Context.Chapters on tc.ChapterId equals c.Id
                          join s in Context.Subjects on c.SubjectId equals s.Id
                          where tc.Id == testId
                          select new { Subject = s.Name, Chapter = c.Name }).ToList();

            Model.DTO.TestSubjectChapters[] chaptersCovered = (from m in detail
                                                               group m.Chapter by m.Subject into g
                                                               select new Model.DTO.TestSubjectChapters { Subject = g.Key, Chapters = g.ToList().ToArray<string>() }).ToArray<Model.DTO.TestSubjectChapters>();

            fullTestInstruction = new Model.DTO.FullTestInstruction { TestId = testInst.TestId, TopText = testInst.TopText.Split(','), BottomText = testInst.BottomText.Split(','), ChaptersCovered = chaptersCovered };
            return fullTestInstruction;
        }

        public Model.TestFormat[] GetTestFormatsForStream(Guid streamId)
        {
            var formats = (from tf in Context.TestFormats
                           where (tf.StreamId == streamId || streamId == Guid.Empty) && tf.Active == true
                           select tf).OrderBy(tf => tf.Name).ToArray();
            foreach (var format in formats)
            {
                format.Subjects = (from tfs in Context.TestFormatSubjects
                                   where tfs.Id == format.Id && tfs.Active == true
                                   select tfs).ToArray();
                foreach (var formatSubject in format.Subjects)
                {
                    formatSubject.Subject = (from s in Context.Subjects
                                             where s.Id == formatSubject.SubjectId
                                             select s).FirstOrDefault();
                }
            }
            return formats;
        }

        public ModelRepository.Types.FullTestPreviewResponse GetFullTestPreview(ModelRepository.Types.GenerateFullTestRequest request)
        {
            return Context.GetFullTestPreview(request);
        }

        public void GenerateFullTest(ModelRepository.Types.GenerateFullTestRequest request)
        {
            Context.GenerateFullTest(request);
        }

        public Model.Sponsor[] GetAllSponsors()
        {
            return (from sponsor in Context.Sponsors
                    select sponsor).ToArray();
        }

        public void SaveFullTestDetails(Model.DTO.FullTest fullTest)
        {
            var test = this.Get(fullTest.Id);
            test.Name = fullTest.Name;
            test.LastModifiedOn = DateTime.UtcNow;
            this.Context.Add(test);
            var testTracking = Context.Entry(test);
            testTracking.State = Microsoft.Data.Entity.EntityState.Modified;
            testTracking.Property("Name").IsModified = true;
            testTracking.Property("LastModifiedOn").IsModified = true;

            var testInstructions = (from inst in this.Context.TestInstructions
                                    where inst.TestId == fullTest.Id
                                    select inst).FirstOrDefault();

            if (testInstructions != null)
            {
                testInstructions.BottomText = String.Join(",", fullTest.Instructions.BottomText.Where(s => !String.IsNullOrEmpty(s)));
                testInstructions.TopText = String.Join(",", fullTest.Instructions.TopText.Where(s => !String.IsNullOrEmpty(s)));
                this.Context.Add(testInstructions);
                var instTracking = Context.Entry(testInstructions);
                instTracking.State = Microsoft.Data.Entity.EntityState.Modified;
                instTracking.Property("BottomText").IsModified = true;
                instTracking.Property("TopText").IsModified = true;
            }

            this.Context.SaveChanges();
        }
    }
}
