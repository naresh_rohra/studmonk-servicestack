﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class QuestionOfDays : RepositoryBase<Model.QuestionOfDay, Guid>, ModelRepository.QuestionOfDays
    {
        public QuestionOfDays(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.QuestionOfDays);
        }

        public Model.QuestionOfDay[] GetAll()
        {
            return Context.QuestionOfDays.ToArray();
        }

        public Model.QuestionOfDay GetQuestionOfTheDay(Guid streamId)
        {
            StudMonk.ServiceStack.Model.Question question = new Model.Question();

            var commonSubjects = new Guid[] { Model.Constants.SubjectId.PHYSICS, Model.Constants.SubjectId.CHEMISTRY };

            Model.QuestionOfDay questionOfDay = (from q in Context.QuestionOfDays
                                                 join qs in Context.Questions on q.QuestionId equals qs.Id
                                                 join c in Context.Chapters on qs.ChapterId equals c.Id
                                                 join s in Context.Subjects on c.SubjectId equals s.Id
                                                 join ss in Context.StreamSubjects on s.Id equals ss.SubjectId
                                                 where q.CreatedOn.ToString("yyyyMMdd") == DateTime.Now.ToString("yyyyMMdd")
                                                    && (ss.Id == streamId || (streamId == Guid.Empty && commonSubjects.Contains(s.Id)))
                                                 select new Model.QuestionOfDay
                                                 {
                                                     QuestionOfDayId = q.QuestionOfDayId,
                                                     QuestionId = q.QuestionId,
                                                     Question = new Model.Question
                                                     {
                                                         Id = qs.Id,
                                                         QuestionType = qs.QuestionType,
                                                         Text = qs.Text,
                                                         TextType = qs.TextType,
                                                         DifficultyLevel = qs.DifficultyLevel,
                                                         Hint = qs.Hint,
                                                         Explanation = qs.Explanation,
                                                         PreviousAppearances = qs.PreviousAppearances,
                                                         ChapterId = qs.ChapterId,
                                                         QualityStatus = qs.QualityStatus,
                                                         QuestionNo = qs.QuestionNo,
                                                         AnswerChoices = (from ac in Context.AnswerChoices where ac.QuestionId == qs.Id select ac).ToArray()
                                                     },
                                                     CreatedOn = q.CreatedOn
                                                 }).FirstOrDefault();

            if (questionOfDay == null)
            {
                //select random question
                List<Guid> lastThreeMonthsQuestionOfDay = (from q in Context.Questions
                                                           join qd in Context.QuestionOfDays on q.Id equals qd.QuestionId
                                                           where qd.CreatedOn >= DateTime.Now.AddMonths(-3)
                                                           select q.Id).ToList<Guid>();

                question = (from
                                q in Context.Questions
                            join c in Context.Chapters on new { Id = q.ChapterId, Active = true }  equals new { c.Id, c.Active }
                            join s in Context.Subjects on c.SubjectId equals s.Id
                            join ss in Context.StreamSubjects on s.Id equals ss.SubjectId
                            where
                               (q.Active == true) && (q.QualityStatus == Model.QualityStatus.Reviewed) && !lastThreeMonthsQuestionOfDay.Contains(q.Id)
                               && (ss.Id == streamId || (streamId == Guid.Empty && commonSubjects.Contains(s.Id)))
                            orderby
                                Guid.NewGuid()
                            select
                                new Model.Question
                                {
                                    Id = q.Id,
                                    QuestionType = q.QuestionType,
                                    Text = q.Text,
                                    TextType = q.TextType,
                                    DifficultyLevel = q.DifficultyLevel,
                                    Hint = q.Hint,
                                    Explanation = q.Explanation,
                                    PreviousAppearances = q.PreviousAppearances,
                                    ChapterId = q.ChapterId,
                                    QualityStatus = q.QualityStatus,
                                    QuestionNo = q.QuestionNo,
                                    AnswerChoices = (from ac in Context.AnswerChoices where ac.QuestionId == q.Id select ac).ToArray()
                                }).Take(1).FirstOrDefault();


                if (question != null)
                {
                    //set todays question of day
                    questionOfDay = new Model.QuestionOfDay { Id = Guid.NewGuid(), QuestionId = question.Id, CreatedOn = DateTime.Now };
                    Context.QuestionOfDays.Add(questionOfDay);
                    Context.SaveChanges();

                    questionOfDay.Question = question;
                }
                else
                {
                    throw new Exception("Not enough questions in repository");
                }
            }

            return questionOfDay;
        }

        public Model.Subject GetSubjectByChapterId(string chapterId)
        {

            Model.Subject subject = (
                                    from q in Context.Subjects
                                    join t in Context.Chapters on q.Id equals t.SubjectId
                                    where t.Id == new Guid(chapterId)
                                    select q
                                    ).FirstOrDefault();
            return subject;
        }

        public Model.QuestionOfDayAttempt GetUserAttempt(string userId, string questionOfDayId)
        {

            Model.QuestionOfDayAttempt questionOfDayAttempt = (
                                    from q in Context.QuestionOfDayAttempts
                                    join t in Context.QuestionOfDays on q.QuestionOfDayId equals t.QuestionOfDayId
                                    where (q.UserId == userId) && (t.QuestionOfDayId == new Guid(questionOfDayId))
                                    select q
                                    ).FirstOrDefault();
            return questionOfDayAttempt;
        }

        public Model.QuestionOfDayAnalytics GetQuestionOfDayAnalytics(string userId, string questionOfDayId)
        {
            Model.QuestionOfDayAnalytics questionOfDayAnalytics = Context.GetUserStreak(userId, questionOfDayId);
            return questionOfDayAnalytics;
        }

        public Model.QuestionOfDay GetQuestionOfDayById(string questionOfDayId)
        {

            Model.QuestionOfDay _questionOfDay = (
                                                    from q in Context.QuestionOfDays
                                                    where q.QuestionOfDayId == new Guid(questionOfDayId)
                                                    select q
                                                ).FirstOrDefault();
            return _questionOfDay;
        }

        public Model.Question GetQuestionByQuestionOfDay(string questionOfDayId)
        {
            Model.Question question = (
                                     from q in Context.Questions
                                     join t in Context.QuestionOfDays on q.Id equals t.QuestionId
                                     where t.QuestionOfDayId == new Guid(questionOfDayId)
                                     select new Model.Question
                                     {
                                         Id = q.Id,
                                         QuestionType = q.QuestionType,
                                         Text = q.Text,
                                         TextType = q.TextType,
                                         DifficultyLevel = q.DifficultyLevel,
                                         Hint = q.Hint,
                                         Explanation = q.Explanation,
                                         PreviousAppearances = q.PreviousAppearances,
                                         ChapterId = q.ChapterId,
                                         QualityStatus = q.QualityStatus,
                                         QuestionNo = q.QuestionNo,
                                         AnswerChoices = (from ac in Context.AnswerChoices where ac.QuestionId == q.Id select ac).ToArray()
                                     }
                                     ).FirstOrDefault();
            return question;
        }
    }
}
