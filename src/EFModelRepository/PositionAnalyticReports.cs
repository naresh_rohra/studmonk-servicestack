﻿using System;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class PositionAnalyticReport : RepositoryBase<Model.PositionAnalyticReport, Guid>, ModelRepository.PositionAnalyticReports
    {
        public PositionAnalyticReport(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.PositionAnalyticReports);
        }
        public Model.PositionAnalyticReport GetPositionAnalyticReport(string userId, string subjectId)
        {
            Model.PositionAnalyticReport positionAnalyticReport = Context.GetPositionAnalytic(userId, subjectId);
            return positionAnalyticReport;
        }

    }
}
