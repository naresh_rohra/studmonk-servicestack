﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class StudentEnrollmentDetails : RepositoryBase<Model.StudentEnrollmentDetail, Guid>, ModelRepository.StudentEnrollmentDetails
    {
        public StudentEnrollmentDetails(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.StudentEnrollmentDetail);
        }

        public Model.StudentEnrollmentDetail GetForStudent(string userId)
        {
            var enrollmentDetail = from ed in _entitySet
                                   where ed.UserId == userId
                                   select ed;

            return enrollmentDetail.FirstOrDefault();
        }
    }
}
