﻿using System;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class TestAttemptsSummary : RepositoryBase<Model.TestAttemptSummary, Guid>, ModelRepository.TestAttemptsSummary
    {
        public TestAttemptsSummary(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.TestAttemptsSummary);
        }

        protected override void BeforeSaveNew(Model.TestAttemptSummary[] testAttempts)
        {
            foreach(var testAttempt in testAttempts)
            {
                var attemptId = Guid.NewGuid();
                testAttempt.Id = attemptId;

                foreach (var attemptDetail in testAttempt.AttemptDetails)
                {
                    attemptDetail.Id = attemptId;
                    attemptDetail.CreatedOn = DateTime.UtcNow;
                }
            }
        }

        protected override void BeforeSaveExisting(Model.TestAttemptSummary testAttempt)
        {
                var attemptId = testAttempt.Id;
                foreach (var attemptDetail in testAttempt.AttemptDetails)
                {
                    attemptDetail.Id = attemptId;
                    attemptDetail.CreatedOn = DateTime.UtcNow;
                }
        }
    }
}
