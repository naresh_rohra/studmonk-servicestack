﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class QuestionBookmarks : RepositoryBase<Model.QuestionBookmark, Guid>, ModelRepository.QuestionBookmarks
    {
        protected override bool IsCompositeRepository
        {
            get
            {
                return true;
            }
        }

        public QuestionBookmarks(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.QuestionBookmarks);
        }

        public Model.Question[] GetForUser(string userId, int pageNo, int pageSize)
        {
            pageNo = pageNo >= 1 ? pageNo : 1;

            var questions = (from q in Context.Questions
                         join qb in Context.QuestionBookmarks on q.Id equals qb.Id
                         where qb.UserId == userId
                         select q).Skip((pageNo - 1) * pageSize).Take(pageSize).ToArray();

            return new Questions(this.ConnectionString).GenerateQuestionsListForQuestions(questions, 0, true).Questions;
        }

        public Model.Question[] GetForSubject(Guid subjectId)
        {
            var questions = (from q in Context.Questions
                         join qb in Context.QuestionBookmarks on q.Id equals qb.Id
                         join c in Context.Chapters on q.ChapterId equals c.Id
                         where c.SubjectId == subjectId
                         select q).ToArray();

            return new Questions(this.ConnectionString).GenerateQuestionsListForQuestions(questions, 0, true).Questions;
        }

        public Model.Question[] GetForChapter(Guid chapterId)
        {
            var questions = (from q in Context.Questions
                         join qb in Context.QuestionBookmarks on q.Id equals qb.Id
                         where q.ChapterId == chapterId
                         select q).ToArray();

            return new Questions(this.ConnectionString).GenerateQuestionsListForQuestions(questions, 0, true).Questions;
        }

        public new Model.QuestionBookmark[] SaveNew(Model.QuestionBookmark[] entities)
        {
            foreach (var entity in entities)
            {
               var duplicate = Context.QuestionBookmarks.FirstOrDefault(e => e.Id == entity.Id && e.UserId == entity.UserId);
               if (duplicate == null)
                {
                    entity.CreatedOn = DateTime.UtcNow;
                    _entitySet.Add(entity);
                    IgnoreFieldsForSaveNew(entity);
                }
            }
            this.SaveContextChanges();
            AfterSaveNew(entities);
            return entities;
        }
    }
}
