﻿using Autofac;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class InstanceActivator : ModelRepository.InstanceActivator
    {
        string _connectionString;
        IContainer _container;
        public InstanceActivator(string connectionString)
        {
            _connectionString = connectionString;
            BuildContainer();
        }

        protected void BuildContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterType<Chapters>().As<ModelRepository.Chapters>();
            builder.RegisterType<QuestionBookmarks>().As<ModelRepository.QuestionBookmarks>();
            builder.RegisterType<Questions>().As<ModelRepository.Questions>();
            builder.RegisterType<Subjects>().As<ModelRepository.Subjects>();
            builder.RegisterType<TestAttemptsSummary>().As<ModelRepository.TestAttemptsSummary>();
            builder.RegisterType<TestChapters>().As<ModelRepository.TestChapters>();
            builder.RegisterType<TestQuestions>().As<ModelRepository.TestQuestions>();
            builder.RegisterType<Tests>().As<ModelRepository.Tests>();
            builder.RegisterType<Users>().As<ModelRepository.Users>();
            builder.RegisterType<QuestionBookmarks>().As<ModelRepository.QuestionBookmarks>();
            builder.RegisterType<QuestionOfDays>().As<ModelRepository.QuestionOfDays>();
            builder.RegisterType<QuestionOfDayAttempts>().As<ModelRepository.QuestionOfDayAttempts>();
            builder.RegisterType<PositionAnalyticReport>().As<ModelRepository.PositionAnalyticReports>();
            builder.RegisterType<StudentEnrollmentDetails>().As<ModelRepository.StudentEnrollmentDetails>();
            builder.RegisterType<TestScoringSystems>().As<ModelRepository.TestScoringSystems>();
            builder.RegisterType<Tags>().As<ModelRepository.Tags>();
            builder.RegisterType<Tenants>().As<ModelRepository.Tenants>();
            builder.RegisterType<AuditLogs>().As<ModelRepository.AuditLogs>();
            _container = builder.Build();
        }
        
        public TModel GetInstance<TModel>() where TModel : ModelRepository.Repository
        {
            var parameters = new NamedParameter[1];
            parameters[0] = new NamedParameter("connectionString", _connectionString);
            return _container.Resolve<TModel>(parameters);
        }
    }
}
