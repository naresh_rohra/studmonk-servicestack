﻿using System;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class TestChapters : RepositoryBase<Model.TestChapter, Guid>, ModelRepository.TestChapters
    {
        public TestChapters(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.TestChapters);
        }
    }
}
