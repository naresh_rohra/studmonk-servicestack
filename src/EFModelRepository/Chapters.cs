﻿using System;
using System.Linq;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class Chapters : RepositoryBase<Model.Chapter, Guid>, ModelRepository.Chapters
    {
        public Chapters(string connectionString) : base(connectionString)
        {
            this.SetEntitySet(this.Context.Chapters);
            this._fieldsIgnoredForSaveExisting.AddRange(new string[] { "StreamId", "SubjectId" });
        }

        public Model.Chapter[] GetAllForSubject(Guid subjectId, bool includeInActive = false)
        {
            return Context.Chapters.Where(c => 
                c.SubjectId == subjectId 
                && (c.Active == true || includeInActive == true)
                ).OrderBy(c => c.ChapterNo).ToArray();
        }

        public Model.TestChapter[] GetAllOfTest(Guid testId)
        {
            var testChapters = (from tc in Context.TestChapters
                     where tc.Id == testId
                     select tc).ToArray();
            return testChapters;
        }

        public Model.Chapter[] GetForChapterIds(Guid[] chapterIds, bool includeInActive = true)
        {
            return Context.Chapters.Where(c => chapterIds.Contains(c.Id)).ToArray();
        }

        public override void Delete(Guid id)
        {
            var chapter = Get(id);
            if (IsReferencedByOtherEntity(chapter))
            {
                SoftDelete(chapter);
            }
            else
            {
                HardDelete(chapter);
            }
            this.SaveContextChanges();
        }

        private bool IsReferencedByOtherEntity(Model.Chapter chapter)
        {
            var referencedQuestions = (new Questions(ConnectionString)).GetForChapter(new ModelRepository.Types.QuestionListRequest()
            {
                ChapterIds = new Guid[] { chapter.Id },
                IncludeFullDetails = false
            });

            if (referencedQuestions.Questions.Length > 0)
                return true;

            var referencedTests = (new Tests(ConnectionString)).GetForChapter(chapter.Id);

            if (referencedTests.Length > 0)
                return true;

            return false;
        }

        private void SoftDelete(Model.Chapter chapter)
        {
            chapter.Active = false;
        }

        protected override void BeforeSaveExisting(Model.Chapter chapter)
        {
            Context.Entry(chapter).Property("Active").IsModified = false;
        }

        protected override void BeforeSaveNew(Model.Chapter[] chapters)
        {
            foreach (var chapter in chapters)
            {
                chapter.Active = true;
            }
        }
    }
}
