﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class Tags : RepositoryBase<Model.Tag, Int64>, ModelRepository.Tags
    {
        public Tags(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.Tags);
        }

        public Model.Tag[] CreateIfNotExists(string[] names, string userId)
        {
            IList<Model.Tag> allTags = new List<Model.Tag>();
            IList<Model.Tag> newTags = new List<Model.Tag>();

            var alreadyCreatedTags = GetByNames(names);

            var notCreatedTags = from name in names
                                 join act in alreadyCreatedTags on name.ToLower() equals act.Name.ToLower() into newNames
                                 from nn in newNames.DefaultIfEmpty()
                                 where nn == default(Model.Tag)
                                 select name;

            foreach (var name in notCreatedTags)
            {
                newTags.Add(new Model.Tag()
                {
                    Name = name,
                    CreatedBy = userId,
                    CreatedOn = DateTime.UtcNow,
                    IsActive = true
                });
            }
            SaveNew(newTags.ToArray());
            allTags = alreadyCreatedTags.Concat(newTags).ToList();
            return allTags.ToArray();
        }

        public Model.Tag[] GetByNames(string[] names)
        {
            var tags = from t in Context.Tags
                       where names.Contains(t.Name, StringComparer.CurrentCultureIgnoreCase)
                       select t;
            return tags.ToArray();
        }
    }
}
