﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class AuditLogs : RepositoryBase<Model.AuditLog, Int64>, ModelRepository.AuditLogs
    {
        public AuditLogs(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.AuditLogs);
        }

        public Model.AuditLogReport[] GetRecentUserAction(Model.DTO.SearchAuditLogsRequest request)
        {
            return Context.GetRecentUserAction(request);
        }

        public Model.AuditLog[] GetDump(Model.DTO.SearchAuditLogsRequest request)
        {
            return Context.GetAuditLogDump(request);
        }

        public Model.AuditLogStat[] GetAuditLogStats(Model.DTO.SearchAuditLogsRequest request)
        {
            return Context.GetAuditLogStats(request);
        }

        public Model.DTO.UserRank[] GetFullTestTopScorers(Model.DTO.TopScorersRequest request)
        {
            return Context.GetFullTestTopScorers(request);
        }

        public Model.DTO.UserTestScore[] GetFullTestScoresForUser(Model.DTO.UserScoresRequest request)
        {
            return Context.GetFullTestScoresForUser(request);
        }
    }
}
