﻿using System;
using System.Linq;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class Subjects : RepositoryBase<Model.Subject, Guid>, ModelRepository.Subjects
    {
        public Subjects(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.Subjects);
            this._fieldsIgnoredForSaveExisting.AddRange(new string[] { "InstitutionId" });
        }

        public Model.Subject[] GetAll()
        {
            return Context.Subjects.ToArray();
        }

        public Model.Subject[] GetForStream(Guid streamId)
        {
            return (from subject in Context.Subjects
                join streamSubs in Context.StreamSubjects on subject.Id equals streamSubs.SubjectId
                where streamSubs.Id == streamId
                select subject).ToArray();
        }

        public Model.DTO.Concept[] GetAllConcepts(Guid subjectId)
        {
            return Context.GetConceptsInfoForSubject(subjectId);
        }

        protected override void BeforeSaveExisting(Model.Subject entity)
        {
            this.Context.Entry(entity).Property("InstitutionId").IsModified = false;
        }
    }
}
