﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class Tenants : RepositoryBase<Model.Tenant, string>, ModelRepository.Tenants
    {
        public Tenants(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.Tenants);
        }
    }
}
