﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Data.Entity;
using System.Linq.Expressions;
using Microsoft.Data.Entity.ChangeTracking;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class Questions : RepositoryBase<Model.Question, Guid>, ModelRepository.Questions
    {
        private static int QuestionsPerPage = 20;
        public static Guid MathematicSubjectId = Guid.Parse("46D3900A-D1E4-4537-8AEE-137A29120069");

        public Questions(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.Questions);
            this._fieldsIgnoredForSaveNew.AddRange(new string[] { "QuestionNo" });
            this._fieldsIgnoredForSaveExisting.AddRange(new string[] { "Active", "QuestionNo" });
        }

        public override Model.Question Get(Guid id)
        {
            var result = (from q in Context.Questions
                          join c in Context.Chapters on q.ChapterId equals c.Id
                          where q.Id == id
                          select new Model.Question
                          {
                              Id = q.Id,
                              QuestionType = q.QuestionType,
                              Text = q.Text,
                              TextType = q.TextType,
                              DifficultyLevel = q.DifficultyLevel,
                              Hint = q.Hint,
                              Explanation = q.Explanation,
                              PreviousAppearances = q.PreviousAppearances,
                              ChapterId = q.ChapterId,
                              SubjectId = c.SubjectId,
                              QualityStatus = q.QualityStatus,
                              QuestionNo = q.QuestionNo,
                              Sources = q.Sources
                          }).FirstOrDefault();

            if (result != null)
            {
                result.AnswerChoices = (from ac in Context.AnswerChoices where ac.QuestionId == id select ac).ToArray();
                result.TaggedConcepts = GetTaggedConcepts(result.Id);
                result.TaggedRelevances = GetTaggedRelevances(result.Id);
                result.Size = GetQuestionSize(result);

            }

            return result;
        }

        private string[] GetTaggedConcepts(Guid questionId)
        {
            var tags = GetConceptTagsForQuestion(questionId);
            if (tags != null && tags.Count() > 0)
            {
                return tags.Select(t => t.Name).ToArray();
            }
            return new string[] { };
        }

        private string[] GetTaggedRelevances(Guid questionId)
        {
            var tags = GetRelevanceTagsForQuestion(questionId);
            if (tags != null && tags.Count() > 0)
            {
                return tags.Select(t => t.Name).ToArray();
            }
            return new string[] { };
        }

        public override Model.Question[] Get(Guid[] ids)
        {
            var questions = from q in Context.Questions
                            where ids.Contains(q.Id)
                            select q;

            return GenerateQuestionsListForQuestions(questions.ToArray(), 0, true).Questions.ToArray();
        }

        public Model.DTO.QuestionsList GetForChapter(ModelRepository.Types.QuestionListRequest request)
        {
            return GetForSubjectChaptersWithDifficultyLevel(request);
        }

        public Model.DTO.QuestionsList SearchForChapter(Guid chapterId, string searchText, Guid exclusionQuestionId)
        {
            return GetForChapterWithText(chapterId, searchText, exclusionQuestionId, false);
        }

        public Model.Question[] GetCandidateQuestionsForTest(ModelRepository.Types.TestCandidateQuestionsRequest request)
        {
            return GetForSubjectChaptersWithDifficultyLevel(new ModelRepository.Types.QuestionListRequest()
            {
                SubjectId = request.SubjectId,
                ChapterIds = request.ChapterIds,
                DifficultLevel = request.DifficultLevel,
                ConceptText = request.Concepts,
                RelevanceText = request.Relevances,
                QualityStatus = Model.QualityStatus.Reviewed,
                PageNumber = 0,
                IncludeFullDetails = false,
                IncludeInActive = false
            }).Questions;
        }

        public Model.DTO.QuestionsList GetForSubjectChaptersWithDifficultyLevel(ModelRepository.Types.QuestionListRequest request)
        {
            var questions = Context.GetQuestionsList(request, 0, 0);
            return GenerateQuestionsListForQuestions(questions, request.PageNumber, request.IncludeFullDetails);
        }

        public Model.Question[] GetQuestionsForTest(Guid testId)
        {
            var question = from q in Context.Questions
                           join tq in Context.TestQuestions on q.Id equals tq.QuestionId
                           where
                              tq.Id == testId
                           orderby q.QuestionNo
                           select q;

            return GenerateQuestionsListForQuestions(question.ToArray(), 0, true).Questions.ToArray<Model.Question>();
        }

        public Model.DTO.SubjectWiseTestSection[] GetQuestionsForFullTest(Guid testId, int perQuestionDuration, string relevance)
        {
            List<Model.DTO.SubjectWiseTestSection> subjectWiseTestSection = new List<Model.DTO.SubjectWiseTestSection>();

            var result = from q in Context.Questions
                         join tq in Context.TestQuestions on q.Id equals tq.QuestionId
                         join c in Context.Chapters on q.ChapterId equals c.Id
                         join s in Context.Subjects on c.SubjectId equals s.Id
                         where tq.Id == testId
                         orderby s.Id
                         select new { Subject = s, Chapter = c, question = q };

            var subjectList = result.Select(x => new { x.Subject }).Distinct().Select(y => y).ToList();

            var questionIds = result.Select(r => r.question.Id).ToArray();

            foreach (var subject in subjectList)
            {
                var answerChoices = (from ac in Context.AnswerChoices
                                     where questionIds.Contains(ac.QuestionId)
                                     select ac).ToArray();

                var conceptTags = (from t in Context.Tags
                                   join ct in Context.QuestionConceptTags on t.Id equals ct.TagId
                                   where questionIds.Contains(ct.Id) && t.IsActive == true
                                   select new
                                   {
                                       Tag = t.Name,
                                       QuestionId = ct.Id,
                                       TagId = ct.TagId
                                   }).ToArray();

                var relevanceTags = (from t in Context.Tags
                                     join rt in Context.QuestionRelevanceTags on t.Id equals rt.TagId
                                     where questionIds.Contains(rt.Id) && t.IsActive == true
                                     select new
                                     {
                                         Tag = t.Name,
                                         QuestionId = rt.Id,
                                         TagId = rt.TagId
                                     }).ToArray();


                var questionsInSubject = (
                        from q in result
                        where q.Subject.Id == subject.Subject.Id
                        select
                            new Model.Question
                            {
                                Id = q.question.Id,
                                QuestionType = q.question.QuestionType,
                                Text = q.question.Text,
                                TextType = q.question.TextType,
                                DifficultyLevel = q.question.DifficultyLevel,
                                Hint = q.question.Hint,
                                Explanation = q.question.Explanation,
                                PreviousAppearances = q.question.PreviousAppearances,
                                ChapterId = q.question.ChapterId,
                                QualityStatus = q.question.QualityStatus,
                                QuestionNo = q.question.QuestionNo,
                                AnswerChoices = (from ac in answerChoices where ac.QuestionId == q.question.Id select ac).ToArray(),
                                Sources = q.question.Sources,
                                TaggedConcepts = conceptTags.Where(t => t.QuestionId == q.question.Id).Select(t => t.Tag).ToArray(),
                                TaggedRelevances = relevanceTags.Where(t => t.QuestionId == q.question.Id).Select(t => t.Tag).ToArray()
                            }
                         );

                subjectWiseTestSection.Add(new Model.DTO.SubjectWiseTestSection
                {
                    SubjectId = subject.Subject.Id,
                    SubjectName = subject.Subject.Name,
                    Questions = GenerateQuestionsListForQuestions(questionsInSubject.ToArray(), 0, true).Questions.ToArray<Model.Question>(),
                    NoOfQuestions = questionsInSubject.Count(),
                    DurationInMinutes = relevance == "JEE MAIN" ? questionsInSubject.Count() * 2 : (subject.Subject.Id == MathematicSubjectId) ? perQuestionDuration * 2 * questionsInSubject.Count() / 60 : perQuestionDuration * questionsInSubject.Count() / 60
                }
               );
            }
            return subjectWiseTestSection.ToArray();
        }

        public Model.DTO.SubjectWiseTestSection[] GetQuestionsForFullTestInfo(Guid testId, int perQuestionDuration, string relevance, bool includeDetails = true)
        {
            List<Model.DTO.SubjectWiseTestSection> subjectWiseTestSection = new List<Model.DTO.SubjectWiseTestSection>();

            var result = from q in Context.Questions
                         join tq in Context.TestQuestions on q.Id equals tq.QuestionId
                         join c in Context.Chapters on q.ChapterId equals c.Id
                         join s in Context.Subjects on c.SubjectId equals s.Id
                         where tq.Id == testId
                         orderby s.Id
                         select new { SubjectId = s.Id, SubjectName = s.Name, QuestionId = q.Id };

            var subjectList = result.Select(x => new { x.SubjectId, x.SubjectName }).Distinct().Select(y => y).ToList();

            foreach (var subject in subjectList)
            {
                var questionsInSubject = (
                                        from q in result
                                        where q.SubjectId == subject.SubjectId
                                        select
                                             new { q.QuestionId }
                                         );

                subjectWiseTestSection.Add(new Model.DTO.SubjectWiseTestSection
                {
                    SubjectId = subject.SubjectId,
                    SubjectName = subject.SubjectName,
                    NoOfQuestions = questionsInSubject.Count(),
                    DurationInMinutes = relevance == "JEE MAIN" ? questionsInSubject.Count() * 2 : (subject.SubjectId == MathematicSubjectId) ? perQuestionDuration * 2 * questionsInSubject.Count() / 60 : perQuestionDuration * questionsInSubject.Count() / 60,
                    DoNotSerializeQuestions = true
                }
               );

            }
            return subjectWiseTestSection.ToArray();
        }

        private Model.DTO.QuestionsList GetForChapterWithText(Guid chapterId, string searchText, Guid exclusionQuestionId, bool fullDetails = true)
        {
            IQueryable<Model.Question> questions;
            if (fullDetails)
            {
                questions = from q in Context.Questions
                            join c in Context.Chapters on q.ChapterId equals c.Id
                            where chapterId == q.ChapterId
                               && (q.Active == true)
                               && q.Text.Contains(searchText)
                               && (q.Id != exclusionQuestionId || exclusionQuestionId == Guid.Empty)
                            orderby q.QuestionNo
                            select q;
            }
            else
            {
                questions = from q in Context.Questions
                            join c in Context.Chapters on q.ChapterId equals c.Id
                            where chapterId == q.ChapterId
                               && (q.Active == true)
                               && q.Text.Contains(searchText)
                               && (q.Id != exclusionQuestionId || exclusionQuestionId == Guid.Empty)
                            orderby q.QuestionNo
                            select new Model.Question
                            {
                                Id = q.Id,
                                Text = q.Text,
                                QuestionNo = q.QuestionNo
                            };

            }

            var questionsList = new Model.DTO.QuestionsList() { Questions = questions.ToArray() };
            questionsList.TotalQuestions = questionsList.Questions.Length;
            return questionsList;
        }

        public Model.DTO.QuestionsList GenerateQuestionsListForQuestions(Model.Question[] questions, int pageNumber, bool includeFullDetails)
        {
            Model.Question[] questionsWithAnswers = null;
            var questionsArray = questions;

            if (includeFullDetails)
            {
                var questionIds = questionsArray.Select(q => q.Id).ToArray();

                var answerChoices = (from ac in Context.AnswerChoices
                                     where questionIds.Contains(ac.QuestionId)
                                     select ac).ToArray();

                var conceptTags = (from t in Context.Tags
                                   join ct in Context.QuestionConceptTags on t.Id equals ct.TagId
                                   where questionIds.Contains(ct.Id) && t.IsActive == true
                                   select new
                                   {
                                       Tag = t.Name,
                                       QuestionId = ct.Id,
                                       TagId = ct.TagId
                                   }).ToArray();

                var relevanceTags = (from t in Context.Tags
                                     join rt in Context.QuestionRelevanceTags on t.Id equals rt.TagId
                                     where questionIds.Contains(rt.Id) && t.IsActive == true
                                     select new
                                     {
                                         Tag = t.Name,
                                         QuestionId = rt.Id,
                                         TagId = rt.TagId
                                     }).ToArray();

                questionsWithAnswers = (from q in questionsArray
                                        select new Model.Question
                                        {
                                            Id = q.Id,
                                            QuestionType = q.QuestionType,
                                            Text = q.Text,
                                            TextType = q.TextType,
                                            DifficultyLevel = q.DifficultyLevel,
                                            Hint = q.Hint,
                                            Explanation = q.Explanation,
                                            PreviousAppearances = q.PreviousAppearances,
                                            ChapterId = q.ChapterId,
                                            QualityStatus = q.QualityStatus,
                                            QuestionNo = q.QuestionNo,
                                            Size = q.Size,
                                            AnswerChoices = answerChoices.Where(a => a.QuestionId == q.Id).ToArray(),
                                            Sources = q.Sources,
                                            TaggedConcepts = conceptTags.Where(t => t.QuestionId == q.Id).Select(t => t.Tag).ToArray(),
                                            TaggedRelevances = relevanceTags.Where(t => t.QuestionId == q.Id).Select(t => t.Tag).ToArray()
                                        }).ToArray();
            }
            else
            {
                questionsWithAnswers = (from q in questionsArray
                                        select new Model.Question
                                        {
                                            Id = q.Id
                                        }).ToArray();
            }

            var questionsList = new Model.DTO.QuestionsList
            {
                TotalQuestions = questionsArray.Count(),
                Questions = pageNumber == 0 ? questionsWithAnswers : questionsWithAnswers.Skip((pageNumber - 1) * QuestionsPerPage).Take(QuestionsPerPage).ToArray()
            };
            return questionsList;
        }

        private void UpdateQuestionsCountForChapter(Guid chapterId)
        {
            this.Context.UpdateQuestionsCountForChapter(chapterId);
        }

        protected override void BeforeSaveExisting(Model.Question question)
        {
            foreach (var choice in question.AnswerChoices)
            {
                var choiceTrackingObject = Context.Entry(choice);
                if (choiceTrackingObject.State == EntityState.Added)
                {
                    choice.CreatedOn = DateTime.UtcNow;
                }
                else if (choiceTrackingObject.State == EntityState.Modified)
                {
                    choiceTrackingObject.Property("CreatedOn").IsModified = false;
                }
                else if (choiceTrackingObject.State == EntityState.Unchanged)
                {
                    choiceTrackingObject.State = EntityState.Modified;
                    choiceTrackingObject.Property("CreatedOn").IsModified = false;
                }
            }
            question.Size = GetQuestionSize(question);
        }

        private bool SetTrackingStatusForTag(Model.QuestionTag questionTag)
        {
            var trackingObject = Context.Entry(questionTag);
            if (questionTag.State == Model.ModificationState.Added)
            {
                trackingObject.State = EntityState.Added;
            }
            else if (questionTag.State == Model.ModificationState.Deleted)
            {
                trackingObject.State = EntityState.Deleted;
            }
            return true;
        }

        private long GetQuestionSize(Model.Question question)
        {
            long size = question.Text.Length;
            size += question.Explanation != null ? question.Explanation.Length : 0;
            size += question.Hint != null ? question.Explanation.Length : 0;

            var answerChoices = question.AnswerChoices.ToArray();
            if (answerChoices.Length >= 4)
            {
                size += answerChoices[0].Text.Length + answerChoices[1].Text.Length + answerChoices[2].Text.Length + answerChoices[3].Text.Length;
            }
            return size;
        }

        protected override void AfterSaveExisting(Model.Question question)
        {
            this.UpdateQuestionsCountForChapter(question.ChapterId);
            if (question.ConceptTags != null)
            {
                foreach (var conceptTag in question.ConceptTags)
                {
                    Context.QuestionConceptTags.Add(conceptTag);
                    SetTrackingStatusForTag(conceptTag);
                }
            }
            if (question.RelevanceTags != null)
            {
                foreach (var relevanceTag in question.RelevanceTags)
                {
                    Context.QuestionRelevanceTags.Add(relevanceTag);
                    SetTrackingStatusForTag(relevanceTag);
                }
            }
            Context.SaveChanges();
        }

        protected override void BeforeSaveNew(Model.Question[] questions)
        {
            foreach (var question in questions)
            {
                question.Active = true;
                if (question.AnswerChoices != null)
                {
                    foreach (var choice in question.AnswerChoices)
                    {
                        choice.Id = Guid.NewGuid();
                        choice.CreatedOn = DateTime.UtcNow;
                    }
                }
                question.Size = GetQuestionSize(question);
            }
        }

        protected override void AfterSaveNew(Model.Question[] questions)
        {
            foreach (var chapterId in questions.Select(q => q.ChapterId).Distinct())
            {
                this.UpdateQuestionsCountForChapter(chapterId);
            }

            foreach (var question in questions)
            {
                if (question.ConceptTags != null)
                {
                    foreach (var conceptTag in question.ConceptTags)
                    {
                        conceptTag.Id = question.Id;
                        Context.QuestionConceptTags.Add(conceptTag);
                        SetTrackingStatusForTag(conceptTag);
                    }
                }
                if (question.RelevanceTags != null)
                {
                    foreach (var relevanceTag in question.RelevanceTags)
                    {
                        relevanceTag.Id = question.Id;
                        Context.QuestionRelevanceTags.Add(relevanceTag);
                        SetTrackingStatusForTag(relevanceTag);
                    }
                }
            }
            Context.SaveChanges();
        }

        public override void Delete(Guid id)
        {
            var question = Get(id);
            SoftDelete(question);
            this.SaveContextChanges();
            this.UpdateQuestionsCountForChapter(question.ChapterId);
        }

        public override void Delete(Guid[] ids)
        {
            var questions = GetQuestionsForIds(ids);
            foreach (var question in questions)
            {
                SoftDelete(question);
            }
            this.SaveContextChanges();

            foreach (var chapterId in questions.Select(q => q.ChapterId).Distinct())
            {
                Context.UpdateQuestionsCountForChapter(chapterId);
            }
        }

        private bool IsReferencedByOtherEntity(Model.Question question)
        {
            var referencedTests = Context.TestQuestions.Where(tq => tq.QuestionId == question.Id);

            if (referencedTests.Count() > 0)
                return true;

            return false;
        }

        private void SoftDelete(Model.Question question)
        {
            question.Active = false;
        }

        protected override void HardDelete(Model.Question question)
        {
            question.AnswerChoices = (from ac in Context.AnswerChoices where ac.QuestionId == question.Id select ac).ToList();

            if (question != null)
            {
                _entitySet.Attach(question);
                _entitySet.Remove(question);
                foreach (var choice in question.AnswerChoices)
                {
                    Context.AnswerChoices.Attach(choice);
                    Context.AnswerChoices.Remove(choice);
                }
            }
        }

        private Model.Question[] GetQuestionsForIds(Guid[] questionIds)
        {
            var result = Context.Questions.Where(q => questionIds.Contains(q.Id));
            return result.ToArray();
        }

        public void MarkQuestionsAsNeedsCorrection(Guid[] questionIds)
        {
            var questions = GetQuestionsForIds(questionIds);
            foreach (var question in questions)
            {
                question.QualityStatus = Model.QualityStatus.NeedsCorrection;
                this.Context.Attach(question);
            }
            this.SaveContextChanges();

            foreach (var chapterId in questions.Select(q => q.ChapterId).Distinct())
            {
                Context.UpdateQuestionsCountForChapter(chapterId);
            }
        }

        public void MarkQuestionsAsReviewed(Guid[] questionIds)
        {
            var questions = GetQuestionsForIds(questionIds);
            foreach (var question in questions)
            {
                question.QualityStatus = Model.QualityStatus.Reviewed;
                this.Context.Attach(question);
            }
            this.SaveContextChanges();

            foreach (var chapterId in questions.Select(q => q.ChapterId).Distinct())
            {
                Context.UpdateQuestionsCountForChapter(chapterId);
            }
        }

        public void MarkQuestionsAsNotReviewed(Guid[] questionIds)
        {
            var questions = GetQuestionsForIds(questionIds);
            foreach (var question in questions)
            {
                question.QualityStatus = Model.QualityStatus.NotReviewed;
                this.Context.Attach(question);
            }
            this.SaveContextChanges();

            foreach (var chapterId in questions.Select(q => q.ChapterId).Distinct())
            {
                Context.UpdateQuestionsCountForChapter(chapterId);
            }
        }

        public Model.Tag[] SearchConceptTags(string searchText)
        {
            var tags = (from t in Context.Tags
                        join ct in Context.QuestionConceptTags on t.Id equals ct.TagId
                        where t.IsActive == true
                        && t.Name.ToLower().StartsWith(searchText.ToLower())
                        select t).Distinct();

            return tags.ToArray();
        }

        public Model.Tag[] SearchRelevanceTags(string searchText)
        {
            var tags = (from t in Context.Tags
                        join rt in Context.QuestionRelevanceTags on t.Id equals rt.TagId
                        where t.IsActive == true
                         && t.Name.ToLower().StartsWith(searchText.ToLower())
                        select t).Distinct();

            return tags.ToArray();
        }

        public Model.Tag[] GetConceptTagsForQuestion(Guid questionId)
        {
            var tags = from t in Context.Tags
                       join ct in Context.QuestionConceptTags on t.Id equals ct.TagId
                       where ct.Id == questionId
                       && ct.IsActive == true
                       && t.IsActive == true
                       select t;

            return tags.ToArray();
        }

        public Model.Tag[] GetRelevanceTagsForQuestion(Guid questionId)
        {
            var tags = from t in Context.Tags
                       join ct in Context.QuestionRelevanceTags on t.Id equals ct.TagId
                       where ct.Id == questionId
                       && ct.IsActive == true
                       && t.IsActive == true
                       select t;

            return tags.ToArray();
        }

        public Model.UserQuestionFlag SaveUserQuestionFlag(Model.UserQuestionFlag questionFlag)
        {
            var existingQuestionFlag = (from flag in Context.UserQuestionFlags
                                        where flag.Id == questionFlag.Id
                                        && flag.UserId == questionFlag.UserId
                                        && flag.Flag == questionFlag.Flag
                                        select flag).FirstOrDefault();

            if (existingQuestionFlag == null)
            {
                questionFlag.CreatedOn = DateTime.UtcNow;
                questionFlag.CreatedBy = questionFlag.UserId;
                Context.Add(questionFlag);
                Context.SaveChanges();
            }
            return questionFlag;
        }
    }
}