﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class Users : RepositoryBase<Model.User, Guid>, ModelRepository.Users
    {
        public Users(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.Users);
        }

        public Model.User GetByDeviceId(string deviceId)
        {
            return _entitySet.FirstOrDefault(u => u.DeviceId == deviceId);
        }

        public Model.UserAuthorizedDevice[] GetUserAuthorizedDevices(string userId, bool includeInActive = false)
        {
            return Context.UserAuthorizedDevices.Where(uad => uad.UserId == userId && (uad.Active == true || includeInActive == true)).ToArray();
        }

        public void UpdateUserAuthorizedDevices(string userId, string[] deviceIds, string modifiedBy)
        {
            var currentAuthorizedDevices = GetUserAuthorizedDevices(userId);
            //remove devices
            foreach (var removedDevice in currentAuthorizedDevices.Where(c => !deviceIds.Any(d => d == c.Id)))
            {
                removedDevice.Active = false;
                removedDevice.LastModifiedOn = DateTime.UtcNow;
                removedDevice.LastModifiedBy = modifiedBy;
                this.Context.UserAuthorizedDevices.Attach(removedDevice);
                this.SetModifiedFlagForFields(this.Context.Entry<Model.UserAuthorizedDevice>(removedDevice), new string[] { "Active", "LastModifiedOn", "LastModifiedBy" }, true);
            }
            this.SaveContextChanges();
        }

        public Model.Tenant GetTenant(string tenantId)
        {
            return Context.Tenants.Where(t => t.Id == tenantId).FirstOrDefault();
        }

        public Model.UserAuthorizedDevice SaveUserAuthorizedDevice(Model.UserAuthorizedDevice authorizedDevice)
        {
            var existingDevice = GetUserAuthorizedDevices(authorizedDevice.UserId, true).Where(uad => uad.Id == authorizedDevice.Id).FirstOrDefault();
            if(existingDevice == null)
            { 
                authorizedDevice.CreatedOn = DateTime.UtcNow;
                authorizedDevice.Active = true;
                Context.UserAuthorizedDevices.Add(authorizedDevice);
                foreach (string field in this._fieldsIgnoredForSaveNew)
                {
                    Context.Entry(authorizedDevice).Property(field).IsModified = false;
                }
            }
            else
            {
                existingDevice.Active = true;
                existingDevice.LastModifiedBy = authorizedDevice.UserId;
                existingDevice.LastModifiedOn = DateTime.UtcNow;
                this.Context.UserAuthorizedDevices.Attach(existingDevice);
                SetModifiedFlagForFields(this.Context.Entry<Model.UserAuthorizedDevice>(existingDevice), new string[] { "Active", "LastModifiedBy",  "LastModifiedOn"}, true);
            }
            this.SaveContextChanges();
            return authorizedDevice;
        }

        public Model.Tenant[] GetAllTenants()
        {
            return (from tenant in Context.Tenants
                    where tenant.Active == true
                    select tenant).ToArray();
        }
        
        public Model.DTO.UserRank GetUserRanking(Model.DTO.UserRankingRequest request)
        {
            return Context.GetUserRanking(request);
        }
    }
}
