﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class TestScoringSystems : RepositoryBase<Model.TestScoringSystem, Guid>, ModelRepository.TestScoringSystems
    {
        public TestScoringSystems(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.TestScoringSystems);
        }

        public Model.TestScoringSystem[] GetAll()
        {
            return Context.TestScoringSystems.ToArray();
        }

        public Model.DTO.ScoringSystem[] GetByStream(Guid streamId)
        {
            return (from tss in Context.TestScoringSystems
                    join s in Context.Subjects on tss.SubjectId equals s.Id
                    where tss.StreamId == streamId && tss.Active == true
                    select new Model.DTO.ScoringSystem
                    {
                        SubjectId = s.Id,
                        ScoringSystemId = tss.Id,
                        SubjectName = s.Name,
                        CorrectAnswer = tss.CorrectAnswer,
                        InCorrectAnswer = tss.InCorrectAnswer,
                        Unanswered = tss.Unanswered
                    }).ToArray();
        }

        public Model.DTO.ScoringSystem[] GetById(Guid id)
        {
            return (from tss in Context.TestScoringSystems
                    join s in Context.Subjects on tss.SubjectId equals s.Id
                    where tss.Id == id && tss.Active == true
                    select new Model.DTO.ScoringSystem
                    {
                        SubjectId = s.Id,
                        SubjectName = s.Name,
                        ScoringSystemId = tss.Id,
                        CorrectAnswer = tss.CorrectAnswer,
                        InCorrectAnswer = tss.InCorrectAnswer,
                        Unanswered = tss.Unanswered
                    }).ToArray();
        }
    }
}
