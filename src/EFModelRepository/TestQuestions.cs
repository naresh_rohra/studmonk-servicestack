﻿using System;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class TestQuestions : RepositoryBase<Model.TestQuestion, Guid>, ModelRepository.TestQuestions
    {
        public TestQuestions(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.TestQuestions);
        }
    }
}
