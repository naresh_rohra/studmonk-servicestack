﻿using System;
using System.Linq;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class QuestionOfDayAttempts : RepositoryBase<Model.QuestionOfDayAttempt, Guid>, ModelRepository.QuestionOfDayAttempts
    {
        public QuestionOfDayAttempts(string connectionString) : base(connectionString)
        {
            SetEntitySet(Context.QuestionOfDayAttempts);
        }

        public Model.QuestionOfDayAttempt[] GetAll()
        {
            return Context.QuestionOfDayAttempts.ToArray();
        }
    }
}
