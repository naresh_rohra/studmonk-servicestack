﻿using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using StudMonk.ServiceStack.ModelRepository.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.EFModelRepository
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Model.Subject> Subjects { get; set; }

        public DbSet<Model.Chapter> Chapters { get; set; }

        public DbSet<Model.Question> Questions { get; set; }

        public DbSet<Model.AnswerChoice> AnswerChoices { get; set; }

        public DbSet<Model.Tag> Tags { get; set; }

        public DbSet<Model.QuestionConceptTag> QuestionConceptTags { get; set; }

        public DbSet<Model.QuestionRelevanceTag> QuestionRelevanceTags { get; set; }

        public DbSet<Model.Test> Tests { get; set; }

        public DbSet<Model.TestChapter> TestChapters { get; set; }

        public DbSet<Model.TestQuestion> TestQuestions { get; set; }

        public DbSet<Model.TestAttemptSummary> TestAttemptsSummary { get; set; }

        public DbSet<Model.TestAttemptDetail> TestAttemptsDetail { get; set; }

        public DbSet<Model.User> Users { get; set; }

        public DbSet<Model.QuestionBookmark> QuestionBookmarks { get; set; }

        public DbSet<Model.QuestionOfDay> QuestionOfDays { get; set; }

        public DbSet<Model.QuestionOfDayAttempt> QuestionOfDayAttempts { get; set; }

        public DbSet<Model.PositionAnalyticReport> PositionAnalyticReports { get; set; }

        public DbSet<Model.TestSponsor> TestSponsors { get; set; }

        public DbSet<Model.TestInstruction> TestInstructions { get; set; }

        public DbSet<Model.Sponsor> Sponsors { get; set; }

        public DbSet<Model.UserTestRating> UserTestRatings { get; set; }

        public DbSet<Model.UserQuestionFlag> UserQuestionFlags { get; set; }

        public DbSet<Model.StudentEnrollmentDetail> StudentEnrollmentDetail { get; set; }

        public DbSet<Model.TestScoringSystem> TestScoringSystems { get; set; }

        public DbSet<Model.Tenant> Tenants{ get; set; }

        public DbSet<Model.UserAuthorizedDevice> UserAuthorizedDevices { get; set; }

        public DbSet<Model.TestFormat> TestFormats { get; set; }

        public DbSet<Model.TestFormatSubject> TestFormatSubjects { get; set; }

        public DbSet<Model.Stream> Streams { get; set; }

        public DbSet<Model.StreamSubject> StreamSubjects { get; set; }

        public DbSet<Model.AuditLog> AuditLogs { get; set; }

        public void UpdateQuestionsCountForChapter(Guid chapterId)
        {
            Database.ExecuteSqlCommand("UpdateQuestionsCountForChapter @ChapterId='" + chapterId + "'");
        }

        public void GenerateFullTest(ModelRepository.Types.GenerateFullTestRequest request)
        {
            var command = Database.GetDbConnection().CreateCommand();
            command.CommandText = BuildGenerateFullTestCommand(request, false);
            Database.OpenConnection();
            try
            {
                using (var reader = command.ExecuteReader())
                {
                }
            }
            finally
            {
                Database.CloseConnection();
            }
        }

        private string GetNullSafeParamValue(string value)
        {
            return value == null ? "null" : "'" + value + "'";
        }

        public Model.AuditLogReport[] GetRecentUserAction(Model.DTO.SearchAuditLogsRequest request)
        {
            IList<Model.AuditLogReport> result;

            var command = Database.GetDbConnection().CreateCommand();
            command.CommandText = String.Format("GetRecentUserAction " + 
                "@UserId = {0}, " +
                "@FullName = {1}, " +
                "@UserName = {2}, " +
                "@Category = {3}, " +
                "@Action = {4}, " +
                "@FirstRecordedFrom  = {5}, " +
                "@FirstRecordedTo = {6}, " +
                "@RecentRecordedFrom  = {7}, " +
                "@RecentRecordedTo = {8}, " +
                "@TenantId = {9}",
                GetNullSafeParamValue(request.UserId),
                GetNullSafeParamValue(request.FullName),
                GetNullSafeParamValue(request.UserName),
                GetNullSafeParamValue(request.Category),
                GetNullSafeParamValue(request.Action),
                request.FirstRecordedFrom == DateTime.MinValue ? "null" : "'" + request.FirstRecordedFrom.ToString() + "'",
                request.FirstRecordedTo == DateTime.MinValue ? "null" : "'" + request.FirstRecordedTo.ToString() + "'",
                request.RecentRecordedFrom == DateTime.MinValue ? "null" : "'" + request.RecentRecordedFrom.ToString() + "'",
                request.RecentRecordedTo == DateTime.MinValue ? "null" : "'" + request.RecentRecordedTo.ToString() + "'",
                GetNullSafeParamValue(request.TenantId));

            Database.OpenConnection();
            try
            {
                using (var reader = command.ExecuteReader())
                {
                    result = new TypedResultSet<Model.AuditLogReport>().GetTypedResultSetFromReader(reader);
                }
            }
            finally
            {
                Database.CloseConnection();
            }
            return result.ToArray();
        }

        public Model.AuditLogStat[] GetAuditLogStats(Model.DTO.SearchAuditLogsRequest request)
        {
            IList<Model.AuditLogStat> result;

            var command = Database.GetDbConnection().CreateCommand();
            command.CommandText = String.Format("GetAuditStats " +
                "@Category = {0}, " +
                "@Action = {1}, " +
                "@RecordedFrom = {2}, " +
                "@RecordedTo = {3}, " +
                "@TenantId = {4}, " +
                "@Frequency = {5}",
                GetNullSafeParamValue(request.Category),
                GetNullSafeParamValue(request.Action),
                request.RecordedFrom == DateTime.MinValue ? "null" : "'" + request.RecordedFrom.ToString() + "'",
                request.RecordedTo == DateTime.MinValue ? "null" : "'" + request.RecordedTo.ToString() + "'",
                GetNullSafeParamValue(request.TenantId),
                GetNullSafeParamValue(request.Frequency));

            Database.OpenConnection();
            try
            {
                using (var reader = command.ExecuteReader())
                {
                    result = new TypedResultSet<Model.AuditLogStat>().GetTypedResultSetFromReader(reader);
                }
            }
            finally
            {
                Database.CloseConnection();
            }
            return result.ToArray();
        }

        public Model.AuditLog[] GetAuditLogDump(Model.DTO.SearchAuditLogsRequest request)
        {
            IList<Model.AuditLog> result;

            var command = Database.GetDbConnection().CreateCommand();
            command.CommandText = String.Format("GetAuditLogDump " +
                "@UserId = {0}, " +
                "@FullName = {1}, " +
                "@UserName = {2}, " +
                "@Category = {3}, " +
                "@Action = {4}, " +
                "@RecordedFrom = {5}, " +
                "@RecordedTo = {6}, " +
                "@TenantId = {7}",
                GetNullSafeParamValue(request.UserId),
                GetNullSafeParamValue(request.FullName),
                GetNullSafeParamValue(request.UserName),
                GetNullSafeParamValue(request.Category),
                GetNullSafeParamValue(request.Action),
                request.RecordedFrom == DateTime.MinValue ? "null" : "'" + request.RecordedFrom.ToString() + "'",
                request.RecordedTo == DateTime.MinValue ? "null" : "'" + request.RecordedTo.ToString() + "'",
                GetNullSafeParamValue(request.TenantId));

            Database.OpenConnection();
            try
            {
                using (var reader = command.ExecuteReader())
                {
                    result = new TypedResultSet<Model.AuditLog>().GetTypedResultSetFromReader(reader);
                }
            }
            finally
            {
                Database.CloseConnection();
            }
            return result.ToArray();
        }

        public Model.DTO.UserRank[] GetFullTestTopScorers(Model.DTO.TopScorersRequest request)
        {
            IList<Model.DTO.UserRank> result;

            var command = Database.GetDbConnection().CreateCommand();
            command.CommandText = String.Format("GetFullTestTopScorers " +
                "@FromDate = {0}, " +
                "@ToDate = {1}, " +
                "@Stream = {2}, " +
                "@TenantId = {3}, " +
                "@MinAttempts = {4}, " +
                "@RankingCriteria = {5}, " +
                "@RankTill = {6}",
                request.RecordedFrom == DateTime.MinValue ? "null" : "'" + request.RecordedFrom.ToString() + "'",
                request.RecordedTo == DateTime.MinValue ? "null" : "'" + request.RecordedTo.ToString() + "'",
                GetNullSafeParamValue(request.Stream),
                GetNullSafeParamValue(request.TenantId),
                request.MinAttempts,
                GetNullSafeParamValue(request.RankingCriteria),
                request.RankTill);

            Database.OpenConnection();
            try
            {
                using (var reader = command.ExecuteReader())
                {
                    result = new TypedResultSet<Model.DTO.UserRank>().GetTypedResultSetFromReader(reader);
                }
            }
            finally
            {
                Database.CloseConnection();
            }
            return result.ToArray();
        }

        public Model.DTO.UserRank GetUserRanking(Model.DTO.UserRankingRequest request)
        {
            IList<Model.DTO.UserRank> result;

            var command = Database.GetDbConnection().CreateCommand();
            command.CommandText = String.Format("GetUserRanking " +
                "@UserId = {0}, " +
                "@StreamId = {1} ",
                GetNullSafeParamValue(request.UserId),
                GetNullSafeParamValue(request.StreamId.ToString()));

            Database.OpenConnection();
            try
            {
                using (var reader = command.ExecuteReader())
                {
                    result = new TypedResultSet<Model.DTO.UserRank>().GetTypedResultSetFromReader(reader);
                }
            }
            finally
            {
                Database.CloseConnection();
            }
            return result.FirstOrDefault();
        }

        public Model.DTO.UserTestScore[] GetFullTestScoresForUser(Model.DTO.UserScoresRequest request)
        {
            IList<Model.DTO.UserTestScore> result;

            var command = Database.GetDbConnection().CreateCommand();
            command.CommandText = String.Format("GetFullTestScoresForUser " +
                "@Email = '{0}'", request.Email);

            Database.OpenConnection();
            try
            {
                using (var reader = command.ExecuteReader())
                {
                    result = new TypedResultSet<Model.DTO.UserTestScore>().GetTypedResultSetFromReader(reader);
                }
            }
            finally
            {
                Database.CloseConnection();
            }
            return result.ToArray();
        }

        public ModelRepository.Types.FullTestPreviewResponse GetFullTestPreview(ModelRepository.Types.GenerateFullTestRequest request)
        {
            ModelRepository.Types.FullTestPreviewResponse response = new ModelRepository.Types.FullTestPreviewResponse();

            var command = Database.GetDbConnection().CreateCommand();
            command.CommandText = BuildGenerateFullTestCommand(request, true);

            Database.OpenConnection();
            try
            {
                using (var reader = command.ExecuteReader())
                {
                    response.generalStats = new TypedResultSet<GeneralStats>().GetTypedResultSetFromReader(reader).FirstOrDefault();

                    reader.NextResult();
                    response.subjectStats = new TypedResultSet<SubjectStat>().GetTypedResultSetFromReader(reader).ToArray();

                    reader.NextResult();
                    response.chapterStats = new TypedResultSet<ChapterStat>().GetTypedResultSetFromReader(reader).ToArray();

                    reader.NextResult();
                    response.questionIssues = new TypedResultSet<QuestionIssue>().GetTypedResultSetFromReader(reader).ToArray();
                }
            }
            finally
            {
                Database.CloseConnection();
            }
            return response;
        }

        private string BuildGenerateFullTestCommand(ModelRepository.Types.GenerateFullTestRequest request, bool previewOnly = true)
        {
            return String.Format("Exec dbo.GenerateFullTest " +
                "@MathQuestionNos = '{0}'," +
                "@PhysicsQuestionNos = '{1}'," +
                "@ChemistryQuestionNos = '{2}'," +
                "@BiologyQuestionNos = '{3}'," +
                "@SponsorId = '{4}'," +
                "@TestName = '{5}'," +
                "@StreamId = '{6}'," +
                "@DifficultyLevel = {7}," +
                "@DurationInMinutes = {8}," +
                "@InstructionTopText = '{9}'," +
                "@InstructionBottomText = '{10}'," +
                "@CreatedBy = '{11}'," +
                "@TenantId = {12}," +
                "@MaxScore = {13}," +
                "@Relevance = '{14}'," +
                "@ScoringSystemId = '{15}'," + 
                "@PreviewOnly = {16}", request.MathematicsQuestionNos, request.PhysicsQuestionNos, request.ChemistryQuestionNos, request.BiologyQuestionNos,
                request.SponsorId, request.TestName, request.StreamId, request.DifficultyLevel, request.DurationInMinutes, request.InstructionTopText,
                request.InstructionBottomText, request.CreatedBy, request.TenantId != null ? "'" + request.TenantId + "'" : "null", request.MaxScore, 
                request.Relevance, request.ScoringSystemId,
                previewOnly ? "1" : "0");
        }

        public Model.Question[] GetQuestionsList(QuestionListRequest request, int start, int end)
        {
            var questions = new List<Model.Question>();
            var dbConnection = this.Database.GetDbConnection();
            using (var command = dbConnection.CreateCommand())
            {
                dbConnection.Open();
                command.CommandText = String.Format("EXEC GetQuestionsList " +
                                            "@SubjectId='{0}', " +
                                            "@ChapterIds='{1}', " +
                                            "@DifficultyLevel = {2}, " +
                                            "@QualityStatus = {3}, " +
                                            "@IncludeInActive = {4}, " +
                                            "@SearchText = '{5}', " +
                                            "@ConceptText = '{6}', " +
                                            "@RelevanceText = '{7}', " +
                                            "@SortField = '{8}', " +
                                            "@SortDirection = '{9}', " +
                                            "@IncludeFullDetails = {10}, " +
                                            "@Start = {11}, " +
                                            "@End = {12}",
                                        request.SubjectId,
                                        request.ChapterIds != null ? String.Join(",", request.ChapterIds) : "",
                                        (byte)request.DifficultLevel,
                                        (byte)request.QualityStatus,
                                        request.IncludeInActive ? 1 : 0,
                                        request.SearchText,
                                        request.ConceptText,
                                        request.RelevanceText,
                                        request.SortBy,
                                        request.SortOrder.ToString(),
                                        request.IncludeFullDetails ? 1 : 0,
                                        start,
                                        end);
                command.CommandType = CommandType.Text;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var question = new Model.Question();
                        for(var i = 0; i < reader.FieldCount; i++)
                        {
                            string fieldName = reader.GetName(i);
                            var fieldProperty = question.GetType().GetProperty(fieldName);
                            if(fieldName == "QuestionId")
                            {
                                question.Id = reader.GetGuid(i);
                            }
                            else if (fieldProperty!= null && !reader.IsDBNull(i))
                            { 
                                fieldProperty.SetValue(question, reader.GetValue(i));
                            }
                        }
                        questions.Add(question);
                    }
                }
            }
            return questions.ToArray();
        }

        public Model.DTO.Concept[] GetConceptsInfoForSubject(Guid subjectId)
        {
            var conceptInfoList = new List<Model.DTO.Concept>();
            var dbConnection = this.Database.GetDbConnection();
            using (var command = dbConnection.CreateCommand())
            {
                dbConnection.Open();
                command.CommandText = String.Format("EXEC GetConceptsInfoForSubject @SubjectId='{0}'", subjectId);
                command.CommandType = CommandType.Text;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var conceptInfo = new Model.DTO.Concept();
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            string fieldName = reader.GetName(i);
                            var fieldProperty = conceptInfo.GetType().GetProperty(fieldName);
                            if (fieldProperty != null && !reader.IsDBNull(i))
                            {
                                fieldProperty.SetValue(conceptInfo, reader.GetValue(i));
                            }
                        }
                        conceptInfoList.Add(conceptInfo);
                    }
                }
            }
            return conceptInfoList.ToArray();
        }

        public Model.QuestionOfDayAnalytics GetUserStreak(string userId, string questionOfDayId)
        {
            Model.QuestionOfDayAnalytics questionOfDayAnalytics = new Model.QuestionOfDayAnalytics { CurrentStreak = 0, RecordStreak = 0, TotalAttempt = 0, TotalFailure = 0, TotalSuccess = 0 };

            var connection = (SqlConnection)this.Database.GetDbConnection();
            var command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "GetStreak";
            command.Parameters.AddWithValue("@userid", userId);
            command.Parameters.AddWithValue("@QuestionOfDayId", questionOfDayId);

            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            SqlDataReader dr = command.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    questionOfDayAnalytics.CurrentStreak = Convert.ToInt16(dr["CurrentStreak"]);
                    questionOfDayAnalytics.RecordStreak = Convert.ToInt16(dr["RecordStreak"]);
                    questionOfDayAnalytics.TotalAttempt = Convert.ToInt16(dr["TotalAttempt"]);
                    questionOfDayAnalytics.TotalFailure = Convert.ToInt16(dr["TotalFailure"]);
                    questionOfDayAnalytics.TotalSuccess = Convert.ToInt16(dr["TotalSuccess"]);
                }
            }
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            return questionOfDayAnalytics;
        }

        public Model.PositionAnalyticReport GetPositionAnalytic(string userId, string subjectId)
        {
            Model.PositionAnalyticReport result = new Model.PositionAnalyticReport { UserId = userId, SubjectId = new Guid(subjectId) };

            var connection = (SqlConnection)this.Database.GetDbConnection();
            var command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "PositionAnalytic";
            command.Parameters.AddWithValue("@userid", userId);
            command.Parameters.AddWithValue("@SubjectId", subjectId);

            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            SqlDataReader dr = command.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    result.UserHighScorePercentage = Convert.ToDouble(dr["UserHighestInSubject"]);
                    result.DifficultyLevel = Convert.ToInt16(dr["DifficultyLevel"]);
                    result.AppToppperPercentage = Convert.ToDouble(dr["AppHighestInSubject"]);
                    result.AppAveragePercentage = Convert.ToDouble(dr["AppAvgInSubject"]);
                    result.LastYearToppperPercentage = Convert.ToDouble(dr["BoardTopper"]);
                }
            }
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            return result;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.TestChapter>()
                .HasKey(tc => new { tc.Id, tc.ChapterId });

            modelBuilder.Entity<Model.TestQuestion>()
                .HasKey(tq => new { tq.Id, tq.QuestionId });

            modelBuilder.Entity<Model.TestAttemptDetail>()
                .HasKey(tq => new { tq.Id, tq.QuestionId });

            modelBuilder.Entity<Model.QuestionBookmark>()
                .HasKey(tq => new { tq.Id, tq.UserId });

            modelBuilder.Entity<Model.TestAttemptSummary>()
                .HasMany(tas => tas.AttemptDetails)
                .WithOne()
                .HasForeignKey(tad => tad.Id);

            modelBuilder.Entity<Model.QuestionOfDayAttempt>()
               .HasKey(tq => new { tq.QuestionOfDayId, tq.UserId });

            modelBuilder.Entity<Model.PositionAnalyticReport>()
             .HasKey(tq => new { tq.UserId, tq.SubjectId });

            modelBuilder.Entity<Model.TestSponsor>()
            .HasKey(ts => new { ts.TestId, ts.SponsorId });

            modelBuilder.Entity<Model.TestInstruction>()
           .HasKey(ts => new { ts.TestId});

            modelBuilder.Entity<Model.QuestionConceptTag>()
            .HasKey(qt => new { qt.Id, qt.TagId });

            modelBuilder.Entity<Model.QuestionRelevanceTag>()
            .HasKey(qt => new { qt.Id, qt.TagId });

            modelBuilder.Entity<Model.UserQuestionFlag>()
            .HasKey(uqf => new { uqf.Id, uqf.UserId, uqf.Flag });

            modelBuilder.Entity<Model.UserTestRating>()
            .HasKey(utr => new { utr.Id, utr.UserId });

            modelBuilder.Entity<Model.TestScoringSystem>()
           .HasKey(ts => new { ts.Id, ts.SubjectId });

            modelBuilder.Entity<Model.UserAuthorizedDevice>()
           .HasKey(ts => new { ts.Id, ts.UserId});

            modelBuilder.Entity<Model.TestFormatSubject>()
           .HasKey(tts => new { tts.Id, tts.SubjectId });

            modelBuilder.Entity<Model.StreamSubject>()
           .HasKey(ss => new { ss.Id, ss.SubjectId });

            base.OnModelCreating(modelBuilder);
        }
    }

    class TypedResultSet<T>
        where T : class, new()
    {
        public IList<T> GetTypedResultSetFromReader(DbDataReader reader)
        {
            IList<T> typedSet = new List<T>();

            while (reader.Read())
            {
                var typedRecord = new T();
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    string fieldName = reader.GetName(i);
                    if (!reader.IsDBNull(i))
                    {
                        var field = typedRecord.GetType().GetField(fieldName);
                        if (field != null)
                        {
                            field.SetValue(typedRecord, reader.GetValue(i));
                        }
                        else
                        {
                            var property = typedRecord.GetType().GetProperty(fieldName);
                            if(property != null)
                            {
                                property.SetValue(typedRecord, reader.GetValue(i));
                            }
                        }
                    }
                }
                typedSet.Add(typedRecord);
            }
            return typedSet;
        }
    }
}
