﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface QuestionBookmarks : RepositoryBaseOperations<Model.QuestionBookmark, Guid>
    {
        Model.Question[] GetForUser(string userId, int pageNo, int pageSize);

        Model.Question[] GetForSubject(Guid subjectId);

        Model.Question[] GetForChapter(Guid chapterId);
    }
}
