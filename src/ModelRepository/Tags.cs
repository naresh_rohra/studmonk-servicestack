﻿using System;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface Tags : RepositoryBaseOperations<Model.Tag, Int64>
    {
        Model.Tag[] CreateIfNotExists(string[] names, string userId);

        Model.Tag[] GetByNames(string[] names);
    }
}
