﻿namespace StudMonk.ServiceStack.ModelRepository
{
    public interface InstanceActivator
    {
        T GetInstance<T>() where T : ModelRepository.Repository;
    }
}
