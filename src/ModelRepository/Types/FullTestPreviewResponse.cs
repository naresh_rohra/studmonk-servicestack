﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.ModelRepository.Types
{
    public class FullTestPreviewResponse
    {
        public GeneralStats generalStats { get; set; }

        public SubjectStat[] subjectStats { get; set; }

        public ChapterStat[] chapterStats { get; set; }

        public QuestionIssue[] questionIssues { get; set; }
    }

    public class GeneralStats
    {
        public int Duration;
        public int TotalQuestions;
    }

    public class SubjectStat
    {
        public Guid SubjectId;
        public string Subject;
        public int NoOfQuestions;
    }

    public class ChapterStat
    {
        public Guid ChapterId;
        public string Chapter;
        public int NoOfQuestions;
    }

    public class QuestionIssue
    {
        public Int64 QuestionNo;

        public string Subject;

        public string Reason;
    }
}
