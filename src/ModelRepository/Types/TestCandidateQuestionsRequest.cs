﻿using System;

namespace StudMonk.ServiceStack.ModelRepository.Types
{
    public class TestCandidateQuestionsRequest
    {
        public Guid SubjectId { get; set; }

        public Guid[] ChapterIds { get; set; }

        public Model.DifficultyLevel DifficultLevel { get; set; }

        public string Relevances { get; set; }

        public string Concepts { get; set; }
    }
}
