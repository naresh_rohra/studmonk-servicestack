﻿using System;
using StudMonk.ServiceStack.Model;

namespace StudMonk.ServiceStack.ModelRepository.Types
{
    public class QuestionListRequest
    {
        private Model.QualityStatus _qualityStatus = Model.QualityStatus.Any;
        private string _sortBy = "QuestionNo", _searchText = "", _relevanceText = "", _conceptText = "";
        private Types.SortOrder _sortOrder = Types.SortOrder.Ascending;
        private int _pageNumber = 0;
        private bool _includeInActive = false, _includeFullDetails = true;
        private Model.DifficultyLevel _difficultLevel = DifficultyLevel.Random;

        public Guid SubjectId { get; set; }

        public Guid[] ChapterIds { get; set; }

        public Model.DifficultyLevel DifficultLevel { get { return _difficultLevel; } set { _difficultLevel = value; } }

        public int PageNumber { get { return _pageNumber; } set { _pageNumber = value; } }

        public bool IncludeInActive { get { return _includeInActive; } set { _includeInActive = value; } }

        public Model.QualityStatus QualityStatus { get { return _qualityStatus; } set { _qualityStatus = value; } }

        public string SortBy { get { return _sortBy; } set { _sortBy = value; } }

        public Types.SortOrder SortOrder { get { return _sortOrder; } set { _sortOrder = value; } }

        public string SearchText { get { return _searchText; } set { _searchText = value; } }

        public string RelevanceText { get { return _relevanceText; } set { _relevanceText = value; } }

        public string ConceptText { get { return _conceptText; } set { _conceptText = value; } }

        public bool IncludeFullDetails { get { return _includeFullDetails; } set { _includeFullDetails = value; } }
    }
}
