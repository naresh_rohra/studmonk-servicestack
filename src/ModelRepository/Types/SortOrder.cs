﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.ModelRepository.Types
{
    public enum SortOrder
    {
        Descending = 0,
        Ascending = 1
    }
}
