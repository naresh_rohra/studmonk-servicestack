﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.ModelRepository.Types
{
    public class GenerateFullTestRequest
    {
        public string PhysicsQuestionNos;

        public string ChemistryQuestionNos;

        public string MathematicsQuestionNos;

        public string BiologyQuestionNos;

        public Guid SponsorId;

        public string TestName;

        public Guid StreamId;

        public byte DifficultyLevel;

        public int DurationInMinutes;

        public short MaxScore;

        public string InstructionTopText;

        public string InstructionBottomText;

        public string CreatedBy;

	    public string TenantId;

        public bool SendNotification;

        public string Relevance;
        
        public Guid ScoringSystemId;
    }
}
