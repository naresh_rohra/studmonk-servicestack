﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface PositionAnalyticReports : RepositoryBaseOperations<Model.PositionAnalyticReport, Guid>
    {
        Model.PositionAnalyticReport GetPositionAnalyticReport(string userId, string subjectId);
    }
}
