﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface QuestionOfDayAttempts : RepositoryBaseOperations<Model.QuestionOfDayAttempt, Guid>
    {
        Model.QuestionOfDayAttempt[] GetAll();
    }
}
