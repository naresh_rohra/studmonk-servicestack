﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface RepositoryBaseOperations<TModel, TKey> : Repository
        where TModel : Model.ModelBase<TKey>
    {
        TModel Get(TKey id);

        TModel[] Get(TKey[] ids);
            
        TModel SaveNew(TModel value);

        TModel[] SaveNew(TModel[] values);

        TModel SaveExisting(TModel value);

        void Delete(TKey id);

        void Delete(TKey[] ids);

        void Delete(TModel value);

        void Delete(TModel[] values);
    }
}
