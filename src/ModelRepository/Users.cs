﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface Users : RepositoryBaseOperations<Model.User, Guid>
    {
        Model.User GetByDeviceId(string deviceId);

        Model.UserAuthorizedDevice[] GetUserAuthorizedDevices(string userId, bool includeInActive = false);

        void UpdateUserAuthorizedDevices(string userId, string[] deviceIds, string modifiedB);

        Model.Tenant GetTenant(string tenantId);

        Model.UserAuthorizedDevice SaveUserAuthorizedDevice(Model.UserAuthorizedDevice authorizedDevice);

        Model.Tenant[] GetAllTenants();

        Model.DTO.UserRank GetUserRanking(Model.DTO.UserRankingRequest request);
    }
}
