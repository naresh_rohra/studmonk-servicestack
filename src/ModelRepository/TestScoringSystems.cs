﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface TestScoringSystems : RepositoryBaseOperations<Model.TestScoringSystem, Guid>
    {
        Model.TestScoringSystem[] GetAll();

        Model.DTO.ScoringSystem[] GetByStream(Guid streamId);

        Model.DTO.ScoringSystem[] GetById(Guid id);

    }
}
