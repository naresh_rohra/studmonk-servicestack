﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface QuestionOfDays : RepositoryBaseOperations<Model.QuestionOfDay, Guid>
    {
        Model.QuestionOfDay[] GetAll();

        Model.QuestionOfDay GetQuestionOfTheDay(Guid streamId);

        Model.Subject GetSubjectByChapterId(string chapterId);

        Model.QuestionOfDayAttempt GetUserAttempt(string userId,string questionofdayid);

        Model.QuestionOfDay GetQuestionOfDayById(string questionofdayid);

        Model.QuestionOfDayAnalytics GetQuestionOfDayAnalytics(string userid, string questionofdayid);

        Model.Question GetQuestionByQuestionOfDay(string questionofdayid);
    }

}
