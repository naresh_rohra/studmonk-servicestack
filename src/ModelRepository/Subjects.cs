﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface Subjects : RepositoryBaseOperations<Model.Subject, Guid>
    {
        Model.Subject[] GetAll();

        Model.Subject[] GetForStream(Guid streamId);

        Model.DTO.Concept[] GetAllConcepts(Guid subjectId);
    }
}
