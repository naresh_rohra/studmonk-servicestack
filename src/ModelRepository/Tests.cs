﻿using StudMonk.ServiceStack.Model.DTO;
using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface Tests : RepositoryBaseOperations<Model.Test, Guid>
    {
        Model.Test[] GetAllFullTest(string lastSyncDate,string streamId, string tenantId, string relevance);

        Model.Test[] GetTestOfUser(string userId, int pageNo, int pageSize);

        Model.Test GetTestWithUserAttempt(string userId, Guid attemptId);

        Model.Sponsor[] GetSponsorsForTest(Guid testId);

        Model.UserTestRating SaveUserTestRating(Model.UserTestRating rating);

        Model.TestAttemptSummary[] GetTestAttemptSummary(Guid testId,string userId);

        Model.DTO.FullTestInstruction GetTestInstruction(Guid testId);

        Model.TestFormat[] GetTestFormatsForStream(Guid streamId);

        ModelRepository.Types.FullTestPreviewResponse GetFullTestPreview(ModelRepository.Types.GenerateFullTestRequest request);

        void GenerateFullTest(ModelRepository.Types.GenerateFullTestRequest request);

        Model.Sponsor[] GetAllSponsors();

        void SaveFullTestDetails(Model.DTO.FullTest test);
    }
}
