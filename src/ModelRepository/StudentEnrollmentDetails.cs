﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface StudentEnrollmentDetails : RepositoryBaseOperations<Model.StudentEnrollmentDetail, Guid>
    {
        Model.StudentEnrollmentDetail GetForStudent(string userId);
    }
}
