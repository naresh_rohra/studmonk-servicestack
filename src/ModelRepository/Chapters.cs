﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface Chapters : RepositoryBaseOperations<Model.Chapter, Guid>
    {
        Model.Chapter[] GetAllForSubject(Guid subjectId, bool includeInActive = false);

        Model.Chapter[] GetForChapterIds(Guid[] chapterIds, bool includeInActive = true);

        Model.TestChapter[] GetAllOfTest(Guid testId);
    }
}
