﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface TestAttemptsSummary : RepositoryBaseOperations<Model.TestAttemptSummary, Guid>
    {

    }
}
