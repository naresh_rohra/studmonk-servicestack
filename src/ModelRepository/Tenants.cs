﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface Tenants : RepositoryBaseOperations<Model.Tenant, string>
    {
    }
}
