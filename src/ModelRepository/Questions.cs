﻿using System;

namespace StudMonk.ServiceStack.ModelRepository
{
    public interface Questions : RepositoryBaseOperations<Model.Question, Guid>
    {
        Model.DTO.QuestionsList GetForChapter(Types.QuestionListRequest request);

        Model.DTO.QuestionsList GetForSubjectChaptersWithDifficultyLevel(ModelRepository.Types.QuestionListRequest request);

        Model.Question[] GetCandidateQuestionsForTest(ModelRepository.Types.TestCandidateQuestionsRequest request);

        Model.Question[] GetQuestionsForTest(Guid testId);

        Model.DTO.SubjectWiseTestSection[] GetQuestionsForFullTest(Guid testId, int perQuestionDuration, string relevance);

        Model.DTO.SubjectWiseTestSection[] GetQuestionsForFullTestInfo(Guid testId, int perQuestionDuration, string relevance, bool includeDetails = true);

        Model.DTO.QuestionsList SearchForChapter(Guid chapterId, string searchText, Guid exclusionQuestionId);

        void MarkQuestionsAsNeedsCorrection(Guid[] questionIds);

        void MarkQuestionsAsReviewed(Guid[] questionIds);

        void MarkQuestionsAsNotReviewed(Guid[] questionIds);

        Model.Tag[] SearchConceptTags(string searchText);

        Model.Tag[] SearchRelevanceTags(string searchText);

        Model.Tag[] GetConceptTagsForQuestion(Guid questionId);

        Model.Tag[] GetRelevanceTagsForQuestion(Guid questionId);

        Model.UserQuestionFlag SaveUserQuestionFlag(Model.UserQuestionFlag questionFlag);
    }
}
