﻿using System;
using System.Collections.Generic;
using StudMonk.ServiceStack.DomainOperations.Types;
using System.Linq;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class QuestionProcess : ProcessBaseWithStorageOperations<ModelRepository.Questions, Model.Question, Guid>, DomainOperations.Question
    {
        ModelRepository.Tags _tagsRepository;

        public QuestionProcess(ModelRepository.Questions repository) : base(repository) { }

        public Model.DTO.QuestionsList GetForChapter(ModelRepository.Types.QuestionListRequest request)
        {
            return _repository.GetForChapter(request);
        }

        public Model.DTO.QuestionsList GetForSubjectChaptersWithDifficultyLevel(ModelRepository.Types.QuestionListRequest request)
        {
            return _repository.GetForSubjectChaptersWithDifficultyLevel(request);
        }

        public Model.DTO.QuestionsList SearchForChapter(Guid chapterId, string searchText, Guid exclusionQuestionId)
        {
            return _repository.SearchForChapter(chapterId, searchText, exclusionQuestionId);
        }

        public Model.Question[] GetCandidateQuestionsForTest(ModelRepository.Types.TestCandidateQuestionsRequest request)
        {
            return _repository.GetCandidateQuestionsForTest(request);
        }
        public Model.Question[] GetQuestionsForTest(Guid testId)
        {
            return _repository.GetQuestionsForTest(testId);
        }

        public Model.DTO.SubjectWiseTestSection[] GetQuestionsForFullTest(Guid testId, int perQuestionDuration, string relevance, bool includeDetails = true)
        {
            if (includeDetails == true)
            {
                return _repository.GetQuestionsForFullTest(testId, perQuestionDuration, relevance);
            }
            else
            {
                return _repository.GetQuestionsForFullTestInfo(testId, perQuestionDuration, relevance, includeDetails);
            }

        }

        public bool ImportQuestions(QuestionImportRequest questionImportReq)
        {
            var newQuestions = new List<Model.Question>();
            foreach (var importQuestionDetail in questionImportReq.ImportQuestionDetails)
            {
                Model.DifficultyLevel difficultyLevel;
                Enum.TryParse<Model.DifficultyLevel>(importQuestionDetail.DifficultyLevel, out difficultyLevel);
                if (difficultyLevel == Model.DifficultyLevel.Random)
                {
                    difficultyLevel = Model.DifficultyLevel.Medium;
                }
                var newQuestionId = Guid.NewGuid();
                var question = new Model.Question
                {
                    Id = newQuestionId,
                    Text = importQuestionDetail.Text,
                    Explanation = importQuestionDetail.Solution,
                    Hint = importQuestionDetail.Hint,
                    DifficultyLevel = difficultyLevel,
                    TextType = Model.TextType.Text,
                    QuestionType = Model.QuestionType.MultiChoiceSingleSelect,
                    QualityStatus = Model.QualityStatus.NotReviewed,
                    Active = true,
                    ChapterId = questionImportReq.ChapterId,
                    CreatedOn = DateTime.UtcNow,
                    CreatedBy = CurrentUserIdentity.UserId,
                    AnswerChoices = new List<Model.AnswerChoice>(),
                    PreviousAppearances = importQuestionDetail.PreviousAppearances,
                    Sources = importQuestionDetail.Sources,
                    TaggedConcepts = !String.IsNullOrEmpty(importQuestionDetail.Concepts) ? importQuestionDetail.Concepts.Split(',') : null,
                    TaggedRelevances = !String.IsNullOrEmpty(importQuestionDetail.Relevances) ? importQuestionDetail.Relevances.Split(',') : null
                };
                foreach (var choice in new string[] { importQuestionDetail.Choice1, importQuestionDetail.Choice2, importQuestionDetail.Choice3, importQuestionDetail.Choice4 })
                {
                    question.AnswerChoices.Add(new Model.AnswerChoice
                    {
                        Id = Guid.NewGuid(),
                        Text = choice,
                        QuestionId = newQuestionId,
                        IsCorrect = String.Equals(choice.Trim(), importQuestionDetail.CorrectAnswer.Trim(), StringComparison.OrdinalIgnoreCase),
                        CreatedOn = DateTime.UtcNow
                    });
                }
                SetQuestionConceptTags(question);
                SetQuestionRelevanceTags(question);
                newQuestions.Add(question);
            }
            _repository.SaveNew(newQuestions.ToArray());
            return true;
        }

        public void MarkQuestionsAsNeedsCorrection(Guid[] questionIds)
        {
            _repository.MarkQuestionsAsNeedsCorrection(questionIds);
        }

        public void MarkQuestionsAsReviewed(Guid[] questionIds)
        {
            _repository.MarkQuestionsAsReviewed(questionIds);
        }

        public void MarkQuestionsAsNotReviewed(Guid[] questionIds)
        {
            _repository.MarkQuestionsAsNotReviewed(questionIds);
        }

        public override Model.Question Save(Model.Question question)
        {
            question.QualityStatus = Model.QualityStatus.NotReviewed;
            return base.Save(question);
        }

        public Model.Question SaveAndApprove(Model.Question question)
        {
            question.QualityStatus = Model.QualityStatus.Reviewed;
            return base.Save(question);
        }

        protected override void BeforeSaveNew(Model.Question question)
        {
            question.LastModifiedBy = CurrentUserIdentity.UserId;
            SetQuestionConceptTags(question);
            SetQuestionRelevanceTags(question);
        }

        protected override void BeforeSaveExisting(Model.Question question)
        {
            question.LastModifiedBy = CurrentUserIdentity.UserId;
            SetQuestionConceptTags(question);
            SetQuestionRelevanceTags(question);
        }

        private void SetQuestionConceptTags(Model.Question question)
        {
            if (question.TaggedConcepts != null)
            {
                var existingTags = _repository.GetConceptTagsForQuestion(question.Id);
                var conceptTags = GetQuestionTags(existingTags, question.TaggedConcepts, question.Id);
                question.ConceptTags = new List<Model.QuestionConceptTag>();
                foreach (var tag in conceptTags)
                {
                    question.ConceptTags.Add(new Model.QuestionConceptTag(tag));
                }
            }
        }

        private Model.QuestionTag[] GetQuestionTags(Model.Tag[] existingTags, string[] newTagNames, Guid questionId)
        {
            _tagsRepository = RepositoryInstanceActivator.GetInstance<ModelRepository.Tags>();

            var newTags = _tagsRepository.CreateIfNotExists(newTagNames, this.CurrentUserIdentity.UserId);

            List<Model.Tag> allTags = new List<Model.Tag>();
            allTags = existingTags.Concat(newTags).ToList();

            List<Model.QuestionTag> trackedTags = new List<Model.QuestionTag>();
            bool isInNewTag = false, isInExistingTags = false;
            foreach (var tag in allTags)
            {
                isInNewTag = newTags.Any(t => t.Id == tag.Id);
                isInExistingTags = existingTags.Any(t => t.Id == tag.Id);
                if (isInNewTag && !isInExistingTags)
                {
                    //New added
                    trackedTags.Add(new Model.QuestionTag()
                    {
                        TagId = tag.Id,
                        Id = questionId,
                        CreatedBy = this.CurrentUserIdentity.UserId,
                        CreatedOn = DateTime.UtcNow,
                        IsActive = true,
                        State = Model.ModificationState.Added
                    });
                }
                else if (!isInNewTag && isInExistingTags)
                {
                    //removed
                    trackedTags.Add(new Model.QuestionTag()
                    {
                        TagId = tag.Id,
                        Id = questionId,
                        LastModifiedBy = this.CurrentUserIdentity.UserId,
                        LastModifiedOn = DateTime.UtcNow,
                        IsActive = false,
                        State = Model.ModificationState.Deleted
                    });
                }
                //else is unchanged
            }
            return trackedTags.ToArray();
        }

        private void SetQuestionRelevanceTags(Model.Question question)
        {
            if (question.TaggedRelevances != null)
            {
                var existingTags = _repository.GetRelevanceTagsForQuestion(question.Id);
                var relevanceTags = GetQuestionTags(existingTags, question.TaggedRelevances, question.Id);
                question.RelevanceTags = new List<Model.QuestionRelevanceTag>();
                foreach (var tag in relevanceTags)
                {
                    question.RelevanceTags.Add(new Model.QuestionRelevanceTag(tag));
                }
            }
        }

        public string[] GetAllRelevanceTags()
        {
            return _repository.SearchRelevanceTags(String.Empty).Select(t => t.Name).ToArray();
        }

        public string[] GetAllConceptTags()
        {
            return _repository.SearchConceptTags(String.Empty).Select(t => t.Name).ToArray();
        }

        public Model.UserQuestionFlag SaveUserQuestionFlag(Model.UserQuestionFlag questionFlag)
        {
            return _repository.SaveUserQuestionFlag(questionFlag);
        }
    }
}
