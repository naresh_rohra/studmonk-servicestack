﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class QuestionOfDayAttemptProcess : ProcessBaseWithStorageOperations<ModelRepository.QuestionOfDayAttempts, Model.QuestionOfDayAttempt, Guid>, DomainOperations.QuestionOfDayAttempt
    {
        public QuestionOfDayAttemptProcess(ModelRepository.QuestionOfDayAttempts repository) : base(repository) { }

        public Model.QuestionOfDayAttempt[] GetAll()
        {
            return _repository.GetAll();
        }
    }
}
