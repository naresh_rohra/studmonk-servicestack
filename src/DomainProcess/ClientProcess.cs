﻿namespace StudMonk.ServiceStack.DomainProcess
{
    public class ClientProcess : ProcessBase, DomainOperations.Client
    {
        public Model.Settings.Client GetSettings()
        {
            return new Model.Settings.Client();
        }
    }
}
