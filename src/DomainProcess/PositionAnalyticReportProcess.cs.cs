﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class PositionAnalyticReportProcess : ProcessBaseWithStorageOperations<ModelRepository.PositionAnalyticReports, Model.PositionAnalyticReport, Guid>, DomainOperations.PositionAnalyticReport
    {
        public PositionAnalyticReportProcess(ModelRepository.PositionAnalyticReports repository) : base(repository)
        {

        }

        public Model.PositionAnalyticReport GetPositionAnalyticReport(string userId, string subjectId)
        {
            Model.PositionAnalyticReport PositionAnalyticReport = new Model.PositionAnalyticReport();
            PositionAnalyticReport  = _repository.GetPositionAnalyticReport(userId, subjectId);
            return PositionAnalyticReport;
        }
    }
}
