﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class AccountProcess : ProcessBase, DomainOperations.Account
    {
        public Model.StudentEnrollmentDetail EnrollStudent(string userId, Guid streamId)
        {
            var enrollmentRepository = RepositoryInstanceActivator.GetInstance<ModelRepository.StudentEnrollmentDetails>();
            return enrollmentRepository.SaveNew(new Model.StudentEnrollmentDetail()
            {
                UserId = userId,
                Id = streamId,
                Active = true,
                CreatedBy = this.CurrentUserIdentity.UserId,
                CreatedOn = DateTime.UtcNow
            });
        }
    }
}
