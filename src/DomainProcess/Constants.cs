﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public static class Constants
    {
        public static Guid PublicInstitutionId = Guid.Parse("a7276ffc-6a8f-4581-b5e8-4853604a103e");
        public static Guid CetBoardId = Guid.Parse("0ab8f5c2-987a-4f75-bca4-a72aa36ad083");
        public static Guid EngineeringStreamId = Guid.Parse("94085a60-62a5-4096-8110-b0feeb225cf5");
        public static Guid MedicalStreamId = Guid.Parse("ec3ebe58-1c67-4759-aac0-b9ad43010cbe");
        public static int PersonalisedTestDuration = 60;

        public static Guid MathematicSubjectId = Guid.Parse("46D3900A-D1E4-4537-8AEE-137A29120069");
        public static Guid ChemistrySubjectId = Guid.Parse("A1CCFDD9-3B4E-4453-BE1B-6B4FC55D8CAF");
        public static Guid BiologySubjectId = Guid.Parse("5EB27717-C185-41B1-9679-8AC1E6527328");
        public static Guid PhysicsSubjectId = Guid.Parse("F04D9B0E-9CE0-4E90-BDDD-E42551BA7EE9");
    }
}
