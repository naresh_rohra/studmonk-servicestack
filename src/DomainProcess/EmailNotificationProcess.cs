﻿namespace StudMonk.ServiceStack.DomainProcess
{
    public class EmailNotificationProcess : ProcessBase, DomainOperations.EmailNotification
    {
        public EmailTemplate PasswordResetEmailTemplate;
        private EmailNotification emailNotification;

        public EmailNotificationProcess(string server, int port, string fromEmailAddr, string username, string password)
        {
            emailNotification = new EmailNotification
            {
                Server = server,
                Port = port,
                FromEmailAddr = fromEmailAddr,
                Username = username,
                Password = password
            };
        }

        public void SendPasswordResetToken(string fullname, string token, string toEmailAddr, string templatedBody, string templatedSubject)
        {
            PasswordResetEmailTemplate = new EmailTemplate { Subject = templatedSubject , Body = templatedBody };

            var mailBody = GetProcessedTemplate(PasswordResetEmailTemplate.Body, new TemplateParameter[] {
                new TemplateParameter("fullname", fullname),
                new TemplateParameter("token", token)
            });

            emailNotification.SendEmail(toEmailAddr,"QA :"+ PasswordResetEmailTemplate.Subject, mailBody);
        }

        private string GetProcessedTemplate(string templateBody, TemplateParameter[] parameters)
        {
            string processed = templateBody;
            foreach(var parameter in parameters)
            {
                processed = processed.Replace("{" + parameter.Key + "}", parameter.Value);
            }
            return processed;
        }
    }

    public class TemplateParameter
    {
        public TemplateParameter(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; set; }

        public string Value { get; set; }
    }

    public class EmailTemplate
    {
        public string Body { get; set; }

        public string Subject { get; set; }
    }
}
