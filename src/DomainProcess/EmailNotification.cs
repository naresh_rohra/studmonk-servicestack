﻿using System.Net.Mail;
using System.Net;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class EmailNotification
    {
        public string Server { get; set; }

        public int Port { get; set; }

        public string FromEmailAddr { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public void SendEmail(string toEmailAddr, string subject, string body)
        {
            SmtpClient smtpClient = new SmtpClient(Server, Port)
            {
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(Username, Password)
            };

            MailMessage message = new MailMessage(FromEmailAddr, toEmailAddr, subject, body) { IsBodyHtml = true };

            smtpClient.Send(message);
            smtpClient.Dispose();
        }
    }
}
