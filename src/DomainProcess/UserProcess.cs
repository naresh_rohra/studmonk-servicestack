﻿using System;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class UserProcess : ProcessBaseWithStorageOperations<ModelRepository.Users, Model.User, Guid>, DomainOperations.User
    {
        ModelRepository.QuestionBookmarks _questionBookmarksRepository;
        private ModelRepository.QuestionBookmarks QuestionBookmarksRepository
        {
            get
            {
                if (_questionBookmarksRepository == null)
                    _questionBookmarksRepository = RepositoryInstanceActivator.GetInstance<ModelRepository.QuestionBookmarks>();
                return _questionBookmarksRepository;
            }
        }

        public UserProcess(ModelRepository.Users repository) : base(repository) { }

        public Model.User GetByDeviceId(string deviceId)
        {
            return _repository.GetByDeviceId(deviceId);
        }

        public void Register(Model.User user)
        {
            _repository.SaveNew(user);
        }

        public void AddBookmarks(Model.QuestionBookmark[] questionBookmarks)
        {
            QuestionBookmarksRepository.SaveNew(questionBookmarks);
        }             

        public Model.DTO.UserRank GetUserRanking(Model.DTO.UserRankingRequest request)
        {
            return _repository.GetUserRanking(request);
        }
    }
}
