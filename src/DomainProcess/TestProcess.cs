﻿using StudMonk.ServiceStack.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class TestProcess : ProcessBaseWithStorageOperations<ModelRepository.Tests, Model.Test, Guid>, DomainOperations.Test
    {
        public TestProcess(ModelRepository.Tests repository) : base(repository)
        {

        }

        private DomainOperations.Tenant _tenantProcess = null;
        private DomainOperations.Tenant TenantProcess
        {
            get
            {
                if (_tenantProcess == null)
                    _tenantProcess = InstanceActivator.GetInstance<DomainOperations.Tenant, ModelRepository.Tenants>(RepositoryInstanceActivator.GetInstance<ModelRepository.Tenants>(), this.CurrentUserIdentity);
                return _tenantProcess;
            }
        }

        private DomainOperations.Question _questionProcess = null;
        private DomainOperations.Question QuestionsProcess
        {
            get
            {
                if (_questionProcess == null)
                    _questionProcess = InstanceActivator.GetInstance<DomainOperations.Question, ModelRepository.Questions>(RepositoryInstanceActivator.GetInstance<ModelRepository.Questions>(), this.CurrentUserIdentity);
                return _questionProcess;
            }
        }
        private DomainOperations.Chapter _chapterProcess = null;
        private DomainOperations.Chapter ChaptersProcess
        {
            get
            {
                if (_chapterProcess == null)
                    _chapterProcess = InstanceActivator.GetInstance<DomainOperations.Chapter, ModelRepository.Chapters>(RepositoryInstanceActivator.GetInstance<ModelRepository.Chapters>(), this.CurrentUserIdentity);
                return _chapterProcess;
            }
        }
        private DomainOperations.User _userProcess = null;
        private DomainOperations.User UserProcess
        {
            get
            {
                if (_userProcess == null)
                    _userProcess = InstanceActivator.GetInstance<DomainOperations.User, ModelRepository.Users>(RepositoryInstanceActivator.GetInstance<ModelRepository.Users>(), this.CurrentUserIdentity);
                return _userProcess;
            }
        }
        private ModelRepository.TestAttemptsSummary _attemptsRepository = null;
        private ModelRepository.TestAttemptsSummary AttemptsRepository
        {
            get
            {
                if (_attemptsRepository == null)
                    _attemptsRepository = RepositoryInstanceActivator.GetInstance<ModelRepository.TestAttemptsSummary>();
                return _attemptsRepository;
            }
        }

        private ModelRepository.TestScoringSystems _testScoringSystems = null;
        private ModelRepository.TestScoringSystems TestScoringSystems
        {
            get
            {
                if (_testScoringSystems == null)
                    _testScoringSystems = RepositoryInstanceActivator.GetInstance<ModelRepository.TestScoringSystems>();
                return _testScoringSystems;
            }
        }

        public Model.Test GetNew(DomainOperations.Types.NewTestRequest request)
        {
            var randomGenerator = new Random();
            var test = new Model.Test
            {
                Id = Guid.NewGuid(),
                DifficultyLevel = request.DifficultLevel,
                DurationInMinutes = Constants.PersonalisedTestDuration,
                NoOfQuestions = request.NoOfQuestions,
                Questions = new List<Model.Question>(),
                Chapters = new List<Model.Chapter>(),
                StreamId = Constants.EngineeringStreamId,//TODO: get user's stream
                Active = true
            };

            var questions = QuestionsProcess.GetCandidateQuestionsForTest(new ModelRepository.Types.TestCandidateQuestionsRequest()
            {
                SubjectId = request.SubjectId,
                ChapterIds = request.ChapterIdList,
                DifficultLevel = request.DifficultLevel,
                Concepts = request.Concepts,
                Relevances = request.Relevances
            }
                ).ToList();

            var selectedQuestionIds = new List<Guid>();
            for (int i = 0; i < request.NoOfQuestions; i++)
            {
                if (questions.Count() == 0)
                    break;
                var index = randomGenerator.Next(questions.Count() - 1);
                var question = questions[index];
                questions.RemoveAt(index);
                selectedQuestionIds.Add(question.Id);
            }

            var selectedQuestions = QuestionsProcess.Get(selectedQuestionIds.ToArray());
            foreach (var selectedQuestion in selectedQuestions)
                test.Questions.Add(selectedQuestion);

            test.NoOfQuestions = selectedQuestions.Count();

            var chapters = request.ChapterIds.Length == 0 ? ChaptersProcess.GetAllForSubject(request.SubjectId)
                : ChaptersProcess.GetForChapterIds(request.ChapterIdList);

            foreach (var chapter in chapters)
            {
                test.Chapters.Add(chapter);
            }
            _repository.SaveNew(test);

            return test;
        }

        private Model.DTO.ScoringSystem[] GetScoringSystem(Model.Test test)
        {
            if(!test.ScoringSystemId.HasValue)
            {
                return this.TestScoringSystems.GetByStream(test.StreamId);
            }
            else
            {
                return this.TestScoringSystems.GetById(test.ScoringSystemId.Value);
            }
        }

        public Model.DTO.FullTest GetFullTest(Guid testId, string userId, int perQuestionDuration, int breakDurationInMinutes, int neetPerQuestionDuration)
        {
            var test = _repository.Get(testId);
            DateTime testDate = (test.LastModifiedOn.HasValue) ? test.LastModifiedOn.Value : test.CreatedOn;
            Model.DTO.FullTest fullTest = new Model.DTO.FullTest { Id = test.Id, Name = test.Name, StreamId = test.StreamId, TestType = (Model.DTO.TestType)test.TestType, DurationInMinutes = test.DurationInMinutes, NoOfQuestions = test.NoOfQuestions, MaxScore = test.MaxScore, TestDate = testDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), Sponsors = _repository.GetSponsorsForTest(test.Id), ScoringSystem = GetScoringSystem(test), Instructions = _repository.GetTestInstruction(test.Id) };

            if (fullTest.StreamId == Constants.MedicalStreamId)
            {
                perQuestionDuration = neetPerQuestionDuration;
            }

            var subjectWiseTestSections = QuestionsProcess.GetQuestionsForFullTest(testId, perQuestionDuration, "").ToList();

            int i = 1;
            List<Model.DTO.TestSection> testSectionList = new List<Model.DTO.TestSection>();
            testSectionList.Add(new Model.DTO.TestSection());
            testSectionList[0].Name = "Section 1";
            testSectionList[0].BreakDurationInMinutes = breakDurationInMinutes;

            foreach (var subjectWiseTestSection in subjectWiseTestSections)
            {
                if ((subjectWiseTestSection.SubjectId == Constants.PhysicsSubjectId) || (subjectWiseTestSection.SubjectId == Constants.ChemistrySubjectId))
                {
                    if (testSectionList[0].SubjectWiseTestSections == null)
                    {
                        testSectionList[0].SubjectWiseTestSections = new List<Model.DTO.SubjectWiseTestSection>();
                    }

                    testSectionList[0].SubjectWiseTestSections.Add(subjectWiseTestSection);

                    if (subjectWiseTestSection.Questions != null)
                    {
                        testSectionList[0].DurationInMinutes = testSectionList[0].DurationInMinutes + perQuestionDuration * subjectWiseTestSection.Questions.Count() / 60;
                        testSectionList[0].NoOfQuestions = testSectionList[0].NoOfQuestions + subjectWiseTestSection.Questions.Count();
                    }
                }
                else
                {
                    testSectionList.Add(new Model.DTO.TestSection());
                    testSectionList[i].Name = "Section " + (i + 1).ToString();

                    testSectionList[i].SubjectWiseTestSections = new List<Model.DTO.SubjectWiseTestSection>();
                    testSectionList[i].SubjectWiseTestSections.Add(subjectWiseTestSection);
                    testSectionList[i].BreakDurationInMinutes = breakDurationInMinutes;

                    if (subjectWiseTestSection.NoOfQuestions > 0)
                    {
                        //For Maths per question duration is twice of normal per question duration
                        if (subjectWiseTestSection.SubjectId == Constants.MathematicSubjectId)
                        {
                            testSectionList[i].DurationInMinutes = perQuestionDuration * 2 * subjectWiseTestSection.Questions.Count() / 60;
                        }
                        else
                        {
                            testSectionList[i].DurationInMinutes = perQuestionDuration * subjectWiseTestSection.Questions.Count() / 60;
                        }
                        testSectionList[i].NoOfQuestions = subjectWiseTestSection.NoOfQuestions;
                    }
                    else
                    {
                        testSectionList[i].DurationInMinutes = 0;
                        testSectionList[i].NoOfQuestions = 0;
                    }
                    i++;
                }
            }

            fullTest.TestSections = testSectionList;

            return fullTest;
        }

        private Model.DTO.FullTest GetFullTestInfo(Model.Test test, int perQuestionDuration, int breakDurationInMinutes, string relevance, bool includeDetails = true)
        {
            //var test = _repository.Get(testId);
            DateTime testDate = (test.LastModifiedOn.HasValue) ? test.LastModifiedOn.Value : test.CreatedOn;
            Model.DTO.FullTest fullTest = new Model.DTO.FullTest
            {
                Id = test.Id,
                Name = test.Name,
                StreamId = test.StreamId,
                TestType = (Model.DTO.TestType)test.TestType,
                DurationInMinutes = test.DurationInMinutes,
                NoOfQuestions = test.NoOfQuestions,
                MaxScore = test.MaxScore,
                TestDate = testDate.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                Sponsors = _repository.GetSponsorsForTest(test.Id),
                ScoringSystem = GetScoringSystem(test),
                Instructions = _repository.GetTestInstruction(test.Id),
                Relevance = test.Relevance,
                Tenant = TenantProcess.Get(test.TenantId)
            };

            var subjectWiseTestSections = QuestionsProcess.GetQuestionsForFullTest(test.Id, perQuestionDuration, relevance, includeDetails).ToList();

            int i = 1;
            List<Model.DTO.TestSection> testSectionList = new List<Model.DTO.TestSection>();
            testSectionList.Add(new Model.DTO.TestSection());
            testSectionList[0].Name = "Section 1";
            testSectionList[0].BreakDurationInMinutes = 5;

            foreach (var subjectWiseTestSection in subjectWiseTestSections)
            {
                if ((subjectWiseTestSection.SubjectId == Constants.PhysicsSubjectId) || (subjectWiseTestSection.SubjectId == Constants.ChemistrySubjectId))
                {
                    if (testSectionList[0].SubjectWiseTestSections == null)
                    {
                        testSectionList[0].SubjectWiseTestSections = new List<Model.DTO.SubjectWiseTestSection>();
                    }

                    testSectionList[0].SubjectWiseTestSections.Add(subjectWiseTestSection);

                    if (subjectWiseTestSection.NoOfQuestions > 0)
                    {
                        if (relevance == "JEE MAIN")
                        {
                            testSectionList[0].NoOfQuestions = testSectionList[0].NoOfQuestions + subjectWiseTestSection.NoOfQuestions;
                            testSectionList[0].DurationInMinutes = 2 * testSectionList[0].NoOfQuestions;
                        }
                        else
                        {
                            testSectionList[0].DurationInMinutes = testSectionList[0].DurationInMinutes + perQuestionDuration * subjectWiseTestSection.NoOfQuestions / 60;
                            testSectionList[0].NoOfQuestions = testSectionList[0].NoOfQuestions + subjectWiseTestSection.NoOfQuestions;
                        }
                    }
                }
                else
                {
                    testSectionList.Add(new Model.DTO.TestSection());
                    testSectionList[i].Name = "Section " + (i + 1).ToString();

                    testSectionList[i].SubjectWiseTestSections = new List<Model.DTO.SubjectWiseTestSection>();
                    testSectionList[i].SubjectWiseTestSections.Add(subjectWiseTestSection);

                    testSectionList[i].BreakDurationInMinutes = 5;
                    if (subjectWiseTestSection.NoOfQuestions > 0)
                    {
                        if (relevance == "JEE MAIN")
                        {
                            testSectionList[i].DurationInMinutes = 2 * subjectWiseTestSection.NoOfQuestions;
                        }
                        else if (subjectWiseTestSection.SubjectId == Constants.MathematicSubjectId)
                        {
                            //For Maths per question duration is twice of normal per question duration
                            testSectionList[i].DurationInMinutes = perQuestionDuration * 2 * subjectWiseTestSection.NoOfQuestions / 60;
                        }
                        else
                        {
                            testSectionList[i].DurationInMinutes = perQuestionDuration * subjectWiseTestSection.NoOfQuestions / 60;
                        }
                        testSectionList[i].NoOfQuestions = subjectWiseTestSection.NoOfQuestions;
                    }
                    else
                    {
                        testSectionList[i].DurationInMinutes = 0;
                        testSectionList[i].NoOfQuestions = 0;
                    }
                    i++;
                }
            }

            fullTest.TestSections = testSectionList;
            return fullTest;
        }

        public void SaveUserAttempt(DomainOperations.Types.TestAttemptRequest attemptRequest)
        {
            if (attemptRequest.DeviceId != null && attemptRequest.DeviceId != String.Empty)
            {
                var attemptSummary = new Model.TestAttemptSummary
                {
                    TestId = attemptRequest.TestId,
                    UserId = attemptRequest.UserId,
                    CorrectAnswers = attemptRequest.CorrectAnswers,
                    InCorrectAnswers = attemptRequest.InCorrectAnswers,
                    SkippedQuestions = attemptRequest.UnAnsweredQuestions,
                    Score = attemptRequest.Score,
                    TimeSpentInSeconds = attemptRequest.TimeSpentInSeconds,
                    TestEndReason = attemptRequest.TestEndReason,
                    AttemptDetails = new List<Model.TestAttemptDetail>()
                };

                var questionBookmarks = new List<Model.QuestionBookmark>();

                foreach (var questionAttempt in attemptRequest.Questions)
                {
                    string userAnswerChoice = null;
                    if (questionAttempt.UserAnswer != null)
                    {
                        userAnswerChoice = questionAttempt.UserAnswer;
                    }

                    var attemptDetail = new Model.TestAttemptDetail
                    {
                        QuestionId = questionAttempt.QuestionId,
                        AnswerChoice = userAnswerChoice,
                        AnswerStatus = questionAttempt.AnswerStatus,
                        TimeSpentInSeconds = questionAttempt.TimeSpentInSeconds
                    };
                    attemptSummary.AttemptDetails.Add(attemptDetail);

                    if (questionAttempt.IsBookmarked == "1")
                    {
                        questionBookmarks.Add(new Model.QuestionBookmark
                        {
                            Id = questionAttempt.QuestionId,
                            UserId = attemptRequest.UserId,
                            TestId = attemptSummary.TestId,
                            BookmarkType = "Test"
                        });
                    }
                }

                AttemptsRepository.SaveNew(attemptSummary);

                if (questionBookmarks.Count > 0)
                {
                    UserProcess.AddBookmarks(questionBookmarks.ToArray());
                }
            }
            else
            {
                throw new Exception("DeviceId cannot be empty");
            }
        }
        public string SaveUserAttemptForFullTest(DomainOperations.Types.TestAttemptRequest attemptRequest, Guid attemptId)
        {
            if (attemptRequest.DeviceId != null && attemptRequest.DeviceId != String.Empty)
            {
                Model.TestAttemptSummary attemptSummary;
                Model.Test test = _repository.Get(attemptRequest.TestId);

                if (attemptId != default(Guid))
                {
                    //populate old attempt-summary
                    attemptSummary = AttemptsRepository.Get(attemptId);
                    attemptSummary.CorrectAnswers = Convert.ToInt16(attemptSummary.CorrectAnswers + attemptRequest.CorrectAnswers);
                    attemptSummary.InCorrectAnswers = Convert.ToInt16(attemptSummary.InCorrectAnswers + attemptRequest.InCorrectAnswers);
                    attemptSummary.SkippedQuestions = Convert.ToInt16(test.NoOfQuestions - (attemptSummary.CorrectAnswers + attemptSummary.InCorrectAnswers));
                    attemptSummary.TimeSpentInSeconds = attemptSummary.TimeSpentInSeconds + attemptRequest.TimeSpentInSeconds;
                    attemptSummary.TestEndReason = attemptRequest.TestEndReason;
                    attemptSummary.Score = attemptRequest.Score;
                }
                else
                {
                    attemptSummary = new Model.TestAttemptSummary
                    {
                        TestId = attemptRequest.TestId,
                        UserId = attemptRequest.UserId,
                        CorrectAnswers = attemptRequest.CorrectAnswers,
                        InCorrectAnswers = attemptRequest.InCorrectAnswers,
                        SkippedQuestions = attemptRequest.UnAnsweredQuestions,
                        TimeSpentInSeconds = attemptRequest.TimeSpentInSeconds,
                        TestEndReason = attemptRequest.TestEndReason,
                        AttemptDetails = new List<Model.TestAttemptDetail>(),
                        Score = attemptRequest.Score
                    };
                }

                var questionBookmarks = new List<Model.QuestionBookmark>();
                List<Model.TestAttemptDetail> attemptDetailList = new List<Model.TestAttemptDetail>();

                foreach (var questionAttempt in attemptRequest.Questions)
                {
                    string userAnswerChoice = null;
                    if (questionAttempt.UserAnswer != null)
                    {
                        userAnswerChoice = questionAttempt.UserAnswer;
                    }

                    var attemptDetail = new Model.TestAttemptDetail
                    {
                        //Id= Guid.NewGuid(),
                        QuestionId = questionAttempt.QuestionId,
                        AnswerChoice = userAnswerChoice,
                        AnswerStatus = questionAttempt.AnswerStatus,
                        TimeSpentInSeconds = questionAttempt.TimeSpentInSeconds
                    };
                    attemptDetailList.Add(attemptDetail);

                    if (questionAttempt.IsBookmarked == "1")
                    {
                        questionBookmarks.Add(new Model.QuestionBookmark
                        {
                            Id = questionAttempt.QuestionId,
                            UserId = attemptSummary.UserId,
                            TestId = attemptSummary.TestId,
                            BookmarkType = "Test"
                        });
                    }
                }
                attemptSummary.AttemptDetails = attemptDetailList;

                if (attemptId == default(Guid))
                {
                    attemptSummary = AttemptsRepository.SaveNew(attemptSummary);
                }
                else
                {
                    attemptSummary = AttemptsRepository.SaveExisting(attemptSummary);
                }

                if (questionBookmarks.Count > 0)
                {
                    UserProcess.AddBookmarks(questionBookmarks.ToArray());
                }

                return attemptSummary.Id.ToString();
            }
            else
            {
                throw new Exception("DeviceId cannot be empty");
            }
        }
        public Model.DTO.FullTest[] GetAllFullTest(int neetPerQuestionDuration, int perQuestionDuration, int breakDurationInMinutes, string lastSyncDate, string streamId, string tenantId, string relevance)
        {
            if (new Guid(streamId) == Constants.MedicalStreamId)
            {
                perQuestionDuration = neetPerQuestionDuration;
            }
            Model.Test[] testList = _repository.GetAllFullTest(lastSyncDate, streamId, tenantId, relevance);
            List<Model.DTO.FullTest> fullTests = new List<Model.DTO.FullTest>();
            foreach (var test in testList)
            {
                fullTests.Add(GetFullTestInfo(test, perQuestionDuration, breakDurationInMinutes, relevance, false));
            }
            return fullTests.ToArray();
        }
        public Model.Test[] GetTestOfUser(string userId, int pageNo, int pageSize)
        {
            return _repository.GetTestOfUser(userId, pageNo, pageSize);
        }

        public Model.Test GetTestWithUserAttempt(string userId, Guid attemptId)
        {
            return _repository.GetTestWithUserAttempt(userId, attemptId);
        }

        public Model.UserTestRating SaveUserTestRating(Model.UserTestRating rating)
        {
            return _repository.SaveUserTestRating(rating);
        }

        public Model.TestFormat[] GetTestFormatsForStream(Guid streamId)
        {
            var formats = _repository.GetTestFormatsForStream(streamId);
            foreach (var format in formats)
            {
                format.ScoringSystem = this.TestScoringSystems.GetById(format.ScoringSystemId);
            }
            return formats;
        }

        public ModelRepository.Types.FullTestPreviewResponse GetFullTestPreview(ModelRepository.Types.GenerateFullTestRequest request)
        {
            return _repository.GetFullTestPreview(request);
        }

        public void GenerateFullTest(ModelRepository.Types.GenerateFullTestRequest request)
        {
            _repository.GenerateFullTest(request);
        }

        public void SaveFullTestDetails(Model.DTO.FullTest fullTest)
        {
            _repository.SaveFullTestDetails(fullTest);
        }

        public Model.Sponsor[] GetAllSponsors()
        {
            return _repository.GetAllSponsors();
        }
    }
}
