﻿using System;
using System.Collections.Generic;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class QuestionOfDayProcess : ProcessBaseWithStorageOperations<ModelRepository.QuestionOfDays, Model.QuestionOfDay, Guid>, DomainOperations.QuestionOfDay
    {
        ModelRepository.QuestionOfDayAttempts _questionpofdayAttemptRepository;
        private ModelRepository.QuestionOfDayAttempts QuestionOfDayAttemptsRepository
        {
            get
            {
                if (_questionpofdayAttemptRepository == null)
                    _questionpofdayAttemptRepository = RepositoryInstanceActivator.GetInstance<ModelRepository.QuestionOfDayAttempts>();
                return _questionpofdayAttemptRepository;
            }
        }

        private DomainOperations.User _userProcess = null;

        private DomainOperations.User UserProcess
        {
            get
            {
                if (_userProcess == null)
                    _userProcess = InstanceActivator.GetInstance<DomainOperations.User, ModelRepository.Users>(RepositoryInstanceActivator.GetInstance<ModelRepository.Users>(), this.CurrentUserIdentity);
                return _userProcess;
            }
        }

        public QuestionOfDayProcess(ModelRepository.QuestionOfDays repository) : base(repository) { }

        public Model.QuestionOfDay[] GetAll()
        {
            return _repository.GetAll();
        }

        public void SaveUserAttempt(DomainOperations.Types.QuestionOfDayAttemptRequest questionOfDayAttemptRequest)
        {
            if (questionOfDayAttemptRequest.DeviceId != null && questionOfDayAttemptRequest.DeviceId != String.Empty)
            {
                //save attempt
                var attempt = new Model.QuestionOfDayAttempt
                {
                    QuestionOfDayId = questionOfDayAttemptRequest.QuestionOfDayId,
                    UserId = questionOfDayAttemptRequest.UserId,
                    TimeSpentInSeconds = questionOfDayAttemptRequest.TimeSpentInSeconds,
                    AnswerChoice = questionOfDayAttemptRequest.UserAnswer,
                    AnswerStatus = questionOfDayAttemptRequest.AnswerStatus,
                    CreatedOn = DateTime.Now,
                    Score = 0
                };

                QuestionOfDayAttemptsRepository.SaveNew(attempt);

                //bookmark question in question of day
                var questionBookmarks = new List<Model.QuestionBookmark>();
                if (questionOfDayAttemptRequest.IsBookmarked == "1")
                {
                    questionBookmarks.Add(new Model.QuestionBookmark
                    {
                        Id = questionOfDayAttemptRequest.QuestionId,
                        UserId = questionOfDayAttemptRequest.UserId,
                        TestId = questionOfDayAttemptRequest.QuestionOfDayId,
                        BookmarkType = "QuestionOfDay"
                    });
                    UserProcess.AddBookmarks(questionBookmarks.ToArray());
                }
            }
            else
            {
                throw new Exception("DeviceId cannot be empty");
            }
        }

        public DomainOperations.Types.QuestionOfDayRequest GetQuestionOfTheDay(string userId, Guid streamId)
        {
            DomainOperations.Types.QuestionOfDayRequest questionOfDayRequest = new DomainOperations.Types.QuestionOfDayRequest();
            Model.QuestionOfDay QuestionOfDay = _repository.GetQuestionOfTheDay(streamId);

            questionOfDayRequest.QuestionOfDayDetail = new DomainOperations.Types.QuestionOfDayDetail { QuestionOfDayId = QuestionOfDay.QuestionOfDayId, QuestionId = QuestionOfDay.QuestionId, Question = QuestionOfDay.Question, CreatedDate = QuestionOfDay.CreatedOn.ToString("yyyy-MM-dd HH:mm:ss") };
            questionOfDayRequest.Subject = _repository.GetSubjectByChapterId(questionOfDayRequest.QuestionOfDayDetail.Question.ChapterId.ToString());

            if (_repository.GetUserAttempt(userId, questionOfDayRequest.QuestionOfDayDetail.QuestionOfDayId.ToString()) == null)
            {
                questionOfDayRequest.IsAttempted = false;
            }
            else
            {
                questionOfDayRequest.IsAttempted = true;
            }
            return questionOfDayRequest;
        }

        public DomainOperations.Types.QuestionOfDayAnalyticsData GetQuestionOfDayAnalytics(string userId, string questionOfDayId)
        {
            StudMonk.ServiceStack.DomainOperations.Types.QuestionOfDayAnalyticsData questionOfDayAnalyticsData = new DomainOperations.Types.QuestionOfDayAnalyticsData
            {
                QuestionOfDayAnalytics = _repository.GetQuestionOfDayAnalytics(userId, questionOfDayId),
                QuestionOfDayAttempt = _repository.GetUserAttempt(userId, questionOfDayId),
                QuestionOfDay = _repository.GetQuestionOfDayById(questionOfDayId)
            };

            questionOfDayAnalyticsData.QuestionOfDay.Question = _repository.GetQuestionByQuestionOfDay(questionOfDayId);
            questionOfDayAnalyticsData.Subject = _repository.GetSubjectByChapterId(questionOfDayAnalyticsData.QuestionOfDay.Question.ChapterId.ToString());
            return questionOfDayAnalyticsData;
        }

        public Model.QuestionOfDayAttempt GetUserAttempt(string userId, string questionOfDayId)
        {
            return _repository.GetUserAttempt(userId, questionOfDayId);
        }
    }
}
