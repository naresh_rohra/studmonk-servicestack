﻿using System;
using System.Linq;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class QuestionBookmarkProcess : ProcessBaseWithStorageOperations<ModelRepository.QuestionBookmarks, Model.QuestionBookmark, Guid>, DomainOperations.QuestionBookmark
    {
        public QuestionBookmarkProcess(ModelRepository.QuestionBookmarks repository) : base(repository) { }

        public Model.Question[] GetForUser(string userId, int pageNo, int pageSize)
        {
            return _repository.GetForUser(userId, pageNo, pageSize);
        }

        public Model.Question[] GetForSubject(Guid subjectId)
        {
            return _repository.GetForSubject(subjectId);
        }

        public Model.Question[] GetForChapter(Guid chapterId)
        {
            return _repository.GetForChapter(chapterId);
        }

        public void Add(string userId, Guid[] questionIds)
        {
            _repository.SaveNew(GenerateBookmarksForIds(userId, questionIds));
        }

        public void Remove(string userId, Guid[] questionIds)
        {
            _repository.Delete(GenerateBookmarksForIds(userId, questionIds));
        }

        private Model.QuestionBookmark[] GenerateBookmarksForIds(string userId, Guid[] questionIds)
        {
            return (from questionId in questionIds select new Model.QuestionBookmark { Id = questionId, UserId = userId }).ToArray();
        }
    }
}
