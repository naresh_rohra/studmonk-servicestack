﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class TestScoringSystem : ProcessBaseWithStorageOperations<ModelRepository.TestScoringSystems, Model.TestScoringSystem, Guid>, DomainOperations.TestScoringSystem
    {
        public TestScoringSystem(ModelRepository.TestScoringSystems repository) : base(repository) { }

        public Model.TestScoringSystem[] GetAll()
        {
            return _repository.GetAll();
        }
        public Model.DTO.ScoringSystem[] GetByStream(Guid streamId)
        {
            return _repository.GetByStream(streamId);
        }

        public Model.DTO.ScoringSystem[] GetById(Guid id)
        {
            return _repository.GetById(id);
        }
    }
}
