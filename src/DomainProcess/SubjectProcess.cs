﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class SubjectProcess : ProcessBaseWithStorageOperations<ModelRepository.Subjects, Model.Subject, Guid>, DomainOperations.Subject
    {
        public SubjectProcess(ModelRepository.Subjects repository) : base(repository) { }

        public Model.Subject[] GetAll()
        {
            return _repository.GetAll();
        }

        public Model.Subject[] GetForStream(Guid streamId)
        {
            return _repository.GetForStream(streamId);
        }

        public Model.DTO.Concept[] GetAllConcepts(Guid subjectId)
        {
            return _repository.GetAllConcepts(subjectId);
        }

        protected override void BeforeSaveNew(Model.Subject entity)
        {
            entity.InstitutionId = Constants.PublicInstitutionId;
        }
    }
}
