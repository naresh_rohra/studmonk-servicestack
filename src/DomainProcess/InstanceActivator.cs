﻿using Autofac;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class InstanceActivator : DomainOperations.InstanceActivator
    {
        private IContainer _container;
        private ModelRepository.InstanceActivator _repositoryInstanceActivator;

        public InstanceActivator(ModelRepository.InstanceActivator repositoryInstanceActivator)
        {
            _repositoryInstanceActivator = repositoryInstanceActivator;
            BuildContainer();
        }

        void BuildContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterType<ChapterProcess>().As<DomainOperations.Chapter>();
            builder.RegisterType<ClientProcess>().As<DomainOperations.Client>();
            builder.RegisterType<QuestionProcess>().As<DomainOperations.Question>();
            builder.RegisterType<SubjectProcess>().As<DomainOperations.Subject>();
            builder.RegisterType<TestProcess>().As<DomainOperations.Test>();
            builder.RegisterType<UserProcess>().As<DomainOperations.User>();
            builder.RegisterType<QuestionBookmarkProcess>().As<DomainOperations.QuestionBookmark>();
            builder.RegisterType<AccountProcess>().As<DomainOperations.Account>();
            builder.RegisterType<CryptograhpyProcess>().As<DomainOperations.Cryptograhpy>();
            builder.RegisterType<QuestionOfDayProcess>().As<DomainOperations.QuestionOfDay>();
            builder.RegisterType<QuestionOfDayAttemptProcess>().As<DomainOperations.QuestionOfDayAttempt>();
            builder.RegisterType<PositionAnalyticReportProcess>().As<DomainOperations.PositionAnalyticReport>();
            builder.RegisterType<AuditLogProcess>().As<DomainOperations.AuditLog>();
            builder.RegisterType<TenantProcess>().As<DomainOperations.Tenant>();
            _container = builder.Build();
        }

        public T GetInstance<T, R>(R repository, Model.Identity currentUserIdentity) where R : ModelRepository.Repository where T : DomainOperations.OperationSet
        {
            var parameters = new NamedParameter[2];
            parameters[0] = new NamedParameter("repository", repository);
            parameters[1] = new NamedParameter("currentUserIdentity", currentUserIdentity);
            T t = _container.Resolve<T>(parameters);
            t.InstanceActivator = this;
            t.RepositoryInstanceActivator = _repositoryInstanceActivator;
            t.CurrentUserIdentity = currentUserIdentity;
            return t;
        }

        public T GetInstance<T>(Model.Identity currentUserIdentity)
            where T : DomainOperations.OperationSet
        {
            var parameters = new NamedParameter[1];
            parameters[0] = new NamedParameter("currentUserIdentity", currentUserIdentity);
            T t = _container.Resolve<T>(parameters);
            t.InstanceActivator = this;
            t.RepositoryInstanceActivator = _repositoryInstanceActivator;
            t.CurrentUserIdentity = currentUserIdentity;
            return t;
        }

    }
}
