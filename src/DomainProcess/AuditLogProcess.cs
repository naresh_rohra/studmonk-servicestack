﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class AuditLogProcess : ProcessBaseWithStorageOperations<ModelRepository.AuditLogs, Model.AuditLog, Int64>, DomainOperations.AuditLog
    {
        public AuditLogProcess(ModelRepository.AuditLogs repository) : base(repository) { }

        public Model.AuditLogReport[] GetRecentUserAction(Model.DTO.SearchAuditLogsRequest request)
        {
            return _repository.GetRecentUserAction(request);
        }

        public Model.AuditLog[] GetDump(Model.DTO.SearchAuditLogsRequest request)
        {
            return _repository.GetDump(request);
        }

        public Model.AuditLogStat[] GetAuditLogStats(Model.DTO.SearchAuditLogsRequest request)
        {
            return _repository.GetAuditLogStats(request);
        }

        public Model.DTO.UserRank[] GetFullTestTopScorers(Model.DTO.TopScorersRequest request)
        {
            return _repository.GetFullTestTopScorers(request);
        }

        public Model.DTO.UserTestScore[] GetFullTestScoresForUser(Model.DTO.UserScoresRequest request)
        {
            return _repository.GetFullTestScoresForUser(request);
        }
    }
}
