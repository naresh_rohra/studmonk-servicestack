﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class TenantProcess : ProcessBaseWithStorageOperations<ModelRepository.Tenants, Model.Tenant, string>, DomainOperations.Tenant
    {
        public TenantProcess(ModelRepository.Tenants repository) : base(repository) { }
    }
}
