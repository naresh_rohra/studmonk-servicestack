﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{

    public abstract class ProcessBase
    {
        public ModelRepository.InstanceActivator RepositoryInstanceActivator { get; set; }

        public DomainOperations.InstanceActivator InstanceActivator { get; set; }

        public Model.Identity CurrentUserIdentity { get; set; }
    }

    public abstract class ProcessBase<T>
        : ProcessBase, DomainOperations.OperationSet
        where T : ModelRepository.Repository
    {
        protected T _repository;
        protected DomainOperations.InstanceActivator _instanceActivator;

        protected ProcessBase(T repository)
        {
            _repository = repository;
        }
    }

    public abstract class ProcessBaseWithStorageOperations<TRepository, TModel, TKey> : ProcessBase<TRepository>, 
            DomainOperations.OperationSetWithStorageOperations<TModel, TKey>
        where TRepository : ModelRepository.RepositoryBaseOperations<TModel, TKey>
        where TModel : Model.ModelWithTracking<TKey>
    {
        protected ProcessBaseWithStorageOperations(TRepository repository) : base(repository) {}

        public TModel Get(TKey id)
        {
            return _repository.Get(id);
        }

        public TModel[] Get(TKey[] ids)
        {
            return _repository.Get(ids);
        }

        protected virtual void BeforeSaveNew(TModel entity)
        { }

        protected virtual void BeforeSaveExisting(TModel entity)
        { }
        
        protected virtual void AfterSaveNew(TModel entity)
        { }

        protected virtual void AfterSaveExisting(TModel entity)
        { }

        public virtual TModel Save(TModel entity)
        {
            if (entity.Id.Equals(default(TKey)))
            {
                this.BeforeSaveNew(entity);
                _repository.SaveNew(entity);
                this.AfterSaveNew(entity);
            }
            else
            {
                this.BeforeSaveExisting(entity);
                _repository.SaveExisting(entity);
                this.AfterSaveExisting(entity);
            }
            return entity;
        }

        public void Delete(TKey id)
        {
            _repository.Delete(id);
        }

        public void Delete(TKey[] ids)
        {
            _repository.Delete(ids);
        }
    }
}
