﻿using System;

namespace StudMonk.ServiceStack.DomainProcess
{
    public class ChapterProcess : ProcessBaseWithStorageOperations<ModelRepository.Chapters, Model.Chapter, Guid>, DomainOperations.Chapter
    {
        public ChapterProcess(ModelRepository.Chapters repository) : base(repository) { }

        public Model.Chapter[] GetAllForSubject(Guid subjectId)
        {
            return _repository.GetAllForSubject(subjectId);
        }

        public Model.TestChapter[] GetAllOfTest(Guid testId)
        {
            return _repository.GetAllOfTest(testId);
        }

        public Model.Chapter[] GetForChapterIds(Guid[] chapterIds)
        {
            return _repository.GetForChapterIds(chapterIds);
        }

        protected override void BeforeSaveExisting(Model.Chapter chapter)
        {
            chapter.LastModifiedBy = CurrentUserIdentity.UserId;
        }

        protected override void BeforeSaveNew(Model.Chapter chapter)
        {
            chapter.CreatedBy = CurrentUserIdentity.UserId;
            chapter.StreamId = Constants.EngineeringStreamId;
        }
    }
}
