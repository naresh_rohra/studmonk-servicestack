﻿namespace StudMonk.ServiceStack.DomainOperations
{
    public interface EmailNotification : DomainOperations.OperationSet
    {
        void SendPasswordResetToken(string fullname, string token, string toEmailAddress, string templateBody, string subject);
    }
}
