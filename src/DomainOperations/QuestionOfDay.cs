﻿using StudMonk.ServiceStack.DomainOperations.Types;
using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface QuestionOfDay : OperationSet, OperationSetWithStorageOperations<Model.QuestionOfDay, Guid>
    {
        Model.QuestionOfDay[] GetAll();

        void SaveUserAttempt(Types.QuestionOfDayAttemptRequest qdattemptRequest);

        QuestionOfDayRequest GetQuestionOfTheDay(string userId, Guid streamId);

        StudMonk.ServiceStack.DomainOperations.Types.QuestionOfDayAnalyticsData GetQuestionOfDayAnalytics(string userId,string questionOfDayId);

        Model.QuestionOfDayAttempt GetUserAttempt(string userId, string questionOfDayId);
    }
}
