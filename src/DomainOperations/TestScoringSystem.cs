﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface TestScoringSystem : OperationSet, OperationSetWithStorageOperations<Model.TestScoringSystem, Guid>
    {
        Model.TestScoringSystem[] GetAll();

        Model.DTO.ScoringSystem[] GetByStream(Guid streamId);

        Model.DTO.ScoringSystem[] GetById(Guid id);
    }
}
