﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface QuestionOfDayAttempt : OperationSet, OperationSetWithStorageOperations<Model.QuestionOfDayAttempt, Guid>
    {
        Model.QuestionOfDayAttempt[] GetAll();
    }
}
