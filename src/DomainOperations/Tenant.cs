﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface Tenant : OperationSet, OperationSetWithStorageOperations<Model.Tenant, string>
    {
    }
}
