﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.DomainOperations.Types
{
    public class EnrollmentRequest
    {
        public string UserId;

        public Guid StreamId;
    }
}
