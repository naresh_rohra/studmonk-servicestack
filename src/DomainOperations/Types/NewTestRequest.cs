﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudMonk.ServiceStack.DomainOperations.Types
{
    public class NewTestRequest
    {
        public Guid SubjectId { get; set; }

        public string[] ChapterIds { get; set; }

        public Guid[] ChapterIdList { get; set; }

        public Model.DifficultyLevel DifficultLevel { get; set; }

        public int NoOfQuestions { get; set; }
        
        public string UserId { get; set; }

        public string Concepts { get; set; }

        public string Relevances { get; set; }
    }
}
