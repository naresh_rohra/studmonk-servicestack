﻿using System;

namespace StudMonk.ServiceStack.DomainOperations.Types
{
    public class TestAttemptRequest
    {
        public Guid TestId { get; set; }

        public string UserId { get; set; }

        public string DeviceId { get; set; }

        public short CorrectAnswers { get; set; }

        public short InCorrectAnswers { get; set; }

        public short UnAnsweredQuestions { get; set; }

        public short Score { get; set; }

        public int TimeSpentInSeconds { get; set; }

        public byte TestEndReason { get; set; }

        public QuestionAttempt[] Questions { get; set; }
    }

    public class QuestionAttempt
    {
        public Guid QuestionId { get; set; }

        public string UserAnswer { get; set; }

        public string IsBookmarked { get; set; }

        public byte AnswerStatus { get; set; }

        public int TimeSpentInSeconds { get; set; }
    }

    public class FullTestAttemptRequest
    {
        public TestAttemptRequest fullTestAttemptRequest { get; set; }

        public string attemptId { get; set; }
    }
}
