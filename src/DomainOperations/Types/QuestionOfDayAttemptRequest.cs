﻿using System;

namespace StudMonk.ServiceStack.DomainOperations.Types
{

    public class QuestionOfDayAttemptRequest
    {
        public Guid QuestionOfDayId { get; set; }

        public Guid QuestionId { get; set; }

        public string UserId { get; set; }

        public string DeviceId { get; set; }

        public string UserAnswer { get; set; }

        public string IsBookmarked { get; set; }

        public byte AnswerStatus { get; set; }

        public int TimeSpentInSeconds { get; set; }
    }

    public class QuestionOfDayDetail
    {
        public Guid QuestionOfDayId { get; set; }

        public Guid QuestionId { get; set; }

        public string CreatedDate { get; set; }

        public Model.Question Question { get; set; }
    }

    public class QuestionOfDayRequest
    {
        public QuestionOfDayDetail QuestionOfDayDetail { get; set; }

        public Model.Subject Subject { get; set; }

        public bool IsAttempted { get; set; }
    }

    public class QuestionOfDayAnalyticsData
    {
        public Model.QuestionOfDayAnalytics QuestionOfDayAnalytics { get; set; }
        public Model.QuestionOfDay QuestionOfDay { get; set; }
        public Model.Subject Subject { get; set; }
        public Model.QuestionOfDayAttempt QuestionOfDayAttempt { get; set; }
    }
}
