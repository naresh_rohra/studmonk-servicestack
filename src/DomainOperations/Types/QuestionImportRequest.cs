﻿using System;

namespace StudMonk.ServiceStack.DomainOperations.Types
{
    public class QuestionImportRequest
    {
        public Guid SubjectId { get; set; }
        public Guid ChapterId { get; set; }

        public ImportQuestionDetail[] ImportQuestionDetails { get; set; }
    }

    public class ImportQuestionDetail
    {
        public string Text { get; set; }

        public string Choice1 { get; set; }

        public string Choice2 { get; set; }

        public string Choice3 { get; set; }

        public string Choice4 { get; set; }

        public string CorrectAnswer { get; set; }

        public string DifficultyLevel { get; set; }

        public string Solution { get; set; }

        public string Hint { get; set; }

        public string PreviousAppearances { get; set; }

        public string Sources { get; set; }

        public string Concepts { get; set; }

        public string Relevances { get; set; }
    }
}
