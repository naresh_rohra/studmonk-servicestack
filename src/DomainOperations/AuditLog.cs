﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface AuditLog : OperationSet, OperationSetWithStorageOperations<Model.AuditLog, Int64>
    {
        Model.AuditLogReport[] GetRecentUserAction(Model.DTO.SearchAuditLogsRequest request);

        Model.AuditLog[] GetDump(Model.DTO.SearchAuditLogsRequest request);

        Model.AuditLogStat[] GetAuditLogStats(Model.DTO.SearchAuditLogsRequest request);

        Model.DTO.UserRank[] GetFullTestTopScorers(Model.DTO.TopScorersRequest request);

        Model.DTO.UserTestScore[] GetFullTestScoresForUser(Model.DTO.UserScoresRequest request);
    }
}
