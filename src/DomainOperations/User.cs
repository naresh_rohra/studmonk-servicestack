﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface User : OperationSet, OperationSetWithStorageOperations<Model.User, Guid>
    {
        Model.User GetByDeviceId(string deviceId);

        void Register(Model.User newUser);

        void AddBookmarks(Model.QuestionBookmark[] questionBookmarks);

        Model.DTO.UserRank GetUserRanking(Model.DTO.UserRankingRequest request);
    }
}
