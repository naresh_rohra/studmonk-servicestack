﻿using System;
using StudMonk.ServiceStack.DomainOperations.Types;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface Question : OperationSet, OperationSetWithStorageOperations<Model.Question, Guid>
    {
        Model.DTO.QuestionsList GetForChapter(ModelRepository.Types.QuestionListRequest request);

        Model.DTO.QuestionsList GetForSubjectChaptersWithDifficultyLevel(ModelRepository.Types.QuestionListRequest request);

        Model.Question[] GetCandidateQuestionsForTest(ModelRepository.Types.TestCandidateQuestionsRequest request);

        Model.DTO.QuestionsList SearchForChapter(Guid chapterId, string searchText, Guid exclusionQuestionId);

        bool ImportQuestions(QuestionImportRequest questionImportReq);

        void MarkQuestionsAsNeedsCorrection(Guid[] questionIds);

        void MarkQuestionsAsReviewed(Guid[] questionIds);

        void MarkQuestionsAsNotReviewed(Guid[] questionIds);

        Model.Question SaveAndApprove(Model.Question question);

        Model.Question[] GetQuestionsForTest(Guid testId);

        string[] GetAllRelevanceTags();

        string[] GetAllConceptTags();

        Model.DTO.SubjectWiseTestSection[] GetQuestionsForFullTest(Guid testId,int perQuestionDuration, string relevance, bool includeDetails = true);

        Model.UserQuestionFlag SaveUserQuestionFlag(Model.UserQuestionFlag questionFlag);
    }
}
