﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface OperationSet
    {
        ModelRepository.InstanceActivator RepositoryInstanceActivator { get; set; }

        DomainOperations.InstanceActivator InstanceActivator { get; set; }

        Model.Identity CurrentUserIdentity { get; set; }
    }

    public interface OperationSetWithStorageOperations<TModel, TKey>  where TModel : Model.ModelBase<TKey>
    {
        TModel Get(TKey id);

        TModel[] Get(TKey[] ids);

        TModel Save(TModel entity);

        void Delete(TKey id);

        void Delete(TKey[] ids);
    }
}