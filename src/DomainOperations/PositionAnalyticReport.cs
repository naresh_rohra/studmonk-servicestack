﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface PositionAnalyticReport : OperationSet, OperationSetWithStorageOperations<Model.PositionAnalyticReport, Guid>
    {
       Model.PositionAnalyticReport GetPositionAnalyticReport(string userid,string subjectid);
    }
}
