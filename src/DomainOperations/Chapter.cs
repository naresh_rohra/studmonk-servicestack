﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface Chapter : OperationSet, OperationSetWithStorageOperations<Model.Chapter, Guid>
    {
        Model.Chapter[] GetAllForSubject(Guid subjectId);

        Model.TestChapter[] GetAllOfTest(Guid testId);

        Model.Chapter[] GetForChapterIds(Guid[] chapterIds);
    }
}
