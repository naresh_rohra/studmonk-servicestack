﻿namespace StudMonk.ServiceStack.DomainOperations
{
    public interface InstanceActivator
    {
        T GetInstance<T, R>(R repository, Model.Identity currentUserIdentity) where R : ModelRepository.Repository where T : DomainOperations.OperationSet;

        T GetInstance<T>(Model.Identity currentUserIdentity) where T : DomainOperations.OperationSet;
    }
}
