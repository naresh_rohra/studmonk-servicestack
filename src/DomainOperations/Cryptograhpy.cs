﻿namespace StudMonk.ServiceStack.DomainOperations
{
    public interface Cryptograhpy : DomainOperations.OperationSet
    {
        string Hash(string plainText);

        string Encrypt(string plainText);

        string Decrypt(string plainText);
    }
}
