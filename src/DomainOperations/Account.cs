﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface Account : OperationSet
    {
        Model.StudentEnrollmentDetail EnrollStudent(string userId, Guid streamId);
    }
}
