﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface Subject : OperationSet, OperationSetWithStorageOperations<Model.Subject, Guid>
    {
        Model.Subject[] GetAll();

        Model.Subject[] GetForStream(Guid streamId);

        Model.DTO.Concept[] GetAllConcepts(Guid subjectId);
    }
}
