﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface Test : OperationSet, OperationSetWithStorageOperations<Model.Test, Guid>
    {
        Model.Test GetNew(Types.NewTestRequest request);

        void SaveUserAttempt(Types.TestAttemptRequest attemptRequest);

        Model.DTO.FullTest[] GetAllFullTest(int neetPerQuestionDuration, int perQuestionDuration, int breakDurationInMinutes, string lastSyncDate, string streamId, string tenantId, string relevance);

        Model.DTO.FullTest GetFullTest(Guid testId, string userId, int perQuestionDuration, int breakDurationInMinutes, int neetPerQuestionDuration);

        string SaveUserAttemptForFullTest(Types.TestAttemptRequest attemptRequest, Guid attemptId);

        Model.UserTestRating SaveUserTestRating(Model.UserTestRating rating);

        Model.Test[] GetTestOfUser(string userId, int pageNo, int pageSize);

        Model.Test GetTestWithUserAttempt(string userId, Guid attemptId);

        Model.TestFormat[] GetTestFormatsForStream(Guid streamId);

        ModelRepository.Types.FullTestPreviewResponse GetFullTestPreview(ModelRepository.Types.GenerateFullTestRequest request);

        void GenerateFullTest(ModelRepository.Types.GenerateFullTestRequest request);

        Model.Sponsor[] GetAllSponsors();

        void SaveFullTestDetails(Model.DTO.FullTest test);
    }
}