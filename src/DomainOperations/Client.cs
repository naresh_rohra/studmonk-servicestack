﻿namespace StudMonk.ServiceStack.DomainOperations
{
    public interface Client : OperationSet
    {
        Model.Settings.Client GetSettings();
    }
}
