﻿using System;

namespace StudMonk.ServiceStack.DomainOperations
{
    public interface QuestionBookmark : OperationSet, OperationSetWithStorageOperations<Model.QuestionBookmark, Guid>
    {
        Model.Question[] GetForUser(string userId, int pageNo, int pageSize);

        Model.Question[] GetForSubject(Guid subjectId);

        Model.Question[] GetForChapter(Guid chapterId);

        void Add(string userId, Guid[] questionIds);

        void Remove(string userId, Guid[] questionIds);
    }
}
